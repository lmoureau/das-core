# How to contribute?

It it important to respect the logic of the code.

## A few basic principles

General principle at which we try to stick:
 - never write longer pieces of code than 200 (maybe 300) lines of code;
 - never copy paste (but do not over-engineer);
 - never do complex operations;
 - split processing operations from plotting scripts;
 - comment à la doxygen.

More related to the current code:
 - write in C++;
 - respect CMSSW conventions and compile with the `scram` commands;
 - write only compilable code, no Root scripts (this is done in the workarea).

## Design

The idea is to have two kinds of executables:
 - the *n*-tuple modifiers
 - the projectors

When running over a *n*-tuple, it must be possible to speed up the process by
parallelisation of the workflow, using the following syntax:

```
command input ouput [...] [nTot [nCurrent]]
```

Drawing/plotting is performed separately in the workarea (a dedicated Git
repository exists for that), using scripts for simplicity.

## What you could do

Things which would be appreciated:
 - Port the system to CERN (currently only supported for DESY).
 - Check the issue list and propose fixes.
