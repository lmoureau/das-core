# Common Tools

The common tools consist in executable(s), libraries, and scripts, mostly related to the splitting of the job into several jobs to process *n*-tuples in a faster way.

## Meta information

The meta information of a file consists in information such as the year of data acquisition or the nature (data or simulation).
Such information can be used automatically by an executable, for instance to select the right calibration files.
It can be extracted with the `printMetaInfo` command.

## Looper

The `Looper  is a class that can be used for two things:
 - first it selects a subrange of the event list in a *n*-tuple,
 - then it prints to the standard output the progress of the processing of this subrange in percents.

## Toolbox and variables

Some common functions or variables such as the calculation of the rapidity or the standard binning of the pt spectrum are stored here.

## Speed up the processing of a *n*-tuple

Two options are available: `parallel` and `submit`.
The processing of the *n*-tuple is then split into several threads or jobs (respectively).
They are based on the fire-and-forget strategy, meaning that the shared objects are copied to a temporary directory.

If a command is defined as 
```
command arg1 arg2 ...
```
then it to speed up, you just need
```
submit/parallel command arg1 arg2 ...
```

All temporary files are stored in an extra directory, created for each `parallel` or `submit` run.
This directory is not erased and can be used to investigate possible issues.

### `parallel`

Automatically distribute a task locally over several cores.

An extra argument `-j` allows to specify the number of "jobs" (or threads) to be used.
For instance, the run on 6 cores:
```
parallel -j6 command arg1 arg2
As a default the number of threads of the current machine is taken (you can check is with `nproc`).
```
You can also use the option `-o` to find the less busy NAF machine at DESY.
```
parallel -o command arg1 arg2
```

In a similar way, you can use `-b` to run the parallelisation in background.
All three options can of course be combined.

### `submit` 

Submit current task on DESY HTCondor based farm.

The scripts for the submission can be found in `farm`.
They can be killed with `condor_rm`.

The number of jobs can be changed with the `-j` flag, the default one being 20 jobs:
```
submit -j30 command arg1 arg2 ...
```
Interrupting the command with `C+c` does not kill the jobs but only interrupts the watching of the progress.

Alternatively, jobs can be run in background without watching the progress:
```
submit -b -j30 command arg1 arg2
```
