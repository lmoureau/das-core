#include <iostream>

#include <filesystem>

#include <TFile.h>
#include <TTree.h>

using namespace std;

namespace fs = filesystem;

void getEntries (const fs::path& input)
{
    if (!fs::exists(input)) {
        cerr << "\x1B[31m\e[1m" << input << " does not exists.\x1B[30m\e[0m\n";
        exit(EXIT_FAILURE);
    }
    auto f = TFile::Open(input.c_str());
    assert(f->IsOpen());
    auto t = dynamic_cast<TTree*>(f->Get("inclusive_jets"));
    if (t == nullptr) {
        t = dynamic_cast<TTree*>(f->Get("ntupliser/inclusive_jets"));
        if (t == nullptr) {
            cerr << "\x1B[31m\e[1m`inclusive_jets` does not exists.\x1B[30m\e[0m\n";
            f->ls();
            exit(EXIT_FAILURE);
        }
    }
    cout << t->GetEntries() << endl;
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    if (argc < 2) {
        cout << argv[0] << " input" << endl;
        return EXIT_SUCCESS;
    }

    fs::path input = argv[1];

    getEntries(input);
    return EXIT_SUCCESS;
}
#endif
