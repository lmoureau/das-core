#include <cstdlib>
#include <cassert>
#include <fstream>
#include <algorithm>
#include <filesystem>

#include "Core/CommonTools/interface/terminal.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"

#include "Core/JEC/interface/JESreader.h"

#include "getSpectrum.h"

#include <TROOT.h>
#include <TString.h>
#include <TChain.h>
#include <TProfile.h>

#include "Math/VectorUtil.h"

using namespace std;

namespace fs = filesystem;

////////////////////////////////////////////////////////////////////////////////
/// Projects many branches of the *n*-tuple onto histograms (jet and event variables).
void getSpectrum 
             (const fs::path& input,  //!< name of input root file 
              const fs::path& output, //!< name of output root file
              int nSplit = 1, //!< number of jobs/tasks/cores
              int nNow = 0)   //!< index of job/task/core
{
    /// Opening source and checking that it is MC
    assert(fs::exists(input));
    TFile * source = TFile::Open(input.c_str(), "READ");
    TTree * tree = dynamic_cast<TTree *>(source->Get("inclusive_jets"));

    MetaInfo metainfo(tree);
    if (nNow == 0) metainfo.Print();
    bool isMC = metainfo.isMC();

    Event * ev = nullptr;
    PileUp * pu = nullptr;
    MET * met = nullptr;
    PrimaryVertex * pv = nullptr;
    tree->SetBranchAddress("event", &ev);
    tree->SetBranchAddress("pileup", &pu);
    tree->SetBranchAddress("met", &met);
    tree->SetBranchAddress("primaryvertex", &pv);

    vector<RecJet> * recjets = nullptr;
    vector<GenJet> * genjets = nullptr;
    tree->SetBranchAddress("recJets", &recjets);

    if (isMC)
        tree->SetBranchAddress("genJets", &genjets);

    if (fs::exists(output))
        cerr << red << output << " will be overwritten!\n" << normal;
    auto file = TFile::Open(output.c_str(), "RECREATE");

    JESplots jes(isMC);
    TH2 * pt_y_finebinning_beforeJECs = new TH2D("pt_y_finebinning_beforeJECs", "before JECs", nPtBins, pt_edges.data(), nEtaBins, eta_edges.data());
    TH2 * pt_y_finebinning_afterJECs = new TH2D("pt_y_finebinning_afterJECs", "after JECs", nPtBins, pt_edges.data(), nEtaBins, eta_edges.data());

    vector<TH3 *> pt_y_ns;
    for (int N = 1; N <= maxMult; ++N) {
        TString name = Form("pt_y_nth_Nbin%d", N); // n-th jet for jet multiplicity N
        TString title = Form("3D inclusive jet distribution with N_{jets} = %d", N);
        TH3 * h = new TH3D(name, title, nPtBins, pt_edges.data(),
                                         nYbins,  y_edges.data(),
                                        maxMult,  n_edges.data());
        pt_y_ns.push_back(h);
    }

    JetPlots    jetplots("inclusive", genBins, recBins),
             genjetplots("gen"      , genBins, recBins);
    EventPlots eventplots;

    Looper looper(__func__, tree, nSplit, nNow);
    while (looper.Next()) {

        size_t N = count_if(recjets->begin(), recjets->end(), [](RecJet& recjet) { return recjet.CorrPt() > 74 && abs(recjet.Rapidity()) < 2.5;}); // multiplicity
        size_t n = 1;

        // jets
        auto evWgt = ev->recWgts.front();
        if (isMC) evWgt *= ev->genWgts.front();
        for (size_t i = 0; i < recjets->size(); ++i) {
            auto& recjet = recjets->at(i);
            assert(recjet.weights.size() > 0);
            
            auto jetWgt = recjet.weights.front();
            auto w = jetWgt * evWgt;

            pt_y_finebinning_beforeJECs->Fill(recjet.p4.Pt() , abs(recjet.Rapidity()), w);
            pt_y_finebinning_afterJECs ->Fill(recjet.CorrPt(), abs(recjet.Rapidity()), w);
            jes.Fill(recjet, w);

            bool leadInTk = abs(recjets->front().Rapidity()) < 2.5; 
            (leadInTk ? jetplots.pt_y_leadInTk : jetplots.pt_y_leadFwd)->Fill(recjet.CorrPt(), abs(recjet.Rapidity()), w);

            auto JEC = recjet.JECs.front();
            jetplots.Fill(recjet.p4, recjet.area, w, JEC);

            // from now on, for model reweighting
            if (N-1 >= static_cast<size_t>(maxMult)) continue;
            auto absrap = recjet.AbsRap();
            if (absrap >= 2.5) continue;
            auto pt = recjet.CorrPt();
            if (pt < 74) continue;
            if (n > N) continue;
            pt_y_ns.at(N-1)->Fill(pt, absrap, n, w);
            ++n;
        }
        // event
        eventplots.Fill(ev, met, pu, pv);

        if (isMC) 
        for (const auto& genjet: *genjets)
            genjetplots.Fill(genjet.p4, 0, ev->genWgts.front());

    }

    for (TH3 * h: pt_y_ns) h->Write();

    jes.Write();

    for (const JetPlots& plots: {jetplots, genjetplots})
        plots.Write();

    eventplots.Write();

    file->cd();
    pt_y_finebinning_beforeJECs->SetDirectory(file);
    pt_y_finebinning_afterJECs ->SetDirectory(file);
    pt_y_finebinning_beforeJECs->Write();
    pt_y_finebinning_afterJECs ->Write();
    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();

    if (argc < 3) {
        cout << argv[0] << " input output [nSplit [nNow]]" << endl;
        return EXIT_SUCCESS;
    }

    fs::path input = argv[1],
             output = argv[2];
    int nNow = 0, nSplit = 1;
    if (argc > 3) nSplit = atoi(argv[3]);
    if (argc > 4) nNow = atoi(argv[4]);

    getSpectrum(input, output, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
