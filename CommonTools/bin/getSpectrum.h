#include <vector>
#include <iostream>

#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TFile.h>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/variables.h"

using std::vector;
using std::cout;
using std::endl;
using namespace DAS;

////////////////////////////////////////////////////////////////////////////////
/// Contains 2D JES plots
struct JESplots {

    vector<TH2 *> pt_y;

    JESplots (bool isMC) 
    {
        vector<TString> JECs {"nominal"};
        if (!isMC)  {
            vector<TString> JECUncNames = JESreader::getJECUncNames();
            for (TString JECUncName: JECUncNames)  {
                JECs.push_back(JECUncName + "_up");
                JECs.push_back(JECUncName + "_dn");
            }
        }
        else {
            JECs.push_back("JERup");
            JECs.push_back("JERdown");
        }
        for (TString JEC: JECs)
            pt_y.push_back(new TH2D(JEC, JEC,
                        pt_edges.size()-1, pt_edges.data(), y_edges.size()-1, y_edges.data()));
    }

    void Fill (const RecJet& jet, auto w)
    {
        //cout << jet.JECs.size() << ' ' << pt_y.size() << endl;
        assert(jet.JECs.size() <= pt_y.size());
        auto pt = jet.p4.Pt(),
              y  = jet.Rapidity();
        for (size_t i = 0; i < jet.JECs.size(); ++i) {
            float corr = jet.JECs[i];
            pt_y[i]->Fill(pt*corr, y, w);
        }
    }

    void Write ()
    {
        cout << "Saving JES" << endl;
        TDirectory * d = gFile->mkdir("JES");
        d->cd();
        for (TH2 * h: pt_y) {
            h->SetDirectory(d);
            h->Write(h->GetTitle());
        }
        gFile->cd();
    }
};

////////////////////////////////////////////////////////////////////////////////
/// Contains 1D and 2D histograms to describe the jet kinematics.
struct JetPlots {
    
    TString name;
    TH1 *pt, *ptUnf, *y, *eta, *phi, *area;
    TH2 *pt_y, *ptUnf_y, *ptEta;
    TH2 *pt_y_leadInTk, *pt_y_leadFwd;

    JetPlots //!< Constructor, initialises all histograms
        (TString Name, vector<double> genBins, vector<double> recBins) :
        name(Name),
        pt(new TH1F(name + "pt", "pt", recBins.size()-1, recBins.data())),
        ptUnf(new TH1F(name + "ptUnf", "ptUnf", genBins.size()-1, genBins.data())),
        y(new TH1F(name + "y" , "y ", y_edges.size()-1, y_edges.data())),
        eta(new TH1F(name + "eta" , "eta ", y_edges.size()-1, y_edges.data())),
        phi(new TH1F(name + "phi", "phi", 314, -M_PI, M_PI)),
        area(new TH1F(name + "area", "area", 100, 0, 2)),
        pt_y(new TH2F(name + "pt_y", "pt_y", recBins.size()-1, recBins.data(), y_edges.size()-1, y_edges.data())),
        ptUnf_y(new TH2F(name + "ptUnf_y", "ptUnf_y", genBins.size()-1, genBins.data(), y_edges.size()-1, y_edges.data())),
        ptEta(new TH2F(name + "ptEta", "ptEta", recBins.size()-1, recBins.data(), y_edges.size()-1, y_edges.data())),
        pt_y_leadInTk(new TH2F(name + "pt_y_leadInTk", "pt_y", recBins.size()-1, recBins.data(), y_edges.size()-1, y_edges.data())),
        pt_y_leadFwd(new TH2F(name + "pt_y_leadFwd", "pt_y", recBins.size()-1, recBins.data(), y_edges.size()-1, y_edges.data()))
    { }

    void Fill //!< Fill all histograms directly
           (const FourVector& p4, //!< four-momentum of the jet
            float Area,           //!< jet area
            float weight,        //!< entry weight (jet*event)
            float JEC = 1.)
    {
        pt->Fill(p4.Pt()*JEC, weight);
        ptUnf->Fill(p4.Pt()*JEC, weight);
        y->Fill(abs(Rapidity(p4)), weight);
        eta->Fill(abs(p4.Eta()), weight);
        phi->Fill(p4.Phi(), weight);
        area->Fill(Area, weight);
        ptEta->Fill(p4.Pt()*JEC, abs(p4.Eta()), weight);
        pt_y->Fill(p4.Pt()*JEC, abs(Rapidity(p4)), weight);
        ptUnf_y->Fill(p4.Pt()*JEC, abs(Rapidity(p4)), weight);
    }

    void Write () const
    {
        cout << "Saving " << name << endl;
        TDirectory * d = gFile->mkdir("JetPlots_" + name);
        d->cd();
        for (TH1 * h: vector<TH1*>({pt, ptUnf, y, eta, phi, area, pt_y, ptUnf_y, ptEta, pt_y_leadInTk, pt_y_leadFwd})) {
            TString n = h->GetName();
            n.ReplaceAll(name, "");
            h->SetDirectory(d);
            h->Write(n);
        }
        gFile->cd();
    }
};

////////////////////////////////////////////////////////////////////////////////
/// Contains 1D and 2D histograms to describe the jet kinematics.
struct EventPlots {

    // from Event
    TH1 * hard_scale;

    // from MET
    TH1 * metEt, * metSumEt, * metFraction, * metPt, * metPhi;

    // from PileUp
    TH1 * puRho, * nVtx, * trpu, * intpu, * pthatMax;

    // from PV
    TH1 * pvRho, * pvZ, * pvChi2ndof;

    EventPlots () : //!< Constructor, initialises all histograms
        hard_scale (new TH1F("hard_scale", "hard_scale", 6500, 0, 6500)), 
        metEt      (new TH1F("metEt", "metEt", 100, 1, 1000)),
        metSumEt   (new TH1F("metSumEt", "metSumEt", 650, 1, 6500)),
        metFraction(new TH1F("metFraction", "metFraction", 100, 0, 1)),
        metPt      (new TH1F("metPt", "metPt", 100, 1, 1000)),
        metPhi     (new TH1F("metPhi", "metPhi", 314, -M_PI, M_PI)),
        puRho      (new TH1F("puRho",   "puRho", 100, 0, 100)),
        nVtx       (new TH1F("nVtx",  "nVtx", 100, 0, 100)),
        trpu       (new TH1F("trpu",  "trpu", 100, 0, 100)),
        intpu      (new TH1F("intpu", "intpu", 100, 0, 100)),
        pthatMax   (new TH1F("pthatMax", "pthatMax", 6500, 0, 6500)),
        pvRho      (new TH1F("pvRho", "pvRho", 150, 0, 1.5)),
        pvZ        (new TH1F("pvZ", "pvZ", 48, -24, 24)),
        pvChi2ndof (new TH1F("pvChi2ndof", "pvChi2ndof", 50, 0, 5))
    {}

    void Fill //!< Fill all histograms directly
        (Event* ev,             //!< event info (run number, etc, but also weight)
         MET* met,              //!< MET info
         PileUp* pu,            //!< PU info
         PrimaryVertex * pv)    //!< PV info
    {
        auto w = ev->recWgts.front();
        if (ev->genWgts.size() > 0) w *= ev->genWgts.front();

        // from Event
        hard_scale->Fill(ev->hard_scale, w);

        // from MET
        metEt->Fill(met->Et, w);
        metSumEt->Fill(met->SumEt, w);
        metFraction->Fill(met->Et/met->SumEt, w);
        metPt->Fill(met->Pt, w);
        metPhi->Fill(met->Phi, w);

        // from PileUp
        puRho->Fill(pu->rho, w);
        nVtx ->Fill(pu->nVtx, w);
        trpu ->Fill(pu->trpu, w);
        intpu->Fill(pu->intpu, w);
	if (ev->genWgts.size() > 0) {
	    auto wgen = ev->genWgts.front();
	    pthatMax->Fill(pu->pthatMax, wgen);
	}

        // from PrimaryVertex
        pvRho     ->Fill(pv->Rho, w);
        pvZ       ->Fill(pv->z, w);
        pvChi2ndof->Fill(pv->chi2/pv->ndof, w);
    }

    void Write () const
    {
        cout << "Saving EventPlots" << endl;
        TDirectory * d = gFile->mkdir("EventPlots");
        d->cd();
        for (TH1 * h: {hard_scale, metEt, metSumEt, metFraction, metPt, metPhi,
                    puRho, nVtx, trpu, intpu, pthatMax, pvRho, pvZ, pvChi2ndof }) {
            h->SetDirectory(d);
            h->Write();
        }
        gFile->cd();
    }
};

