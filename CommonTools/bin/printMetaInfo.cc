#include <cstdlib>
#include <iostream>
#include <filesystem>

#include <TFile.h>
#include <TROOT.h>
#include <TTree.h>

#include "Core/CommonTools/interface/MetaInfo.h"

using namespace std;
using namespace DAS;

namespace fs = filesystem;

void printMetaInfo (const fs::path& input)
{
    assert(fs::exists(input));
    TFile * source = TFile::Open(input.c_str(), "READ");
    TTree * tree = dynamic_cast<TTree *>(source->Get("inclusive_jets"));

    MetaInfo metainfo(tree);
    metainfo.Print();
}

int main (int argc, char * argv[])
{
    gROOT->SetBatch();

    if (argc < 2) {
        cout << "printMetaInfo input" << endl;
        return EXIT_SUCCESS;
    }

    fs::path input = argv[1];

    printMetaInfo(input);
    return EXIT_SUCCESS;
}


