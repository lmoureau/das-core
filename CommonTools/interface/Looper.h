#ifndef LOOPER_H
#define LOOPER_H

#include <algorithm>

#include <TString.h>
#include <TTree.h>
#include <TTreeReader.h>

namespace DAS {

////////////////////////////////////////////////////////////////////////////////
/// Facility to loop over a n-tuple, including parallelisation and printing
///
/// 1) open the TChain or TTree (directly or via TTreeReader)
/// 2) define `Looper(name, tree, nSplit, nNow)`
/// 3) loop over the tree with `while (looper.Next()`
/// 
/// and that's it!
class Looper {

    TString name; //!< used for output
    TTreeReader * reader; //!< reader (recommended, since faster)
    TTree * tree; //!< tree (not recommended, since slower)
    const int split, //!< number of processes
              now; //!< index of current process (starts from 0, à la C)
    const long long division; //!< prints progress at 100%/division 
    const bool jump;
    const std::pair<int,int> range; //!< covered entries by current process

    long long nEvSlice; // number of entries covered by the current process
    long long i; // current entry
    long long percent; // percentage of progress for current entry

    static std::pair<int, int> splitRange (long long N, int nSplit, int nNow); //!< deduces the range from the number of slices and number of entries
    //inline void Print () const; // print

public:

#define DEFAULT_DIV 10
#define DEFAULT_JUMP false
    Looper (TString n, TTreeReader * r, int nSplit, int nNow, long long Div = DEFAULT_DIV, bool Jump = DEFAULT_JUMP); //!< constructor with TTreeReader
    Looper (TString n, TTree * t      , int nSplit, int nNow, long long Div = DEFAULT_DIV, bool Jump = DEFAULT_JUMP); //!< constructor with TTree
    bool Next (); //!< "iterator"

};

}

#endif
