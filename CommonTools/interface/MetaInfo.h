#ifndef METAINFO_H
#define METAINFO_H

#include <TList.h>
#include <TString.h>
#include <TTree.h>

namespace DAS {

////////////////////////////////////////////////////////////////////////////////
/// Metainformation corresponds to global parameters of a n-tuple, such as flags,
/// or applied corrections.
///
/// In particular, it also contains the information on the different weights
/// and factors that are stored in `Event.weights`, `Jet.weights` and `Jet.JECs`.
/// 
/// It is stored in the `UserInfo` parameter of the `TTree` object, and it can be 
/// printed with the `printMetaInfo` utility.
class MetaInfo {

    TList * UserInfo;    //!< corresponds to object returned by `TTree:GetUserInfo()`
    TList * Flags,       //!< includes year, type (MC/data), and `n`-tuple version
          * Corrections, //!< currently applied corrections (e.g. JES, JER, etc.)
          * genEvWgts,   //!< description of the entries in gen-level event weights vector
          * recEvWgts,   //!< description of the entries in rec-level event weights vector
          * genJetWgts,  //!< description of the entries in gen-level jet weights vector
          * recJetWgts,  //!< description of the entries in rec-level jet weights vector
          * JECs;        //!< description of the entries in jet JECs vector

    ////////////////////////////////////////////////////////////////////////////////
    /// Private method to get the information from a tree.
    /// If it does not exist, it is created.
    TList * GetInfo
        (TList * UserInfo, //!< pointer to the list
         TString name);    //!< list name

    ////////////////////////////////////////////////////////////////////////////////
    /// Private method to retrieve the value corresponding to a name
    /// If it does not exist, it is created.
    template<typename T> T GetVal
        (TString name) const; //!< name of the value to retrieve

    ////////////////////////////////////////////////////////////////////////////////
    /// Private method to add a value in a list.
    void AddToList 
        (TList * l, TString name);

    ////////////////////////////////////////////////////////////////////////////////
    /// Given an index, get the name of the element of a list.
    TString GetName 
        (TList * l, int i) const;

public:

    ////////////////////////////////////////////////////////////////////////////////
    /// ~~Private~~ method to retrieve the value corresponding to a name 
    /// If it does not exist, it is created.
    // TODO: make it private!
    template<typename T> void SetVal
        (TString name, T value);

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor starting new UserInfo list
    MetaInfo (TTree * tree, bool isMC, int year, int version, float radius, int extra = 0);
    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor starting from an existent tree
    MetaInfo (TTree * tree);
    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor starting from an existent list
    MetaInfo (TList * List);

    ////////////////////////////////////////////////////////////////////////////////
    /// Print the whole content of the list
    void Print() const;

    ////////////////////////////////////////////////////////////////////////////////
    /// Add an element to the `Corrections` sublist
    void AddCorrection (TString correction);
    ////////////////////////////////////////////////////////////////////////////////
    /// Check if correction exists in the `Corrections` sublist
    bool HasCorrection (TString correction) const;

    ////////////////////////////////////////////////////////////////////////////////
    /// Get size of list of event weights
    int GetNGenEvWgts  () const;
    ////////////////////////////////////////////////////////////////////////////////
    /// Get size of list of event weights
    int GetNRecEvWgts  () const;
    ////////////////////////////////////////////////////////////////////////////////
    /// Get size of list of jet weights
    int GetNGenJetWgts () const; 
    ////////////////////////////////////////////////////////////////////////////////
    /// Get size of list of jet weights
    int GetNRecJetWgts () const; 
    ////////////////////////////////////////////////////////////////////////////////
    /// Get size of list of jet energy corrections
    int GetNJECs    () const;

    ////////////////////////////////////////////////////////////////////////////////
    /// Flag if MC or data
    bool isMC    () const;
    ////////////////////////////////////////////////////////////////////////////////
    /// Flag for year (both MC and data)
    int year () const;
    ////////////////////////////////////////////////////////////////////////////////
    /// Flag for version (corresponding to version on `pnfs`)
    /// Can be used to check compatibility.
    int  version () const;
    ////////////////////////////////////////////////////////////////////////////////
    /// Get jet algorithm (most likely ak4 or ak8)
    float  radius () const;
    ////////////////////////////////////////////////////////////////////////////////
    /// Flag for extra information (e.g. preVFP, postVFP, etc.)
    /// The code must be defined by the user.
    int extra () const;

    ////////////////////////////////////////////////////////////////////////////////
    /// Add an event weight
    void AddGenEvWgt  (TString name);
    ////////////////////////////////////////////////////////////////////////////////
    /// Add an event weight
    void AddRecEvWgt  (TString name);
    ////////////////////////////////////////////////////////////////////////////////
    /// Add a jet weight
    void AddGenJetWgt (TString name);
    ////////////////////////////////////////////////////////////////////////////////
    /// Add a jet weight
    void AddRecJetWgt (TString name);
    ////////////////////////////////////////////////////////////////////////////////
    /// Add a jet energy correction
    void AddJEC    (TString name);

    ////////////////////////////////////////////////////////////////////////////////
    /// Get an event weight
    TString GetGenEvWgt  (int i) const;
    ////////////////////////////////////////////////////////////////////////////////
    /// Get an event weight
    TString GetRecEvWgt  (int i) const;
    ////////////////////////////////////////////////////////////////////////////////
    /// Get a jet weight
    TString GetGenJetWgt (int i) const;
    ////////////////////////////////////////////////////////////////////////////////
    /// Get a jet weight
    TString GetRecJetWgt (int i) const;
    ////////////////////////////////////////////////////////////////////////////////
    /// Get a jet energy correction
    TString GetJEC    (int i) const;

    ////////////////////////////////////////////////////////////////////////////////
    /// Write list to file
    void Write () const;
};


}

#endif
