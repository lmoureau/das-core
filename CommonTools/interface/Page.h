#include <cassert>

#include <TROOT.h>
#include <TString.h>
#include <TCanvas.h>
#include <TH1.h>

//#include <iostream>
#include <functional>
using std::function;
#include <experimental/optional>
using std::experimental::optional;

struct Style {
    optional<TString> t; //<! title
    optional<Color_t> c; //<! color (line & marker)
    optional<Style_t> m, //<! marker style
                      l; //<! line style

    // default constructor
    Style (optional<TString> title  = {},
           optional<Color_t> color  = {}, 
           optional<Style_t> marker = {},
           optional<Style_t> line   = {}) :
        t(title), c(color), m(marker), l(line)
    { }

    // copy constructor
    Style (Style& temp) :
        t(temp.t), c(temp.c), m(temp.m), l(temp.l)
    { }

    //// overload of () operator makes inline copy of style with modifications
    //template<typename ...Args> Style operator() (Args... args)
    //{
    //    Style s(args...);
    //    if (!s.t && this->t) *(s.t);
    //    if (!s.c && this->c) *(s.c);
    //    if (!s.m && this->m) *(s.m);
    //    if (!s.l && this->l) *(s.l);
    //    return s;
    //}
};

// marker styles:
// https://root.cern.ch/doc/master/classTAttMarker.html
//     20    kFullCircle
//     21    kFullSquare
//     22    kFullTriangleUp
//     23    kFullTriangleDown
//     24    kOpenCircle
//     25    kOpenSquare
//     26    kOpenTriangleUp
//     27    kOpenDiamond
//     28    kOpenCross
//     29    kFullStar
//     30    kOpenStar

// line styles
// https://root.cern.ch/doc/master/classTAttLine.html
// kSolid      = 1
// kDashed     = 2
// kDotted     = 3
// kDashDotted = 4

static Style Pythia  ("Pythia 8", kRed  , {}, {});
static Style MadGraph("MadGraph", kBlue , {}, kDashed);
static Style Run2016 ("Run2016" , kBlack, kFullCircle, {});

struct Page {

    TCanvas * c; //!< canvas

    // for the style of the canvas, predefine `TStyle`s
    template<typename ...Args> Page (Args... args) :
        c(new TCanvas(args...))
    {
        // TODO: no stat, etc.
    }

    void Print ()
    {
        assert(c != nullptr);
        //gStyle->SetOptDate(12);
        // TODO: Git commit?
        // TODO: legend?

        // TODO: redraw axis, update frame, etc.
        const char * name = c->GetName(),
                   * title = c->GetTitle();
        c->Print(Form("%s.pdf", name),
                 Form("Title:%s", title));
        delete c;
        //std::cout << c << std::endl;
        c = nullptr;
    }
    void operator() () { Print(); }
    ~Page () 
    {
        if (c != nullptr) Print();
    }

    Page& operator() (TH1 * h, Style style, const Option_t * option = "")
    {
        assert(c != nullptr);
        assert(h != nullptr);
        if (style.t) h->SetTitle      (*(style.t));
        if (style.c) h->SetLineColor  (*(style.c));
        if (style.l) h->SetLineStyle  (*(style.l));
        if (style.c) h->SetMarkerColor(*(style.c));
        if (style.m) h->SetMarkerStyle(*(style.m));
        h->Draw(option);
        return *this;
    }

    // TODO: TF1, TGraph, ...

    Page& operator() (function<void(TCanvas * c)> func)
    {
        assert(c != nullptr);
        func(c);
        return *this;
    }
};

template<typename ... Args>
function<void(TCanvas * c)> PtStyle (bool logy, Args... args)
{
    return [logy,args...](TCanvas * c) {
        TH1 * f = c->DrawFrame(args...);
        c->SetLogx();
        f->GetXaxis()->SetMoreLogLabels();
        f->GetXaxis()->SetNoExponent();
        if (logy) c->SetLogy();
    };
}

