#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-variable"
namespace DAS {

    static const char * red       = "\x1B[31m\e[1m",
                      * green     = "\x1B[32m\e[1m",
                      * orange    = "\x1B[33m",
                      * bold      = "\e[1m",
                      * normal    = "\x1B[30m\e[0m",
                      * highlight = "\e[3m",
                      * underline = "\e[4m",
                      * def       = "\e[0m";

}
#pragma GCC diagnostic pop
