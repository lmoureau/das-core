#ifndef DAS_TOOLBOX
#define DAS_TOOLBOX

#include <vector>
#include <iostream>
#include <cassert>

#include <TList.h>
#include <TString.h>
#include <TCanvas.h>
#include <TChain.h>
#include <TParameter.h>

#include "Math/PtEtaPhiM4D.h"
#include "Math/Point3D.h"
#include "Math/VectorUtil.h"

#include "Core/Objects/interface/Jet.h"

namespace DAS {

inline bool pt_sort (const RecJet& j1, const RecJet& j2)
{
    return j1.CorrPt() > j2.CorrPt();
}

////////////////////////////////////////////////////////////////////////////////
/// Get (corrected) transverse momentum from DAS::RecJet object
[[ deprecated ]]
inline float Pt (const RecJet& jet)
{
    float pt = jet.p4.Pt(),
          corr = jet.JECs.front();
    return corr*pt;
}

////////////////////////////////////////////////////////////////////////////////
/// Get transverse momentum from DAS::GenJet object
/// This function is only useful so that one can use templates for RecJet and GenJet
/// indifferently.
[[ deprecated ]]
inline float Pt (const GenJet& jet)
{
    return jet.p4.Pt();
}

////////////////////////////////////////////////////////////////////////////////
/// Get rapidity from FourVector
inline float Rapidity (const FourVector& p4)
{
    float E = p4.E(), pz = p4.Pz();
    return 0.5*log((E+pz)/(E-pz));
}

////////////////////////////////////////////////////////////////////////////////
/// Get rapidity from DAS::{Gen,Rec}Jet object
template<typename Jet> [[ deprecated ]] inline float Rapidity (const Jet& j)
{
    return Rapidity(j.p4);
}

////////////////////////////////////////////////////////////////////////////////
/// Dijet Mass calculation 
FourVector operator+ (const FourVector& j1, const FourVector& j2)
{
    auto px = j1.Px() + j2.Px(),
         py = j1.Py() + j2.Py(),
         pz = j1.Pz() + j2.Pz(),
         E  = j1.E () + j2.E ();

    FourVector dijet;
    dijet.SetPxPyPzE(px, py, pz, E);
    return dijet;
}

////////////////////////////////////////////////////////////////////////////////
/// Dijet Mass calculation 
FourVector operator+ (const GenJet& j1, const GenJet& j2)
{
    assert(j1.weights.size() == j2.weights.size());
    return j1.p4 + j2.p4;
}

////////////////////////////////////////////////////////////////////////////////
/// Dijet Mass calculation 
///
/// TODO: find a way to specify JEC variation
FourVector operator+ (const RecJet& j1, const RecJet& j2)
{
    assert(j1.JECs.size() == j2.JECs.size());
    assert(j1.weights.size() == j2.weights.size());
    auto j1c(j1.p4), j2c(j2.p4);
    j1c.Scale(j1.JECs.front());
    j2c.Scale(j2.JECs.front());
    return j1c + j2c;
}

}

#endif
