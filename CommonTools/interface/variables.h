#ifndef DAS_VARIABLES
#define DAS_VARIABLES

#include <vector>
#include <map>
#include <functional>

#include "Math/PtEtaPhiM4D.h"
#include "Math/Point3D.h"
#include "Math/VectorUtil.h"

#include <TString.h>

namespace DAS {

using std::vector;

inline vector<TString> MakeTitle (const vector<double>& edges, const char * v, bool lowEdge, bool highEdge, std::function<const char*(double)> format)
{
    assert(edges.size() > 1);
    vector<TString> titles;
    for (size_t i = 1; i < edges.size(); ++i) {
        TString title;
        if (i >              1 ||  lowEdge) title += Form("%s < ", format(edges.at(i-1)));
        title += v;
        if (i < edges.size()-1 || highEdge) title += Form(" < %s", format(edges.at(i  )));
        titles.push_back(title);
    }
    return titles;
}

typedef ROOT::Math::PtEtaPhiM4D<float> FourVector;
typedef ROOT::Math::XYZPointF TriVector;
using ROOT::Math::VectorUtil::DeltaPhi;
using ROOT::Math::VectorUtil::DeltaR;

static const vector<double> pthat_edges {30,34,39,44,50,56,63,71,80,89,98,108,120,131,143,156,170,196,226,260,300,336,375,420,470,500,531,564,600,645,693,744,800,846,894,946,1000,1088,1183,1287,1400,1491,1587,1690,1800,1934,2078,2233,2400,2579,2771,2978,3200,3578,4000,4472,5000};

static const vector<double> pt_edges {15,18,21,24,28,32,37,43,49,56,64,74,84,97,114,133,153,174,196,220,245,272,300,330,362,395,430,468,507,548,592,638,686,737,790,846,905,967,1032,1101,1172,1248,1327,1410,1497,1588,1684,1784,1890,2000,2116,2238,2366,2500,2640,2787,2941,3103,3273,3450,3637,3832,4037/*,4252,4477,4713,4961,5220,5492,5777,6076,6389,6717,7000*/},
                            Mjj_edges {160,  200,  249,  306,  372,  449,  539, 641,  756,  887, 1029, 1187, 1361, 1556, 1769, 2008, 2273, 2572, 2915, 3306, 3754, 4244, 4805, 5374, 6094, 6908, 7861, 8929, 10050 },
                             y_edges {0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0},
                             n_edges {0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5};
static const int nYbins = y_edges.size()-1,
                 nPtBins = pt_edges.size()-1,
                 nMjjBins = Mjj_edges.size()-1,
                 maxMult = n_edges.size()-1;

//static const vector<const char*> yBins {"|y| < 0.5", "0.5 < |y| < 1.0", "1.0 < |y| < 1.5", "1.5 < |y| < 2.0", "2.0 < |y| < 2.5"/*, "2.5 < |y| < 3.0"*/};
static const vector<TString> yBins = MakeTitle(y_edges, "|y|", false, true, [](double v) { return Form("%.1f", v);} );

static const double ptmin = pt_edges.front(),
                    ptmax = pt_edges.back(),
                    ymin = y_edges.front(),
                    ymax = y_edges.back();

// binning for JES uncertainties (taken from JES files)
static const vector<double> eta_unc_edges = {/*-5.4, -5.0, -4.4, -4.0, -3.5,*/ -3.0, -2.8, -2.6, -2.4, -2.2, -2.0, -1.8, -1.6, -1.4, -1.2, -1.0, -0.8, -0.6, -0.4, -0.2, 0.0 , 0.2 , 0.4 , 0.6 , 0.8 , 1.0 , 1.2 , 1.4 , 1.6 , 1.8 , 2.0 , 2.2 , 2.4 , 2.6 , 2.8 , 3.0/*, 3.5 , 4.0 , 4.4 , 5.0 , 5.4*/};
static const int nEtaUncBins = eta_unc_edges.size()-1;

// binning for nominal JER values (taken from JERCProtoLab repo, /macros/common_info/common_binning.hpp, splitting them in negative and positive bins)
static const vector<double> eta_edges = { -5.191, -3.839, -3.489, -3.139, -2.964, -2.853, -2.650, -2.500, -2.322, -2.172, -2.043, -1.930, -1.740, -1.566, -1.305, -1.044, -0.783, -0.522, -0.261, 0.000, 0.261, 0.522, 0.783, 1.044, 1.305, 1.566, 1.740, 1.930, 2.043, 2.172, 2.322, 2.500, 2.650, 2.853, 2.964, 3.139, 3.489, 3.839, 5.191 };
static const int nEtaBins = eta_edges.size()-1;
static const vector<TString> etaBins = MakeTitle(eta_edges, "#eta", false, true, [](double v) { return Form("%.3f", v);});

// binning for JER request from JERC (taken from JERCProtoLab repo, /macros/common_info/common_binning.hpp)
static const vector<double> abseta_edges = { 0.000, 0.261, 0.522, 0.783, 1.044, 1.305, 1.566, 1.740, 1.930, 2.043, 2.172, 2.322, 2.500, 2.650, 2.853, 2.964, 3.139, 3.489, 3.839, 5.191 };
static const int nAbsEtaBins = abseta_edges.size()-1;
static const vector<TString> absetaBins = MakeTitle(abseta_edges, "|#eta|", false, true, [](double v) { return Form("%.3f", v);});

static const std::map<int,vector<double>> rho_edges = {
    { 2016, {0, 6.69, 12.39, 18.09, 23.79, 29.49, 35.19, 40.9, 999} }, // EOY16
    { 2017, {0, 7.47, 13.49, 19.52, 25.54, 31.57, 37.59, 999} }, // UL17
    { 2018, {0, 7.35, 13.26, 19.17, 25.08, 30.99, 36.9, 999} } // UL18
};

static const std::map<int,int> nRhoBins = {
    {2016, rho_edges.at(2016).size()-1 },
    {2017, rho_edges.at(2017).size()-1 },
    {2018, rho_edges.at(2018).size()-1 }
};

static const std::map<int,vector<TString>> rhoBins = {
    { 2016, MakeTitle(rho_edges.at(2016), "#rho", false, false, [](double v) { return Form("%.2f", v);}) },
    { 2017, MakeTitle(rho_edges.at(2017), "#rho", false, false, [](double v) { return Form("%.2f", v);}) },
    { 2018, MakeTitle(rho_edges.at(2018), "#rho", false, false, [](double v) { return Form("%.2f", v);}) }
};

// NB: actually, it's not rapidity but pseudorapidity, here.....

// bins used for unfolding (corresponds to binning in FastNLO tables for inclusive jet cross section)
static const vector<double> recBins{56,64,74,84,97,114,133,153,174,196,220,245,272,300,330,362,395,430,468,507,548,592,638,686,737,790,846,905,967,1032,1101,1172,1248,1327,1410,1497,1588,1684,1784,1890,2000,2116,2238,2366,2500,2640,2787,2941,3103,3273,3450,3637,3832};
static const vector<double> genBins{56,   74   ,97    ,133    ,174    ,220    ,272    ,330    ,395    ,468    ,548    ,638    ,737    ,846    ,967     ,1101     ,1248     ,1410     ,1588     ,1784     ,2000     ,2238     ,2500     ,2787     ,3103     ,3450,     3832};

static const int nRecBins = recBins.size()-1;
static const int nGenBins = genBins.size()-1;

static const int nPUbins = 150;

}


#endif
