#include "Core/CommonTools/interface/Looper.h"

#include <iostream>
#include <cassert>

using namespace std;
using namespace DAS;

pair<int, int> Looper::splitRange (long long N, int nSplit, int nNow)
{
    assert(N > 0);
    int nStart = N*((nNow+0.)/nSplit); //Included
    int nEnd   = N*((nNow+1.)/nSplit); //notIncluded
    return make_pair(nStart, nEnd);
}

Looper::Looper (TString n, TTreeReader * r, int nSplit, int nNow, long long Div, bool Jump) :
    name(n), reader(r), tree(nullptr), split(nSplit), now(nNow), division(Div), jump(Jump),
    range(splitRange(reader->GetEntries(true), split, now)),
    nEvSlice(range.second - range.first - 1), i(0), percent(-1)
{
    assert(reader);
    cout << name << ' ' << now << '/' << split << '\t' << range.first << '-' << range.second << '\t' << nEvSlice << endl;
    if (!jump)
        reader->SetEntriesRange(range.first, range.second);
}

Looper::Looper (TString n, TTree * t, int nSplit, int nNow, long long Div, bool Jump) :
    name(n), reader(nullptr), tree(t), split(nSplit), now(nNow), division(Div), jump(Jump),
    range(splitRange(tree->GetEntries(), split, now)),
    nEvSlice(range.second - range.first - 1), i(0), percent(-1)
{
    assert(tree);
    cout << name << ' ' << now << '/' << split << '\t' << nEvSlice << endl;
}

bool Looper::Next ()
{
    bool result = false;
    if (tree) {
        result = (i <= nEvSlice);
        if (result) {
            assert(tree != nullptr);
            static const long long N = tree->GetEntries();
            long long ientry = jump ? now + i*split : i+range.first;
            assert(ientry >= 0);
            assert(ientry < N);
            tree->GetEntry(ientry);
        }
    }
    else {
        assert(reader != nullptr);
        if (jump) {
            long long ientry = now + i*split;
            result = (reader->SetEntry(ientry) == TTreeReader::kEntryValid);
        }
        else
            result = reader->Next();
    }

    long long test_percent = (100ll*i)/nEvSlice;

    if (test_percent != percent && test_percent % division == 0) {
        percent = test_percent;
        cout << name << ' ' << now << '/' << split << '\t' << percent << '%' << endl;
    }
    ++i;

    return result;
}
