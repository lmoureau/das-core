#include "Core/CommonTools/interface/MetaInfo.h"
#include "Core/CommonTools/interface/terminal.h"

#include <iostream>
#include <cassert>

#include <TParameter.h>

using namespace std;
using namespace DAS;

TList * MetaInfo::GetInfo (TList * UserInfo, TString name)
{
    assert(UserInfo);
    TObject * obj = UserInfo->FindObject(name);
    if (obj)
        return static_cast<TList *>(obj);
    else {
        TList * l = new TList;
        l->SetName(name);
        UserInfo->Add(l);
        return l;
    }
}

void MetaInfo::AddToList (TList * l, TString name)
{
    int i = l->GetSize();
    auto parameter = new TParameter<int>(name, i, 'l');
    l->Add(parameter);
}

template<typename T> T MetaInfo::GetVal (TString name) const
{
    TObject * flag = Flags->FindObject(name);
    if (flag == nullptr) {
        cerr << red << "Flag " << name << " has not been found.\n" << normal;
        exit(EXIT_FAILURE);
    }
    auto parameter = dynamic_cast<TParameter<T>*>(flag);
    assert(parameter);
    return parameter->GetVal();
}

TString MetaInfo::GetName (TList * l, int i) const
{
    assert(l);
    TIter iter(l);
    TObject* object = nullptr;
    while ((object = iter())) {
        auto param = dynamic_cast<TParameter<int>*>(object);
        if (param->GetVal() == i)
            return param->GetName();
    }
    cerr << red << "Element " << i << " has not been found in list `" << l->GetName() << "`.\n" << normal;
    exit(EXIT_FAILURE);
}

MetaInfo::MetaInfo (TTree * tree, bool isMC, int year, int version, float radius, int extra) :
    UserInfo(tree->GetUserInfo()),
    Flags      (GetInfo(UserInfo, "Flags"      )),
    Corrections(GetInfo(UserInfo, "Corrections")),
    genEvWgts  (GetInfo(UserInfo, "genEvWgts"  )),
    recEvWgts  (GetInfo(UserInfo, "recEvWgts"  )),
    genJetWgts (GetInfo(UserInfo, "genJetWgts" )),
    recJetWgts (GetInfo(UserInfo, "recJetWgts" )),
    JECs       (GetInfo(UserInfo, "JECs"       ))
{
    cout << "Starting new UserInfo list" << endl;

    Flags->Clear();
    Corrections->Clear();
    genEvWgts->Clear();
    recEvWgts->Clear();
    genJetWgts->Clear();
    recJetWgts->Clear();
    JECs->Clear();

    Flags->Add(new TParameter<bool >("isMC"   , isMC   , 'l'));
    Flags->Add(new TParameter<int  >("year"   , year   , 'l'));
    Flags->Add(new TParameter<int  >("version", version, 'l'));
    Flags->Add(new TParameter<float>("radius" , radius , 'l'));
    Flags->Add(new TParameter<int  >("extra"  , extra  , 'l'));

    genEvWgts ->Add(new TParameter<int>("nominal", 0, 'l'));
    recEvWgts ->Add(new TParameter<int>("nominal", 0, 'l'));
    genJetWgts->Add(new TParameter<int>("nominal", 0, 'l'));
    recJetWgts->Add(new TParameter<int>("nominal", 0, 'l'));
    JECs      ->Add(new TParameter<int>("nominal", 0, 'l'));
}

MetaInfo::MetaInfo (TTree * tree) :
    UserInfo(tree->GetUserInfo()),
    Flags      (GetInfo(UserInfo, "Flags"      )),
    Corrections(GetInfo(UserInfo, "Corrections")),
    genEvWgts  (GetInfo(UserInfo, "genEvWgts"  )),
    recEvWgts  (GetInfo(UserInfo, "recEvWgts"  )),
    genJetWgts (GetInfo(UserInfo, "genJetWgts" )),
    recJetWgts (GetInfo(UserInfo, "recJetWgts" )),
    JECs       (GetInfo(UserInfo, "JECs"       ))
{
    cout << "Opening existing UserInfo list" << endl;
}

MetaInfo::MetaInfo (TList * List) :
    UserInfo(List),
    Flags      (GetInfo(UserInfo, "Flags"      )),
    Corrections(GetInfo(UserInfo, "Corrections")),
    genEvWgts  (GetInfo(UserInfo, "genEvWgts"  )),
    recEvWgts  (GetInfo(UserInfo, "recEvWgts"  )),
    genJetWgts (GetInfo(UserInfo, "genJetWgts" )),
    recJetWgts (GetInfo(UserInfo, "recJetWgts" )),
    JECs       (GetInfo(UserInfo, "JECs"       ))
{
    cout << "Opening detached UserInfo list" << endl;
}

void MetaInfo::Print() const
{
    TIter iter(UserInfo);
    TObject* object = nullptr;
    while ((object = iter()))
        (dynamic_cast<TList *>(object))->Print();
}

void MetaInfo::Write () const
{
    TIter iter(UserInfo);
    TObject* object = nullptr;
    while ((object = iter()))
        (dynamic_cast<TList *>(object))->Write();
}

bool MetaInfo::HasCorrection (TString correction) const
{
    TIter iter(Corrections);
    TObject* object = nullptr;
    while ((object = iter())) {
        TString name = object->GetName();
        if (name.Contains(correction)) return true;
    }
    return false;
}

void MetaInfo::AddCorrection (TString correction) { AddToList(Corrections, correction); }

int MetaInfo::GetNGenEvWgts  () const { return genEvWgts ->GetSize(); }
int MetaInfo::GetNRecEvWgts  () const { return recEvWgts ->GetSize(); }
int MetaInfo::GetNGenJetWgts () const { return genJetWgts->GetSize(); } 
int MetaInfo::GetNRecJetWgts () const { return recJetWgts->GetSize(); } 
int MetaInfo::GetNJECs       () const { return JECs      ->GetSize(); }

bool  MetaInfo::isMC    () const { return GetVal<bool >("isMC"   ); }
int   MetaInfo::year    () const { return GetVal<int  >("year"   ); }
int   MetaInfo::version () const { return GetVal<int  >("version"); }
float MetaInfo::radius  () const { return GetVal<float>("radius" ); }
int   MetaInfo::extra   () const { return GetVal<int  >("extra"  ); }

void MetaInfo::AddGenEvWgt  (TString name) { AddToList(genEvWgts , name); }
void MetaInfo::AddRecEvWgt  (TString name) { AddToList(recEvWgts , name); }
void MetaInfo::AddGenJetWgt (TString name) { AddToList(genJetWgts, name); }
void MetaInfo::AddRecJetWgt (TString name) { AddToList(recJetWgts, name); }
void MetaInfo::AddJEC       (TString name) { AddToList(JECs      , name); }

TString MetaInfo::GetGenEvWgt  (int i) const { return GetName(genEvWgts ,i); }
TString MetaInfo::GetRecEvWgt  (int i) const { return GetName(recEvWgts ,i); }
TString MetaInfo::GetGenJetWgt (int i) const { return GetName(genJetWgts,i); }
TString MetaInfo::GetRecJetWgt (int i) const { return GetName(recJetWgts,i); }
TString MetaInfo::GetJEC       (int i) const { return GetName(JECs      ,i); }
