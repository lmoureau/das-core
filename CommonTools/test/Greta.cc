#include "Core/CommonTools/interface/Greta.h"

#include <vector>

#include <TROOT.h>
#include <TCanvas.h>
#include <TF1.h>

using namespace std;
using namespace DAS;

int main ()
{
    const vector<double> p {0.1,0.1,0.1};
    const size_t n = 2;

    double m = 1, M = 2;

    Greta g(n,m,M);
    Thunberg<n> ch(m,M);
    TF1 * f = new TF1("f", g, m, M, p.size());
    TF1 * f2 = new TF1("f2", ch, m, M, p.size());
    f->SetParameters(p.data());
    f2->SetParameters(p.data());
    f2->SetLineColor(kBlue);
    f2->SetLineStyle(2);
    new TCanvas;
    f->Draw();
    f2->Draw("same");
    gPad->Print("Greta.pdf");

    return 0;
}
