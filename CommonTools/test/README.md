# Tests

After compilation, executables are in `$CMSSW_BASE/test/$SCRAM_ARCH".

In principle, one can run them automatically at each compilation with CMSSW (see [TWiki](https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideDevelopersGuide#Add_tests_to_your_package)), but this is presently not the case.

## Toy

Dummy example to test the Toy MC approach.

## Greta

Test and compare functions from Greta and from Thunberg and compare the results.
