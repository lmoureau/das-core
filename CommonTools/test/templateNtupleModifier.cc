#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <filesystem>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/terminal.h"
#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"

#include <TROOT.h>
#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>

#include "Math/VectorUtil.h"

using namespace std;
using namespace DAS;

namespace fs = filesystem;
////////////////////////////////////////////////////////////////////////////////
/// Template for function (TODO)
void NtupleModifier 
             (const fs::path& input,  //!< name of input root file 
              const fs::path& output, //!< name of output root file
              /* TODO: add all arguments you may need */
              int nSplit = 1, //!< number of jobs/tasks/cores
              int nNow = 0)   //!< index of job/task/core
{
    assert(fs::exists(input));

    auto oldchain = new TChain("inclusive_jets"); // arg = path inside the ntuple
    oldchain->Add(input.c_str());

    if (fs::exists(output))
        cerr << red << output << " will be overwritten!\n" << normal;
    auto file = TFile::Open(output.c_str(), "RECREATE");
    auto newtree = oldchain->CloneTree(0);

    MetaInfo metainfo(newtree);
    if (nNow == 0) metainfo.Print();
    metainfo.AddCorrection("NtupleModifier");
    bool isMC = metainfo.isMC();

    vector<RecJet> * recJets = nullptr;
    oldchain->SetBranchAddress("recJets", &recJets);
    vector<GenJet> * genJets = nullptr;
    if (isMC)
        oldchain->SetBranchAddress("genJets", &genJets);
    // TODO: load any branch you may need

    Looper looper(__func__, oldchain, nSplit, nNow);
    while (looper.Next()) {

        // TODO: modify the branches

        newtree->Fill();
    }
    file->Write();
    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 3) {
        cout << argv[0] << " input output [nSplit [nNow]]\n"
             << "where\tinput = ...\n" // TODO: fill
             << "     \toutput = ...\n" // TODO: fill
             << flush;
        return EXIT_SUCCESS;
    }

    fs::path input = argv[1],
             output = argv[2];
    int nNow = 0, nSplit = 1;
    if (argc > 3) nSplit = atoi(argv[3]);
    if (argc > 4) nNow = atoi(argv[4]);

    NtupleModifier(input, output, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
