#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <filesystem>

#include <TROOT.h>
#include <TString.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>

#include "Core/CommonTools/interface/terminal.h"

#include "Math/VectorUtil.h"

using namespace std;
using namespace DAS;

namespace fs = filesystem;
////////////////////////////////////////////////////////////////////////////////
/// Template for function (TODO)
void SimpleExec 
             (const fs::path& input,  //!< name of input file 
              const fs::path& output) //!< name of output file
{
    assert(fs::exists(input));

    auto source = TFile::Open(input.c_str(), "READ");

    source->Close();

    if (fs::exists(output))
        cerr << red << output << " will be overwritten!\n" << normal;
    auto file = TFile::Open(output.c_str(), "RECREATE");

    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 3) {
        cout << argv[0] << " input output [nSplit [nNow]]\n"
             << "where\tinput = ...\n" // TODO: fill
             << "     \toutput = ...\n" // TODO: fill
             << flush;
        return EXIT_SUCCESS;
    }

    fs::path input = argv[1],
         output = argv[2];

    SimpleExec(input, output);
    return EXIT_SUCCESS;
}
#endif

