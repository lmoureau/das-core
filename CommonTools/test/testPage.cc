#include <cstdlib>
#include <cmath>

#include <iostream>

#include "Core/CommonTools/interface/Page.h"
#include "Core/CommonTools/interface/variables.h"

using namespace std;
using namespace DAS;

int main ()
{
    // create dummy histograms
    TH1 * h1 = new TH1D("h1", "h1", nPtBins, pt_edges.data());
    for (int i = 1; i <= nPtBins; ++i) {
        double pt = h1->GetXaxis()->GetBinCenter(i);
        h1->SetBinContent(i, 1e8*pow(pt,-5));
    }
    TH1 * h2 = dynamic_cast<TH1 *>(h1->Clone("h2"));
    h2->Scale(0.1);

    // testing Page, a one-liner plotting class
    Page("testPage", "Test Page")(PtStyle(true, 74, 1e-8, 3000, 1, ";p_{T};#sigma"))
                                 (h1, Pythia  , "same")
                                 (h2, MadGraph, "same")
                                 ();

    return EXIT_SUCCESS;
}
