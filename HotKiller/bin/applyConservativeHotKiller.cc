#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <algorithm>
#include <filesystem>

#include "Core/CommonTools/interface/terminal.h"
#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"
#include "Core/CommonTools/interface/ControlPlots.h"

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include <TROOT.h>
#include <TChain.h>
#include <TH2D.h>
#include <TProfile.h>

#include "Math/VectorUtil.h"

#include "Core/HotKiller/interface/applyConservativeHotKiller.h"

using namespace DAS;
using namespace std;
namespace fs = filesystem;

////////////////////////////////////////////////////////////////////////////////
/// Remove regions that are not properly simulated both in data and simulation
/// (fixed via unfolding at the end).
///
/// Maps are derived by Hannu (Helsinki), and should be downloaded and adapted.
void applyHotKiller 
             (const fs::path& input,  //!< name of input root file 
              const fs::path& output, //!< name of output root file
              const fs::path& hotmap, //!< 2D hist with maps as efficiency
              int nSplit = 1, //!< number of jobs/tasks/cores
              int nNow = 0)   //!< index of job/task/core
{
    // Get old file, old tree and set top branch address
    TChain * oldchain = new TChain("inclusive_jets"); // arg = path inside the ntuple
    oldchain->Add(input.c_str());

    if (fs::exists(output))
        cerr << red << "Overwriting existing output file\n" << normal;
    TFile * file = TFile::Open(output.c_str(), "RECREATE");
    TTree * newtree = oldchain->CloneTree(0);

    MetaInfo metainfo(newtree);
    if (nNow == 0) metainfo.Print();
    metainfo.AddCorrection("Jet veto maps");
    bool isMC = metainfo.isMC();
    assert(exists(hotmap));
    ConservativeVeto hotkiller(hotmap.c_str());

    Event * event = nullptr;
    oldchain->SetBranchAddress("event", &event);

    vector<RecJet> * recjets = nullptr;
    vector<FourVector> * hltjets = nullptr;
    oldchain->SetBranchAddress("recJets", &recjets);
    if (!isMC)
        oldchain->SetBranchAddress("hltJets", &hltjets);

    file->cd();

    ControlPlots raw("raw");
    ControlPlots nominal("HotKillers");

    auto totRecWgt = [&](size_t i) {
        return (isMC ? event->genWgts.front() : 1) * event->recWgts.at(i);
    };
    Looper looper(__func__, oldchain, nSplit, nNow);
    while (looper.Next()) {

        raw(*recjets, totRecWgt(0));

        hotkiller(recjets);
        if (!isMC)
            hotkiller(hltjets);
        
        nominal(*recjets, totRecWgt(0));
        
        newtree->Fill();
    }

    /* close */
    newtree->Write();
    hotkiller.Write(file);
    raw.Write(file);
    nominal.Write(file);

    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 4) {
        cout << argv[0] << " input output efficiency [nSplit [nNow]]" << endl;
        return EXIT_SUCCESS;
    }

    fs::path input = argv[1],
             output = argv[2],
             hotmap = argv[3];

    int nNow = 0, nSplit = 1;
    if (argc > 4) nSplit = atoi(argv[4]);
    if (argc > 5) nNow = atoi(argv[5]);

    applyHotKiller(input, output, hotmap, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
