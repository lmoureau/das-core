#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <algorithm>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"

#include <TROOT.h>
#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TProfile.h>

#include "Math/VectorUtil.h"

using namespace std;
using namespace DAS;

//    if      (run >= 304911) return "17runF";
//    else if (run >= 303435) return "17runE";
//    else if (run >= 302030) return "17runD";
//    else if (run >= 299337) return "17runC";
//    else if (run >= /*297179*/297000) return "17runB";
//    else if (273158 <= run && run <= 275376) return "runB";
//    else if (275657 <= run && run <= 276283) return "runC";
//    else if (276315 <= run && run <= 276811) return "runD";
//    else if (276831 <= run && run <= 277420) return "runE";
//    else if (277981 <= run && run <= 278801) return "runFe";
//    else if (278802 <= run && run <= 278808) return "runFl";
//    else if (278820 <= run && run <= 280385) return "runG";
//    else if (281613 <= run && run <= 284044) return "runH";

////////////////////////////////////////////////////////////////////////////////
/// Template for function
void applyHotKillerDijets 
             (TString input,  //!< name of input root file 
              TString output, //!< name of output root file
              TString hotmap, //!< 2D hist with maps as efficiency
              int nSplit = 1, //!< number of jobs/tasks/cores
              int nNow = 0)   //!< index of job/task/core
{
    // Get old file, old tree and set top branch address
    TChain * oldchain = new TChain("inclusive_jets"); // arg = path inside the ntuple
    oldchain->Add(input);

    Event * event = nullptr;
    oldchain->SetBranchAddress("event", &event);

    vector<RecJet> * jets = nullptr;
    oldchain->SetBranchAddress("recJets", &jets);

    TFile * file = TFile::Open(output, "RECREATE");
    TTree * newtree = oldchain->CloneTree(0);

    MetaInfo metainfo(newtree);
    int year = metainfo.year();
    if (nNow == 0) metainfo.Print();
    metainfo.AddCorrection("applyHotKiller");

    assert(year != 2018);

    // get hot map
    
    cout << hotmap << endl;
    TFile * f_hotmap = TFile::Open(hotmap, "READ");
    TH2 * h = dynamic_cast<TH2*>(f_hotmap->Get("h2hot2"));  
    cout << h << endl;
    h->SetDirectory(0);
    assert(h->Integral() > 0);
    f_hotmap->Close();

    file->cd();

    Looper looper(__func__, oldchain, nSplit, nNow);
    while (looper.Next()) {

        //double ev_w = event->weights.front();
        if(jets->size() < 2) continue; //get only dijet events

        FourVector p1 = jets->at(0).p4;
        FourVector p2 = jets->at(1).p4;
        
        //JEC
        p1.Scale(jets->at(0).JECs.front());
        p2.Scale(jets->at(1).JECs.front());

        int ibin1 = h->FindBin(p1.Eta(),p1.Phi());
        int ibin2 = h->FindBin(p2.Eta(),p2.Phi());

        float eff1 = h->GetBinContent(ibin1);
        float eff2 = h->GetBinContent(ibin2);

        assert((eff1 == 0. || eff1 == 1. ) and (eff2 == 0. || eff2 == 1. ));

        if (eff1 == 0. or eff2 == 0.){  //if leading or subleading jet belong to a hotzone then rm all the recjets from the event
        
            for (auto jet = jets->begin(); jet != jets->end(); /* nothing */ ){

               assert(jet->weights.size() == 1 && jet->weights.back() == 1);   
               jets->erase(jet);
        
               continue;
            }
        }

        newtree->Fill();
    }

    /* close */
    newtree->Write();

    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 4) {
        cout << argv[0] << " input output efficiency [nSplit [nNow]]" << endl;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2],
            hotmap = argv[3];

    int nNow = 0, nSplit = 1;
    if (argc > 4) nSplit = atoi(argv[4]);
    if (argc > 5) nNow = atoi(argv[5]);

    applyHotKillerDijets(input, output, hotmap, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
