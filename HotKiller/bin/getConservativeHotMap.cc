#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include <TROOT.h>
#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TProfile.h>

#include "Math/VectorUtil.h"

using namespace std;
using namespace DAS;

////////////////////////////////////////////////////////////////////////////////
/// Maps the JetMET veto maps onto maps containing weights
void getConservativeHotMap 
             (TString input,  //!< name of input root file 
              TString output, //!< name of output root file
              const char* hname)
{
    /// Opening source and checking that it is MC
    TFile * source = TFile::Open(input, "READ");
    TH2 * h = dynamic_cast<TH2*>(source->Get(hname));
    h->SetDirectory(0);
    source->Close();

    TFile * file = TFile::Open(output, "RECREATE");

    static const auto deps = numeric_limits<double>::epsilon();

    for (int x = 1; x <= h->GetNbinsX(); ++x)
    for (int y = 1; y <= h->GetNbinsY(); ++y) {
        double content = h->GetBinContent(x,y);
        content = (content < deps); // 0 or 1, can then be used as an efficiency
        h->SetBinContent(x,y,content);
    }
    h->GetXaxis()->SetTitle("#eta");
    h->GetYaxis()->SetTitle("#phi");
    h->GetZaxis()->SetRangeUser(0,1);
    h->Write("h2hot2");

    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 3) {
        cout << argv[0] << " input output hname\n"
             << "where\tinput = hot maps from JetMET\n"
             << "     \toutput = name of conservative map, where bin entries can be used as efficiencies\n"
             << "     \thname = name of map in input file\n"
             << flush;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2];

    const char * hname = argv[3];

    getConservativeHotMap(input, output, hname);
    return EXIT_SUCCESS;
}
#endif

