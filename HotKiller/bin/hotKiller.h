#ifndef hotKiller
#define hotKiller

#include <cassert>
#include <algorithm>
#include <vector>
#include <map>

#include <TString.h>
#include <TH2D.h>

#include "Core/Objects/interface/Jet.h"

using namespace std;


const TString hotPath = "/nfs/dust/cms/user/zlebcr/DAS18/hotMaps";



inline TString getMapTag(int run)
{
    if      (run >= 304911) return "17runF";
    else if (run >= 303435) return "17runE";
    else if (run >= 302030) return "17runD";
    else if (run >= 299337) return "17runC";
    else if (run >= /*297179*/297000) return "17runB";
    else if (273158 <= run && run <= 275376) return "runB";
    else if (275657 <= run && run <= 276283) return "runC";
    else if (276315 <= run && run <= 276811) return "runD";
    else if (276831 <= run && run <= 277420) return "runE";
    else if (277981 <= run && run <= 278801) return "runFe";
    else if (278802 <= run && run <= 278808) return "runFl";
    else if (278820 <= run && run <= 280385) return "runG";
    else if (281613 <= run && run <= 284044) return "runH";
    else {
        cout << "Wrong run " << endl;
        exit(1);
        return "x";
    }
}





//Remove hot points from the data
void ApplyHotKiller(int runNo,  vector<DAS::RecJet> &jets)
{
    static map<TString,TH2D *> hotHistMap;

    if (hotHistMap.size() == 0) {
        for(TString per : {"runB", "runC", "runD", "runE", "runFe", "runFl", "runG", "runH",
                           "17runB", "17runC", "17runD", "17runE", "17runF"  } ) {
            TFile *hotFile = TFile::Open(hotPath+"/hotjets-"+per+".root");
            assert(hotFile);
            hotHistMap[per] = dynamic_cast<TH2D*>(hotFile->Get("h2hotfilter"));
            assert(hotHistMap.at(per));
        }
    }

    TString per = getMapTag(runNo);
    TH2D *hotHist = hotHistMap.at(per);

    jets.erase(remove_if(jets.begin(), jets.end(),
                [&](DAS::RecJet &jet){
                    int ibin  = hotHist->FindBin(jet.p4.Eta(), jet.p4.Phi());
                    double val = hotHist->GetBinContent(ibin);
                    return (val == 0); //remove if val == 0
                }),
            jets.end());
}

#endif
