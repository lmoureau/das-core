#include <cassert>
#include <limits>
#include <vector>

#include <TString.h>
#include <TFile.h>
#include <TH2.h>

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/Objects/interface/Jet.h"

static const auto feps = std::numeric_limits<float>::epsilon();

namespace DAS {

////////////////////////////////////////////////////////////////////////////////
/// Check jets reconstructed in hot regions of the detector and give them a 
/// weight of zero (previously, they were just removed from the list, which is 
/// fine for inclusive jet and b jet analyses, but which screws up multi-jet
/// analyses).
///
/// We apply a conservative apporach in this code:
/// one could refine a bit the code to apply it including time
/// dependence (with a weight representative of the luminosity of the
/// era), but the effect being very fine for 2016 and only in the
/// tracker acceptance, we have not considered it 
struct ConservativeVeto {

    TH2 * h; //!< jet veto map

    ConservativeVeto (const char * hotmap) :
        h(nullptr)
    {
        auto f = TFile::Open(hotmap, "READ");
        h = dynamic_cast<TH2*>(f->Get("h2hot2"));
        h->SetDirectory(0);
        assert(h->Integral() > 0);
        f->Close();
    }

    void operator() (std::vector<DAS::FourVector> * hltjets)
    {
        for (auto hltjet = hltjets->begin(); hltjet != hltjets->end();) {
            int ibin  = h->FindBin(hltjet->Eta(), hltjet->Phi());
            float eff = h->GetBinContent(ibin);
            assert(std::abs(eff) < feps || std::abs(eff - 1) < feps);
            if (abs(eff) < feps ) 
               hltjets->erase(hltjet);
            else 
                ++hltjet;
        }
    }

    void operator() (std::vector<DAS::RecJet> * jets)
    {
        for (auto& jet: *jets) {

            // sanity check
            assert(jet.weights.size() == 1);
            auto& jet_w = jet.weights.front();

            int ibin  = h->FindBin(jet.p4.Eta(), jet.p4.Phi()); // TODO: CorrEta?
            float eff = h->GetBinContent(ibin);
            assert(std::abs(eff) < feps || std::abs(eff - 1) < feps);

            jet_w *= eff;
        }
    }

    void Write (TDirectory * dir)
    {
        dir->cd();
        h->SetDirectory(dir);
        h->Write("jetvetomap");
    }

};

}
