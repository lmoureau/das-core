# Jet Energy Corrections

[General TWiki](https://twiki.cern.ch/twiki/bin/view/CMS/JetMET)

Definition of levels in
[FactorizedJetCorrectorCalculator](https://github.com/cms-sw/cmssw/blob/master/CondFormats/JetMETObjects/src/FactorizedJetCorrectorCalculator.cc#L79).

## Jet Energy Scale with `applyJEScorrections`

Apply the JES corrections and produce a new *n*-tuple.
It uses `JES.h`, implementing a class to read one JES tables.

## Jet Energy Resolution with `applyJERsmearing`

Apply the JER smearing and produce a new *n*-tuple.
It uses `JER.h`, implementing a class to read one JER tables.

Another executable `getJERcurves` just plots the resolution curves and can be
applied on any *n*-tuple.

[Dedicated TWiki](https://twiki.cern.ch/twiki/bin/viewauth/CMS/JetResolution#Smearing_procedures)
