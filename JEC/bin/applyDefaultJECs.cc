#include <cstdlib>
#include <string>
#include <iostream>
#include <filesystem>

#include <TChain.h>
#include <TFile.h>
#include <TROOT.h>
#include <TH2.h>
#include <TRegexp.h>

#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Jet.h"

#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"
#include "Core/CommonTools/interface/toolbox.h"

#include "Core/JEC/interface/JEShandle.h"
#include "Core/JEC/interface/JESreader.h"

#include "applyJERsmearing.h"
#include "applyJEScorrections.h"

using namespace std;
using namespace DAS;

namespace fs = filesystem;

string GetTag (int year)
{
    switch (year) {
        case 2018: return "Summer19UL18_JRV2";
        case 2017: return "Fall17_V3b";
        case 2016: return "Summer16_25nsV1";
        default: exit(EXIT_FAILURE);
    }
}

////////////////////////////////////////////////////////////////////////////////
/// apply JES corrections to data and MC *n*tuples, as well as uncertainties
/// in data (all sources are considered separately)
void applyDefaultJECs
            (TString input,     //!< name of input root file containing the *n*-tuple
             TString output,    //!< name of output root file containing the histograms
             int nSplit = 1,    //!< number of jobs/tasks/cores
             int nNow = 0)      //!< index of job/task/core
{                               
    TChain * oldchain = new TChain("inclusive_jets"); 
    oldchain->Add(input);

    TFile *newfile = TFile::Open(output, "RECREATE");
    TTree *newtree = oldchain->CloneTree(0);

    // declaring meta information
    MetaInfo metainfo(newtree);
    bool isMC = metainfo.isMC();
    if (nNow == 0) metainfo.Print();
    metainfo.AddCorrection("JES");
    if (isMC)
        metainfo.AddCorrection("JER");

    // setting up branches
    Event * evnt = nullptr;
    PileUp * pu = nullptr;
    oldchain->SetBranchAddress("event", &evnt);
    oldchain->SetBranchAddress("pileup", &pu);
    vector<GenJet> * genJets = nullptr;
    if (isMC)
        oldchain->SetBranchAddress("genJets", &genJets);
    vector<RecJet> * recJets = nullptr;
    oldchain->SetBranchAddress("recJets", &recJets);

    // initialise JES
    JEShandle handle(input, metainfo);
    applyJEScorrectionsFunctor applyJES(handle, isMC);

    // initialise JER
    applyJERsmearingFunctor * applyJER = nullptr;
    if (isMC) {
        fs::path JERtables = getenv("DAS_WORKAREA");
        JERtables /= "tables/JER"; // further automation already in `JER.h`
        assert(fs::exists(JERtables));
        string tag = GetTag(metainfo.year());
        int radius = metainfo.radius();
        applyJER = new applyJERsmearingFunctor(JERtables.c_str(), tag, radius, nNow /*seed*/);
    }

    // adding unc to meta info & control plots
    vector<TString> sources = JESreader::getJECUncNames();
    if (isMC) {
        metainfo.AddJEC("JERup");
        metainfo.AddJEC("JERdown");
    }
    else {
        for (TString source: sources) {
            metainfo.AddJEC(source + "up");
            metainfo.AddJEC(source + "down");
        }
    }

    Looper looper(__func__, oldchain, nSplit, nNow);
    while (looper.Next()) {

        applyJES(*evnt, *pu, *recJets);
        if (isMC)
            (*applyJER)(*evnt, *pu, *recJets, *genJets);

        sort(recJets->begin(), recJets->end(), pt_sort); // sort again the jets by pt
        newtree->Fill();
    }

    /* closing */
    newtree->AutoSave();

    TDirectory * JEScontrolplots = newfile->mkdir("JES");
    applyJES.Write(JEScontrolplots);

    newfile->cd();
    newfile->Close();
    delete oldchain;
    cout << "Done " << output << endl;
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 4) {
        cout << argv[0] << " input output [nSplit [nNow]]\n"
             << "\twhere\tinput = n-tuple\n"
             << "\t     \toutput = new n-tuple\n" << flush;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2];

    int nNow = 0, nSplit = 1;
    if (argc > 3) nSplit = atoi(argv[3]);
    if (argc > 4) nNow   = atoi(argv[4]);

    applyDefaultJECs(input, output, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif

