#include <cstdlib>
#include <iostream>

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH2D.h>

#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"
#include "Core/CommonTools/interface/toolbox.h"

#include "Core/JEC/interface/JER.h"

#include "applyJERsmearing.h"
#include "Core/CommonTools/interface/ControlPlots.h"

#include <filesystem>

using namespace std;
using namespace DAS;

namespace fs = filesystem;

////////////////////////////////////////////////////////////////////////////////
/// Get JER smearing corrections in the form of factors to store in the `JECs`
/// member of `RecJet`.
void applyJERsmearing
            (TString input,   //!< name of input root file containing the *n*-tuple
             TString output,  //!< name of output root file containing the histograms
             const fs::path& JERtables,  //!< path to JERtables
             string tag,
             JER::Mode mode = JER::Mode::scalingAllMatched, //!< smearing method
             int nSplit = 1,  //!< number of jobs/tasks/cores
             int nNow = 0)    //!< index of job/task/core
{
    //Get old file, old tree and set top branch address
    TChain * oldchain = new TChain("inclusive_jets"); // arg = path inside the ntuple
    oldchain->Add(input);

    Event * evnt = nullptr;
    PileUp * pu = nullptr;
    oldchain->SetBranchAddress("event", &evnt);
    oldchain->SetBranchAddress("pileup", &pu);

    vector<GenJet> * genJets = nullptr;
    vector<RecJet> * recJets = nullptr;
    oldchain->SetBranchAddress("genJets", &genJets);
    oldchain->SetBranchAddress("recJets", &recJets);

    // saving MC weight 
    TFile *newfile = TFile::Open(output, "RECREATE");
    TTree *newtree = oldchain->CloneTree(0);

    MetaInfo metainfo(newtree);
    if (nNow == 0) metainfo.Print();
    assert(metainfo.isMC());

    const size_t N = metainfo.GetNJECs();
    assert(N == 1);

    float radius = metainfo.radius();

    metainfo.AddCorrection("JER");
    metainfo.AddJEC("JERup");
    metainfo.AddJEC("JERdown");

    assert(fs::exists(JERtables));
    cout << "Starting" << endl;
    applyJERsmearingFunctor applyJER(JERtables.c_str(), tag, radius, nNow /* seed */, mode);

    ControlPlots::isMC = metainfo.isMC();
    ControlPlots raw("raw");
    vector<ControlPlots> calib { ControlPlots("nominal"),
                                 ControlPlots("upper"  ),
                                 ControlPlots("lower"  ) };

    Looper looper(__func__, oldchain, nSplit, nNow);
    while (looper.Next()) {
        raw(*genJets, evnt->genWgts.front());
        raw(*recJets, evnt->genWgts.front() * evnt->recWgts.front());

        applyJER(*evnt, *pu, *recJets, *genJets);

        sort(recJets->begin(), recJets->end(), pt_sort);

        for (size_t i = 0; i < calib.size(); ++i) {
            calib.at(i)(*genJets, evnt->genWgts.front());
            calib.at(i)(*recJets, evnt->genWgts.front() * evnt->recWgts.front(), i);
        }

        newtree->Fill();
    }

    /* closing */
    newtree->AutoSave();

    newfile->cd();

    raw.Write(newfile);
    for (size_t i = 0; i < calib.size(); ++i)
        calib.at(i).Write(newfile);

    newfile->Close();
    delete oldchain;
    cout << "Done " << output << endl;
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 6) {
        cout << argv[0] << " input output JERtables [nSplit [nNow]]\n"
             << "\twhere\tinput = n-tuple\n"
             << "\t     \toutput = new n-tuple\n"
             << "\t     \tJERtables = directory for JER tables\n"
             << "\t     \ttag = sth like SummerXXX (check JetMET recommendations)\n"
             << "\t     \tmode = stochasticOnly, scalingAllMatched, scalingCoreOnly\n"
             << "Note:\n"
             << " - `stochasticOnly` means stochastic smearing regardless of matching (dangerous as relying blindly on resolutions measured by JetMET);\n"
             << " - `scalingAllMatched` means stochastic only if there is no matching (dangerous in the tails);\n"
             << " - `scalingCoreOnly` means scaling only in the core of the resolution (recommended)\n"
             << flush;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2];
    fs::path JERtables = argv[3];
    string tag = argv[4];
    JER::Mode mode = JER::GetEnumMode(argv[5]);

    int nNow = 0, nSplit = 1;
    if (argc > 6) nSplit = atoi(argv[6]);
    if (argc > 7) nNow   = atoi(argv[7]);

    applyJERsmearing(input, output, JERtables, tag, mode, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
