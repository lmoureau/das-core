#include <vector>
#include <algorithm>
#include <functional>
#include <limits>
#include <cassert>

#include <TString.h>
#include <TH2.h>
#include <TDirectory.h>

#include "Core/CommonTools/interface/toolbox.h"

#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Jet.h"

#include "Core/JEC/interface/JER.h"

#include "Core/JEC/bin/matching.h"

using namespace std;
using namespace DAS;

////////////////////////////////////////////////////////////////////////////////
/// Functor to be used *inside* of the event loop.
/// Its purpose is to avoid bad copy-pasting to be used in different executables,
/// namely `applyDefaultJECs` and `applyJERsmearing` (the former apply both JES
/// and JER corrections in one go).
///
/// The drawback is that we are a little bit over-engineering the process, since
/// the functor itself calls another functor, implemented in `JER.h`...
/// While the JER functor takes care of the smearing itself (i.e. changes
/// the values of pt), the current functor modifies the vector of rec jets and 
/// fills control plots.
struct applyJERsmearingFunctor {

    JER SmearingFactor; //!< functor to *apply* the smearing factor to the jet + get unc.

    const JER::Mode mode; //!< smearing method

    applyJERsmearingFunctor 
        (const string& JERtables,     //!< directory for JER files
         const string& tag,           //!< tag from JetMET (or custom)
         float radius,                //!< jet size (e.g. 0.4)
         int seed,                    //!< seed 
         JER::Mode m = JER::Mode::scalingCoreOnly) :               //!< smearing method
        SmearingFactor(JERtables, tag, radius, seed),
        mode(m)
    {
        cout << "Initialising " << __func__ << endl;
        Matching<RecJet, GenJet>::DR = radius/2;
    }

    void operator() (const Event& evnt, PileUp & pu, vector<RecJet>& recJets, const vector<GenJet>& genJets)
    {
        // define the matching
        // NOTE: the rec jets are *copied* in the matching class
        static const bool CoreOnly = mode == JER::Mode::scalingCoreOnly;
        Matching<RecJet, GenJet> matching(recJets, CoreOnly, pu.rho);

        // loop and empty original vector, which we will fill step by step
        for (auto recJet = recJets.begin(); recJet != recJets.end(); recJets.erase(recJet)) { }

        // smear matched jet with scaling method (if allowed by the mode)
        if (mode != JER::Mode::stochasticOnly)
        for (const GenJet& genJet: genJets) {
            auto recJet = matching.HighestPt(genJet);

            // if no successful match
            if (recJet.p4.Pt() > 13000 /* GeV */) continue;

            recJet.JECs = SmearingFactor.ScalingMethod(recJet, genJet, pu.rho);
            recJets.push_back(recJet);
        }

        // smear unmatched jets with stochastic method
        for (RecJet& recJet: matching.mcands) {
            recJet.JECs = SmearingFactor.StochasticMethod(recJet, pu.rho);
            recJets.push_back(recJet);
        }
    }
};
