#include <cstdlib>
#include <string>
#include <iostream>

#include <TChain.h>
#include <TFile.h>
#include <TROOT.h>
#include <TH2.h>
#include <TRegexp.h>

#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Jet.h"

#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/ControlPlots.h"


#include "Core/JEC/interface/JEShandle.h"
#include "Core/JEC/interface/JESreader.h"

#include "applyJEScorrections.h"

using namespace std;
using namespace DAS;

////////////////////////////////////////////////////////////////////////////////
/// apply JES corrections to data and MC *n*tuples, as well as uncertainties
/// in data (all sources are considered separately)
void applyJEScorrections
            (TString input,     //!< name of input root file containing the *n*-tuple
             TString output,    //!< name of output root file containing the histograms
             TString JEStables, //!< path to directory containing corrections
             int nSplit = 1,    //!< number of jobs/tasks/cores
             int nNow = 0)      //!< index of job/task/core
{                               
    TChain * oldchain = new TChain("inclusive_jets"); 
    oldchain->Add(input);

    Event * evnt = nullptr;
    PileUp * pu = nullptr;
    oldchain->SetBranchAddress("event", &evnt);
    oldchain->SetBranchAddress("pileup", &pu);

    vector<RecJet> * recJets = nullptr;
    oldchain->SetBranchAddress("recJets", &recJets);

    TFile *newfile = TFile::Open(output, "RECREATE");
    TTree *newtree = oldchain->CloneTree(0);

    // declaring meta information
    MetaInfo metainfo(newtree);
    bool isMC = metainfo.isMC();
    if (nNow == 0) metainfo.Print();
    metainfo.AddCorrection("JES");

    JEShandle * handle = nullptr;
    if (JEStables.Contains("auto") || JEStables.Contains("default"))
        handle = new JEShandle(input, metainfo);
    else
        handle = new JEShandle(input, metainfo, JEStables);
    applyJEScorrectionsFunctor apply(*handle, isMC);

    // adding unc to meta info & control plots
    vector<TString> sources = JESreader::getJECUncNames();  //for all sources
    if (!isMC)
    for (TString source: sources) {
        metainfo.AddJEC(source + "up");
        metainfo.AddJEC(source + "down");
    }

    Looper looper(__func__, oldchain, nSplit, nNow);
    while (looper.Next()) {
        apply(*evnt, *pu, *recJets);

        sort(recJets->begin(), recJets->end(), pt_sort); // sort again the jets by pt
        newtree->Fill();
    }

    /* closing */
    newtree->AutoSave();

    TDirectory * controlplots = newfile->mkdir("JES");
    apply.Write(controlplots);

    newfile->cd();
    newfile->Close();
    delete oldchain;
    cout << "Done " << output << endl;
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 4) {
        cout << argv[0] << " input output JEStables  [nSplit [nNow]]\n"
             << "\twhere\tinput = n-tuple\n"
             << "\t     \toutput = new n-tuple\n"
             << "\t     \tJEStables = directory to tables\n"
             << "NB: either JEStables directly contains txt files "
             << "(which will be used even if it does not make sense) "
             << "or it contains subdirectories which it will try "
             << "to interpret smartly according to the given sample.\n"
             << "NB2: use 'def' or 'auto' to use default JES tables." << endl;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2],
            JEStables = argv[3];

    int nNow = 0, nSplit = 1;
    if (argc > 4) nSplit = atoi(argv[4]);
    if (argc > 5) nNow   = atoi(argv[5]);

    applyJEScorrections(input, output, JEStables, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
