#include <vector>

#include <TH2.h>
#include <TDirectory.h>

#include "Core/CommonTools/interface/toolbox.h"

#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Jet.h"

#include "Core/JEC/interface/JESreader.h"
#include "Core/JEC/interface/JEShandle.h"

using namespace std;
using namespace DAS;

////////////////////////////////////////////////////////////////////////////////
/// Functor to be used *inside* of the event loop.
/// Its purpose is to avoid bad copy-pasting to be used in different executables,
/// namely `applyDefaultJECs` and `applyJEScorrections` (the former apply both JES
/// and JER corrections in one go).
///
/// The drawback is that we are a little bit over-engineering the process, since
/// the functor itself calls other functors, implemented in `JES*.h`...
///  - `JESreader` is used to extract the JES corrections for a given file
///  - `JEShandle` find this file
///  - `applyJEScorrectionsFunctor` applies the JES corrections to the vector 
///     of rec jets
struct applyJEScorrectionsFunctor {

    JEShandle& handle;
    const bool isMC;
    vector<TH2 *> plots;
    TH2 * pt_fine_y;

    void addControlPlot (TString name)
    {
        TH2 * h = new TH2D(name, name, nPtBins, pt_edges.data(), nYbins, y_edges.data());
        plots.push_back(h);
    }

    applyJEScorrectionsFunctor (JEShandle& jeshandle, bool IsMC) :
        handle(jeshandle), isMC(IsMC)
    {
        addControlPlot("JESraw");
        addControlPlot("JEScorrected");
        if (!isMC) {
            vector<TString> sources = JESreader::getJECUncNames();
            for (TString source: sources) {
                addControlPlot("JES" + source + "up");
                addControlPlot("JES" + source + "down");
            }
        }
        
        //if (!isMC) {

        //     addControlPlot("JESup");
        //     addControlPlot("JESdown");
	     //}
    }

    void operator() (const Event& evnt, PileUp & pu, vector<RecJet>& recJets)
    {
        auto weight = evnt.recWgts.front();
        if (evnt.genWgts.size() > 0)
            weight *= evnt.genWgts.front();

        for (auto& recJet: recJets) {

            // sanity check
            assert(recJet.JECs.size() == 1 && recJet.JECs.front() == 1);

            // before JES
            plots.front()->Fill(recJet.CorrPt(), recJet.AbsRap(), weight);

            // apply JES
            JESreader * jecs = handle(evnt);

            // nominal value
            auto corr = jecs->getJEC(recJet, pu.rho);
            recJet.JECs.front() = corr;

            // if data, load uncertainties in JECs
            if (!isMC) {
                recJet.JECs = jecs->getUncVec(recJet.p4.Pt(), recJet.p4.Eta()); //all sources
                for (auto& JEC: recJet.JECs) JEC *= corr;
            }

            assert(recJet.JECs.size() + 1 == plots.size());

            // after JES up/down for each source
            for (size_t i = 0; i < recJet.JECs.size(); ++i) 
                plots.at(i+1)->Fill(recJet.CorrPt(i), recJet.AbsRap(), weight);
        }
    }

    void Write (TDirectory * dir)
    {
        dir->cd();
        for (TH2 * h: plots) {
            h->SetDirectory(dir);
            TString name = h->GetName();
            name = name(3,3333);
            h->Write(name);
        }
    }
};

