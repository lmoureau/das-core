#ifndef JER_COMMON
#define JER_COMMON
#include <cassert>
#include <vector>
#include <iostream>

#include "TH3.h"
#include "TDirectory.h"

#include "Core/CommonTools/interface/variables.h"

using namespace DAS;

using std::vector;
using std::cerr;
using std::cout;
using std::endl;

static const vector<double> pt_JERC_edges = {15, 17, 20, 23, 27, 30, 35, 40, 45, 57, 72, 90, 120, 150, 200, 300, 400, 550, 750, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000, 7500, 8000, 8500, 9000, 9500};
static const int nPtJERCbins = pt_JERC_edges.size()-1;

static const vector<const char *> variations {"nominal", "upper", "lower"};
static const int N = variations.size();

static const double w = 0.8;

vector<double> getBinning(int nBins, double first, double last)
{
    vector<double> bins(nBins+1);
    for(int i = 0; i <= nBins; ++i)
        bins[i] = first + ((last-first)/nBins) * i;
    return bins;
}

void assertValidBinning (const vector<double>& v)
{
    assert(v.size() > 1);
    for (size_t i = 1; i<v.size(); ++i) {
        if (v[i] > v[i-1]) continue;
        cerr << i << ' ' << v[i] << ' ' << v[i-1] << '\n';
    }
}

////////////////////////////////////////////////////////////////////////////////
/// Make equidistant binning in log pt
vector<double> GetLogBinning (double minpt, double maxpt, int nbins)
{
    assert(maxpt > minpt);
    assert(minpt > 0);
    assert(nbins > 1);

    vector<double> edges;

    double R = pow(maxpt/minpt,1./nbins);
    for (double pt = minpt; pt <= maxpt; pt *= R) edges.push_back(pt);
    cout << edges.size() << endl;
    
    return edges;
}

#endif
