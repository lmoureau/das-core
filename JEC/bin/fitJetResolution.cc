#include <cstdlib>
#include <cmath>
#include <cassert>
#include <iostream>
#include <map>
#include <limits>
#include <functional>
#include <filesystem>

#include "Core/CommonTools/interface/terminal.h"

#include <TROOT.h>
#include <TString.h>
#include <TRegexp.h>
#include <TFile.h>
#include <TKey.h>
#include <TH1.h>
#include <TF1.h>

#include "Core/JEC/interface/resolution.h"

#include "common.h"

using namespace std;
namespace fs = filesystem;

////////////////////////////////////////////////////////////////////////////////
/// Projection 3D histograms onto 1D histograms and fit response
void FitResolution (TDirectory * dir,
                    TH1 * res)
{
    dir->cd();
    TString stdPrint = res->GetTitle();
    stdPrint.ReplaceAll(TString( res->GetName() ), "");
    cout << "---------- " << orange << res->GetName() << normal << stdPrint << endl;

    int startBin = 0,
        stopBin  = 0;
    for (int i = 1; i <= res->GetNbinsX(); ++i) {
        double binCont = res->GetBinContent(i);
        if (binCont > 0) {
            startBin = i;
            break;
        }
    }
    for (int i = res->GetNbinsX(); i >= 1; --i) {
        double binCont = res->GetBinContent(i);
        if (binCont > 0) {
            stopBin = i;
            break;
        }
    }

    double ptmin = res->GetXaxis()->GetBinLowEdge(startBin),
           ptmax = res->GetXaxis()->GetBinUpEdge(stopBin);
    // Resolution fit
    TString name = res->GetName();
    cout << "-- Fit of resolution width (" << orange << name << normal << ") between: " << ptmin << " and " << ptmax << endl;
    TF1 * sigma = new TF1("sigma", ::mNSC, ptmin, ptmax, 4);
    sigma->SetParameters(5, 0.5, 0.01, 1);
    sigma->SetParLimits(0, -10, 10);
    sigma->SetParLimits(1, 0, 5);
    sigma->SetParLimits(2, 0, 3);
    sigma->FixParameter(3, 1);
    res->Fit(sigma, "Q0SNRE");
    auto N = sigma->GetParameter(0),
         S = sigma->GetParameter(1),
         C = sigma->GetParameter(2);
    printf("--   1st Resolution fit: %.3f %.3f %.3f \n", N, S, C);
    printf("--\n");
    sigma->ReleaseParameter(3);
    sigma->SetParLimits(0, min(N/2, N*2), max(N/2, N*2));
    sigma->SetParLimits(1, S/2, S*2);
    sigma->SetParLimits(2, min(C/2, C*2), max(C/2, C*2));
    sigma->SetParLimits(3, 0, 2);
    auto r = res->Fit(sigma, "Q0SNRE");
    printf("--   2nd Resolution fit: %.3f %.3f %.3f %.3f \n", sigma->GetParameter(0), sigma->GetParameter(1),
                                                              sigma->GetParameter(2), sigma->GetParameter(3));
    sigma->SetTitle( Form("#chi^{2}/ndf = %.2f/%d", sigma->GetChisquare(), sigma->GetNDF()) );
    sigma->SetNpx(2000); // Increase points used to draw TF1 object when saving
    sigma->Write();

    delete sigma;
}

////////////////////////////////////////////////////////////////////////////////
/// Copy directory structure.
/// Given an input file dIn recursively loop over all directory
/// hierarchy/contents and reproduce it in output file dOut.
/// For each TH1 instance found containing the resolution
/// perform a NSC fit by calling FitResolution() function.
void loopDirs (TDirectory * dIn,
               TDirectory * dOut)
{
    for (const auto&& obj: *(dIn->GetListOfKeys())) {
        auto const key = dynamic_cast<TKey*>(obj);
        if ( key->IsFolder() ) {
            auto ddIn = dynamic_cast<TDirectory*>( key->ReadObj() );
            if ( TString( ddIn->GetName() ).Contains("ptbin") ) continue;
            TDirectory * ddOut = dOut->mkdir(ddIn->GetName(), ddIn->GetTitle());
            ddOut->cd();
            cout << "Creating directory: " << bold << ddIn->GetPath() << normal << endl;

            loopDirs(ddIn, ddOut);
        }
        else if ( TString( key->ReadObj()->ClassName() ).Contains("TH1") ) {
            auto res = dynamic_cast<TH1*>( key->ReadObj() );
            if ( TString( res->GetName() ).Contains("sigmaCore") ) {
                res->SetDirectory(0);
                res->Write();
            }
            else {
                cout << "---------- Found " << orange << key->ReadObj()->GetName() << normal
                            << ".. Is of " << underline << key->ReadObj()->ClassName() << normal << " type, ignoring it" << endl;
                continue;
            }

            // Fit resolution
            FitResolution(dOut, res);
        }
        else cout << "---------- Found " << orange << key->ReadObj()->GetName() << normal
                    << ".. Is of " << underline << key->ReadObj()->ClassName() << normal << " type, ignoring it" << endl;
    } // End of for (list of keys) loop
}

////////////////////////////////////////////////////////////////////////////////
/// Fit resolution curves from `fitJetResponse`
void fitJetResolution
             (const fs::path& input, //!< name of input root file
              const fs::path& output) //!< name of output root file
{
    // Opening source and checking that it is MC
    assert(fs::exists(input));
    TFile * source = TFile::Open(input.c_str(), "READ");
    assert(source != nullptr);

    if (fs::exists(output))
        cerr << red << "Overwriting " << output << '\n' << normal;
    TFile * file = TFile::Open(output.c_str(), "RECREATE");
    assert(file != nullptr);
   
    loopDirs(source, file);

    file->Close();
    source->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 3) {
        cout << argv[0] << " input output\n"
             << "\twhere\tinput = output from `fitJetResponse`\n"
             << "\t     \toutput = root file with resolution fits ready for extraction to tables\n"
             << flush;
        return EXIT_SUCCESS;
    }

    fs::path input = argv[1],
            output = argv[2];

    fitJetResolution(input, output);
    return EXIT_SUCCESS;
}
#endif

