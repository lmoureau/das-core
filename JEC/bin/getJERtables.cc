#include <cstdlib>
#include <string>
#include <iostream>
#include <filesystem>
#include <limits>

#include <TFile.h>
#include <TROOT.h>
#include <TH3.h>
#include <TF1.h>
#include <TMath.h>

#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/variables.h"

#include "JetMETCorrections/Modules/interface/JetResolution.h"

#include "common.h"

using namespace std;
using namespace DAS;

namespace fs = filesystem;

static const auto feps = numeric_limits<float>::epsilon();

void GetResolution (const fs::path& input, TDirectory * d, int year)
{
    assert(fs::exists(input));
    assert(is_regular_file(input));
    cout << input.stem().c_str() << endl;

    JME::JetResolution resolution(input.string());

    d->cd();

    TH3 * h3 = new TH3D("h3", "resolution", nGenBins, genBins.data(),
                                            nEtaBins, eta_edges.data(),
                                            nRhoBins.at(year), rho_edges.at(year).data());

    for (int i = 1; i <= h3->GetNbinsX(); ++i)
    for (int j = 1; j <= h3->GetNbinsY(); ++j) 
    for (int k = 1; k <= h3->GetNbinsZ(); ++k) {
        double pt  = h3->GetXaxis()->GetBinCenter(i),
               eta = h3->GetYaxis()->GetBinCenter(j),
               rho = h3->GetZaxis()->GetBinCenter(k);

        JME::JetParameters params = {{JME::Binning::JetPt , pt  },
                                     {JME::Binning::JetEta, eta },
                                     {JME::Binning::Rho   , rho }};
        auto res = resolution.getResolution(params);
        h3->SetBinContent(i,j,k, res);
    }

    h3->Write();

    for (int j = 1; j <= h3->GetNbinsY(); ++j) { // looping over eta

        double etamin = h3->GetYaxis()->GetBinLowEdge(j),
               etamax = h3->GetYaxis()->GetBinLowEdge(j+1);

        d->cd();
        auto dd = d->mkdir(Form("eta%d", j));
        dd->SetTitle(Form("%.3f < |#eta| < %.3f", etamin, etamax));

        for (int k = 1; k <= h3->GetNbinsZ(); ++k) { // looping over rho
            dd->cd();
            auto ddd = dd->mkdir(Form("rho%d", k-1));
            ddd->cd();
            ddd->SetTitle(rhoBins.at(year).at(k-1));

            auto h = h3->ProjectionX("h", j, j, k, k);
            h->SetDirectory(ddd);
            h->SetTitle(rhoBins.at(year).at(k-1));
            h->Write();
        }
    }
}

void ReadFunction (const fs::path& input, TDirectory * d, bool rho_ov, int year)
{
    assert(fs::exists(input));
    assert(is_regular_file(input));
    cout << input.stem().c_str() << endl;

    ifstream file;
    file.open(input.c_str());

    d->cd();

    // parsing the header line
    TString dummy, formula;
    do {
        file >> dummy;
        if (dummy.Contains("sqrt")) formula = dummy;
    }
    while (!dummy.Contains("}"));
    cout << "formula:\t" << formula << endl;
    assert(formula != "");

    // parsing all the next lines
    double etamin, etamax, rhomin, rhomax, ptmin, ptmax;
    int nparameters;
    int etabin = 1, rhobin = 0;
    while (file.good()) {

        file >> etamin >> etamax >> rhomin >> rhomax >> nparameters >> ptmin >> ptmax;

        nparameters -= 2;
        assert(nparameters > 0);

        auto p = new double[nparameters];
        for (int i = 0; i < nparameters; ++i)
            file >> p[i];

        if (file.fail()) break;
        if (etamax <= 0) continue; // we skip the negative eta bins

        bool goodBin = abs(etamin - eta_edges.at(etabin-1)) < feps 
                    && abs(etamax - eta_edges.at(etabin  )) < feps 
                    && abs(rhomin - rho_edges.at(year).at(rhobin  )) < feps 
                    && abs(rhomax - rho_edges.at(year).at(rhobin+1)) < feps;

        cout << setw(7) << etamin << setw(7) << eta_edges.at(etabin-1) << setw(7) << etamax << setw(7) << eta_edges.at(etabin  ) << setw(7) << etabin
             << setw(7) << rhomin << setw(7) << rho_edges.at(year).at(rhobin  ) << setw(7) << rhomax << setw(7) << rho_edges.at(year).at(rhobin+1) << setw(7) << rhobin
             << setw(7) << nparameters
             << setw(7) << ptmin << setw(7) << ptmax;

        d->cd();
        auto ddd = d->GetDirectory(Form("eta%d", etabin))
                    ->GetDirectory(Form("rho%d", rhobin));

        cout << setw(15) << ddd;
        if (!goodBin) cout << setw(15) << "edges are not matching";
        else if (ddd == nullptr) cout << setw(15) << "no directory";
        else {
            ddd->cd();
            auto f = new TF1("f", formula, ptmin, ptmax);
            f->SetParameters(p);
            for (int i = 0; i < nparameters; ++i)
                cout << setw(15) << p[i];
            f->Write();
        }
        cout << endl;
        
        delete p;

        ++rhobin;
        int nRhoBinsHere = rho_ov ? nRhoBins.at(year) : nRhoBins.at(year)-1;
        if (rhobin >= nRhoBinsHere) {
            ++etabin;
            rhobin = 0;
        }
        if (etabin > nEtaBins  ) break;
    }
    file.close();
}

void GetResSFs (const fs::path& input, TDirectory * d)
{
    assert(fs::exists(input));
    assert(is_regular_file(input));
    cout << input.stem().c_str() << endl;

    JME::JetResolutionScaleFactor resolution_sf(input);

    d->cd();

    TH1 * hSFnom = new TH1D("nom", "nominal", nEtaBins, eta_edges.data());
    TH1 * hSFup  = new TH1D("up" , "upper"  , nEtaBins, eta_edges.data());
    TH1 * hSFdn  = new TH1D("dn" , "lower"  , nEtaBins, eta_edges.data());

    for (int j = 1; j <= hSFnom->GetNbinsX(); ++j) {
        double eta = hSFnom->GetXaxis()->GetBinCenter(j);

        JME::JetParameters params = {{JME::Binning::JetPt , 100 },
                                     {JME::Binning::JetEta, eta }, // the only variable that matters
                                     {JME::Binning::Rho   , 10  }};
        auto SFnom = resolution_sf.getScaleFactor(params, Variation::NOMINAL),
             SFup  = resolution_sf.getScaleFactor(params, Variation::UP     ),
             SFdn  = resolution_sf.getScaleFactor(params, Variation::DOWN   );
        hSFnom->SetBinContent(j, SFnom);
        hSFup ->SetBinContent(j, SFup );
        hSFdn ->SetBinContent(j, SFdn );
    }

    hSFnom->Write();
    hSFup ->Write();
    hSFdn ->Write();
}

void getJERtables (const fs::path& input1,
                   const fs::path& input2,
                   TString output,
                   bool rho_ov,
                   int year)
{
    TFile * f = TFile::Open(output, "RECREATE");

    TDirectory * d1 = f->mkdir("resolution");
    GetResolution(input1, d1, year);

    ReadFunction(input1, d1, rho_ov, year);

    f->cd();

    TDirectory * d2 = f->mkdir("SFs");
    GetResSFs    (input2, d2);

    f->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 5) {
        cout << argv[0] << " input1 input2 output rho_overflow year\n"
             << "where\tinput1 = resolution tables\n"
             << "     \tinput2 = SF tables\n"
             << "     \toutput = root files with histograms\n"
             << "     \trho_overflow = consider rho beyond 40.9 (default from JME tables is `false`, but for DAS tables, one should use `true`)\n"
             << "     \tyear = 201*\n"
             << "Note: This executbles has been developed to read the JetMET tables from `Summer16_25nsV1`. Hopefully it should work out of the box for other campaigns but no guarantee!\n"
             << flush;
        return EXIT_SUCCESS;
    }

    fs::path input1 = argv[1],
             input2 = argv[2];
    TString output = argv[3],
            rho_overflow = argv[4];
    rho_overflow.ToLower();
    bool rho_ov = rho_overflow.Contains("true") || rho_overflow.Contains("yes") || rho_overflow.Contains("1");
    int year = atoi(argv[5]);

    getJERtables(input1, input2, output, rho_ov, year);
    return EXIT_SUCCESS;
}
#endif
