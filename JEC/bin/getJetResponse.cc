#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"
#include "Core/CommonTools/interface/toolbox.h"

#include <TROOT.h>
#include <TTree.h>
#include <TFile.h>
#include <TString.h>
#include <TH1D.h>
#include <TH3D.h>

#include "common.h"
#include "matching.h"

using namespace std;
using namespace DAS;

////////////////////////////////////////////////////////////////////////////////
/// Create a TH3 histogram for the response with appropriate axes
TH3 * makeRespHist (TString name)
{
    static int nResBins = 200;
    static auto resBins = getBinning(nResBins, 0, 2);
    static const char * axistitles = ";p_{T}^{rec};|#eta|^{rec};#frac{p_{T}^{rec}}{p_{T}^{gen}}";

    // Fill with rec level quantities and pure rec level weight
    auto h = new TH3F("Response" + name, axistitles,
                nPtJERCbins, pt_JERC_edges.data(), nAbsEtaBins, abseta_edges.data(), nResBins, resBins.data());
    h->SetDirectory(0);
    return h;
}

////////////////////////////////////////////////////////////////////////////////
/// Get JER differential resolution and balance.
void getJetResponse (TString input, //!< n-tuple
                     TString output, //!< root file with histograms
                     int nSplit = 1, //!< index of slice
                     int nNow = 0) //!< number of slices
{
    TFile * source = TFile::Open(input, "READ");
    auto * tree = dynamic_cast<TTree*>(source->Get("inclusive_jets"));

    MetaInfo metainfo(tree);
    if (nNow == 0) metainfo.Print();
    bool isMC = metainfo.isMC();
    assert(isMC);
    int year = metainfo.year();

    Matching<RecJet, GenJet>::DR = metainfo.radius()/2;
    if (nNow == 0) cout << "Radius for matching: " << Matching<RecJet, GenJet>::DR << endl;

    Event * ev = nullptr;
    PileUp * pileUp = nullptr;
    tree->SetBranchAddress("event", &ev);
    tree->SetBranchAddress("pileup", &pileUp);

    vector<RecJet> * recjets = nullptr;
    vector<GenJet> * genjets = nullptr;
    tree->SetBranchAddress("recJets", &recjets);
    tree->SetBranchAddress("genJets", &genjets);

    TH3 * incl = makeRespHist("");
    vector<TH3*> rhobins;
    rhobins.reserve(nRhoBins.at(year));
    for (int rhobin = 1; rhobin <= nRhoBins.at(year); ++rhobin)
        rhobins.push_back( makeRespHist( Form("_rhobin%d", rhobin) ) );

    Looper looper(__func__, tree, nSplit, nNow);
    while (looper.Next()) {
        // Matching candidates recjets
        Matching<RecJet, GenJet> matching(*recjets);
        auto recWgt = ev->recWgts.front();
        
        int irho = 0;
        while (pileUp->rho > rho_edges.at(year).at(irho+1) && irho < nRhoBins.at(year)) ++irho;
        assert(irho != nRhoBins.at(year));
        for (const auto& genjet: *genjets) {
            float ptgen = genjet.p4.Pt();
            assert(ptgen > 0);

            // Match: highest-pt approach is working much better
            //RecJet recjet = matching.Closest(genjet); //TODO: externalize option?
            RecJet recjet = matching.HighestPt(genjet);

            if (recjet.p4.Pt() > numeric_limits<float>::max()/2.f) continue;

            auto ptrec = recjet.CorrPt();

            // Fill resolution from smearing (here, *no* PS selection)
            auto resolution = ptrec/ptgen;
            auto etarec     = std::abs( recjet.CorrEta() );
            // Inclusive rho case
            incl->Fill(ptrec, etarec, resolution, recWgt);

            // Exclusive rho case
            rhobins.at(irho)->Fill(ptrec, etarec, resolution, recWgt);
        } // End of for (gen jets) loop
    } // End of while (event) loop

    // Creating directory hierarchy and saving histograms
    TFile * file = TFile::Open(output, "RECREATE");
    file->cd();
    incl->SetTitle("Response distribution (inclusive #rho)");
    incl->Write("Response");
    for (int irho = 0; irho < nRhoBins.at(year); ++irho) {
        TString title = Form("%.2f < #rho < %.2f", rho_edges.at(year).at(irho), rho_edges.at(year).at(irho+1));
        TDirectory * d_rho = file->mkdir(Form("rhobin%d", irho+1), title);
        d_rho->cd();
        rhobins.at(irho)->SetTitle("Response distribution (" + title + ")");
        rhobins.at(irho)->Write("Response");
    }

    file->Close();
    source->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();

    if (argc < 3) {
        cout << argv[0] << " input output [nSplit [nNow]]\n"
             << "\twhere\tinput = n-tuple\n"
             << "\t     \toutput = root file" << endl;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2];
    int nNow = 0, nSplit = 1;
    if (argc > 3) nSplit = atoi(argv[3]);
    if (argc > 4) nNow = atoi(argv[4]);

    getJetResponse(input, output, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
