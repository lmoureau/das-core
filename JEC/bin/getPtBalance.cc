#include <cstdlib>
#include <cassert>

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

#include <TROOT.h>
#include <TTree.h>
#include <TFile.h>
#include <TProfile.h>
#include <TRandom.h>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"

#include "Core/JEC/interface/JESreader.h"
#include "common.h"
#include "getPtBalance.h"

#include "Math/VectorUtil.h"

template<typename Jet> const vector<double> Plots<Jet>::y_edges = DAS::y_edges;

using namespace std;
using namespace DAS;

////////////////////////////////////////////////////////////////////////////////
/// Get Pt balance
void getPtBalance (TString input, TString output, int nSplit = 1, int nNow = 0)
{
    TFile * source = TFile::Open(input, "READ");
    TTree * tree = dynamic_cast<TTree *>(source->Get("inclusive_jets"));

    MetaInfo metainfo(tree);
    metainfo.Print();
    const bool isMC = metainfo.isMC();

    static const int minpt = 74;

    Event * ev = nullptr;
    tree->SetBranchAddress("event", &ev);

    vector<RecJet> * recjets = nullptr;
    vector<GenJet> * genjets = nullptr;
    tree->SetBranchAddress("recJets", &recjets);

    TFile *file = TFile::Open(output, "RECREATE");

    vector<TString> var {"nominal"};

    if (isMC) {
        tree->SetBranchAddress("genJets", &genjets);
        var.push_back("JER_upper");
        var.push_back("JER_lower");
    }
    else {
        const auto& JES = JESreader::getJECUncNames();
        var.insert(var.end(), JES.begin(), JES.end());
    }

    static const int N = var.size();

    // detector level
    vector<Plots<RecJet>> plots;
    plots.push_back(Plots<RecJet>("rec_any_vs_any", N, [](const RecJet& tag, const RecJet& probe){ return true ;}  ));

    // hadron level
    vector<Plots<GenJet>> gplots;
    if (isMC) 
        gplots.push_back(Plots<GenJet>("gen_any_vs_any", N, [](const GenJet& tag, const GenJet& probe) { return true; }));


    Looper looper(__func__, tree, nSplit, nNow);
    while (looper.Next()) {

        auto gw = ev->genWgts.front(),
             rw = ev->recWgts.front();

        // we take the probe and the tag randomly
        auto r = gRandom->Uniform();
        int itag = (r<0.5),
            iprobe = (r>=0.5);

        // selection on pseudorapity
        if (DijetSelection<RecJet>(recjets)
                && abs(recjets->at(itag).CorrEta()) < 1.3
                && (recjets->at(0).CorrPt() > minpt || recjets->at(1).CorrPt() > minpt))
        for (auto p: plots) {

            if (!p.condition(recjets->at(itag), recjets->at(iprobe))) continue;

            auto absy_probe = recjets->at(iprobe).AbsRap();
            for (int i = 0; i < N; ++i ) {

                auto pt_tag   = recjets->at(itag  ).CorrPt(i);
                auto pt_probe = recjets->at(iprobe).CorrPt(i);

                auto ptMean = (pt_tag+pt_probe)/2.0;
                auto bal = (pt_probe-pt_tag)/ptMean;

                p.hs[i]->Fill(bal, ptMean, absy_probe, gw*rw);
            }
        }

        /**************** GEN LEVEL ***********/
        if (!isMC) continue;
        if (!DijetSelection<GenJet>(genjets)) continue;
        if (genjets->at(0).p4.Pt() < minpt || genjets->at(1).p4.Pt() < minpt) continue;
        if (abs(genjets->at(itag).p4.Eta()) > 1.3) continue;

        for (auto p: gplots) {

            if (!p.condition(genjets->at(itag), genjets->at(iprobe))) continue;

            auto absy_probe = genjets->at(iprobe).AbsRap();

            for (int i = 0; i < N; ++i ) {
                auto pt_tag   = genjets->at(itag  ).p4.Pt();
                auto pt_probe = genjets->at(iprobe).p4.Pt();

                auto ptMean = (pt_tag+pt_probe)/2.0;
                auto bal = (pt_probe-pt_tag)/ptMean;

                p.hs[i]->Fill(bal, ptMean, absy_probe, gw);
            }
        }
    }

    for (auto p: plots)
        p.Write(file);

    if(isMC)
        for (auto p: gplots)
            p.Write(file);

    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 3) {
        cout << "getPtBalance input output [nSplit [nNow]]\n"
            << "\twhere\tinput = n-tuple\n"
            << "\t     \toutput = root file" << endl;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2];
    int nNow = 0, nSplit = 1;
    if (argc > 3) nSplit = atoi(argv[3]);
    if (argc > 4) nNow = atoi(argv[4]);

    getPtBalance (input, output, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
