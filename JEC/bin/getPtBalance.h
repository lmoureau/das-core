#include <vector>
#include <functional>
#include <type_traits>

using std::vector;
using std::function;

#include "Math/VectorUtil.h"
using ROOT::Math::VectorUtil::DeltaR;
using ROOT::Math::VectorUtil::DeltaPhi;

#include <TH3.h>
#include <TString.h>
#include <TDirectory.h>

#include "Core/CommonTools/interface/variables.h"
using namespace DAS;

template<typename Jet> bool DijetSelection (const vector<Jet> * jets)
{
   if (jets->size() < 2) return false;
   if (DeltaPhi(jets->at(0).p4, jets->at(1).p4) < 2.7) return false;
   if (abs(jets->at(0).p4.Eta()) > 2.5 || abs(jets->at(1).p4.Eta()) > 2.5) return false;

   if (jets->size() > 2) {

         if constexpr (std::is_same<Jet,RecJet>()) {
              const auto pt0 = jets->at(0).CorrPt(),
                         pt1 = jets->at(1).CorrPt(),
                         pt2 = jets->at(2).CorrPt();
              if (pt2 > 0.15*(pt0 + pt1)) return false;
         }
         else {
              const auto pt0 = jets->at(0).p4.Pt(),
                         pt1 = jets->at(1).p4.Pt(),
                         pt2 = jets->at(2).p4.Pt();
              if (pt2 > 0.15*(pt0 + pt1)) return false;
         }

   }
   return true;
}

template<typename Jet> struct Plots {
    static const vector<double> y_edges;

    TString n;
    vector<TH3 *> hs;
    function<bool(const Jet&, const Jet&)> condition;

    Plots (const char * name, //!< general name (will be used for the directory)
            int N, //!< number of variations
            function<bool(const Jet&, const Jet&)> f) : //!< condition for filling
        n(name),
        hs(N),
        condition(f)
    {
        auto balBins =  getBinning(60, -0.6, 0.6);
        assertValidBinning(balBins);
        cout << balBins.size() << endl;

        for(int i = 0; i < N; ++i)
            hs[i] = new TH3D(Form("%s_balance_%d", name, i), Form("%s_balance_%d", name, i), //var[i], // TODO: give proper name to variation
                    balBins.size()-1, balBins.data(), nGenBins, genBins.data(), nYbins, y_edges.data());
    }

    void Write (TFile * f)
    {
        f->cd();
        TDirectory * d = f->mkdir(n); // member of the class
        d->cd();
        for(TH3 * h: hs) {
            h->SetDirectory(d);
            TString hname = h->GetName();
            hname.ReplaceAll(n + "_", ""); // n is a member of the class
            h->Write(hname);
        }
    }
};

