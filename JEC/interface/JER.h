#ifndef JER_H
#define JER_H

#include <random>
#include <limits>
#include <vector>
#include <string>
#include <functional>

#include "Core/Objects/interface/Jet.h"

#include "JetMETCorrections/Modules/interface/JetResolution.h"

#include <TString.h>

namespace DAS {
////////////////////////////////////////////////////////////////////////////////
/// Class to acces Jet Energy Resolution files from JetMET and apply them.
///
/// Source: https://twiki.cern.ch/twiki/bin/viewauth/CMS/JetResolution
class JER {

    const std::string sfFile, resolutionFile;

    JME::JetResolution resolution;
    JME::JetResolutionScaleFactor resolution_sf;
    std::mt19937 m_random_generator;

    JME::JetParameters params; //!< NOTE: this gets modified internally!!

    ////////////////////////////////////////////////////////////////////////////////
    /// Private method to return a vector of JECs corresponding to the nominal value
    /// of the JES and to the three variations related to JER smearing.
    ///
    /// The idea is that it can directly override `DAS::RecJet::JECs`.
    std::vector<float> vJECs
        (const RecJet& recjet, //!< corrected rec jet
         const std::function<float(Variation)>& var //!< lambda function defined in `JER::*Method()`
        );

public:

    enum Mode {
        stochasticOnly,
        scalingAllMatched,
        scalingCoreOnly
    };

    ////////////////////////////////////////////////////////////////////////////////
    /// Given a string, get the corresponding enumeration (to be used for command 
    /// line interpreter)
    static Mode GetEnumMode (TString s);

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor
    JER 
        (const std::string& JERtables,//!< directory for JER files
         const std::string& tag,
         float radius,           //!< R-parameter used by anti-kt algo
         int seed) ;             //!< seed

    ////////////////////////////////////////////////////////////////////////////////
    /// Get resolution for given pt, eta, rho
    JME::JetParameters getParams
        (float pt,  //!< pt (raw or corrected)
         float eta, //!< pseudorapidity
         float rho  //!< soft activty
        ) const;

    ////////////////////////////////////////////////////////////////////////////////
    /// Get resolution for given corrected rec jet and rho
    JME::JetParameters getParams
        (const RecJet& recjet,  //!< reconstructed jet (raw or corrected)
         float rho              //!< soft activty
        ) const;

    ////////////////////////////////////////////////////////////////////////////////
    /// Get resolution from tables given (corrected) rec jet
    template<typename... Args> float getResolution (Args... args) const
    {
        auto params = getParams(args...);
        return resolution.getResolution(params);
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// For a given rec jet with a match at gen level, returns variations
    /// corresponding to resolution from JetMET using scaling smearing.
    std::vector<float> ScalingMethod
        (const RecJet& recjet, //!< rec jet to smear
         const GenJet& genjet, //!< corresponding gen get or empty gen jet
         float rho //!< soft activity
        );

    ////////////////////////////////////////////////////////////////////////////////
    /// For a given rec jet (without a match at gen level), returns variations
    /// corresponding to resolution from JetMET using stochastic smearing.
    std::vector<float> StochasticMethod
        (const RecJet& recjet, //!< rec jet to smear
         float rho   //!< soft activity
        );
};

}

#endif
