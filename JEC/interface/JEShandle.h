#ifndef JEShandle_h
#define JEShandle_h

#include <map>
#include <string>

#include "Core/CommonTools/interface/MetaInfo.h"

namespace DAS {

class Event;
class JESreader;

////////////////////////////////////////////////////////////////////////////////
/// A wrapper to pick up the right corrections from the name.
/// No physics correction is performed by the class itself.
///
/// It makes abundant use of regular expressions (regexps) on tags such as, for
/// instance, Summer16_07Aug2017_V11, in order to find the relevant corrections
/// in the workarea repository (on the LFS)
class JEShandle {

    static TString GetLocation (TString d);
    static TString GetName (TString d);
    static TString GetGlobalTag (TString name);
    static TString GetVersion (TString name);
    static TString guessL5 (TString input);
    static TString GetSmoothCorrection (MetaInfo& metainfo, TString name);

    TString getDATAsubdir (MetaInfo& metainfo, TString era);
    TString getMCsubdir (MetaInfo& metainfo, TString input);

public:

    const int year; //!< year (e.g. 2016)
    const float radius; //!< anti-kt R-parameter
    // example: Summer16_07Aug2017_V11
    const TString location,  //!< path on machine
                  name,      //!< full name of correction (corresponding to directory's name)
                  globaltag, //!< for instance Summer16_07Aug2017
                  version,   //!< for instance V11 in Summer16_07Aug2017_V11
                  smoothL2;  //!< path to handmade corrections (done with `getJEScurves`)
    JESreader * singleJES; //!< used for MC or by default
    std::map<TString, JESreader *> multiJES; //!< used for data

    ////////////////////////////////////////////////////////////////////////////////
    /// constructor with full control
    JEShandle 
        (TString input, //!< name of input root file with *n*-tuple
         DAS::MetaInfo& metainfo, //!< meta information from *n*-tuple
         TString directory); //!< root directory of corrections 
    ////////////////////////////////////////////////////////////////////////////////
    /// constructor with default value
    JEShandle 
        (TString input, //!< name of input root file with *n*-tuple
         DAS::MetaInfo& metainfo); //!< meta information from *n*-tuple

    ////////////////////////////////////////////////////////////////////////////////
    /// Overload of () operator applying the right JES as a function of the
    /// information in the Event (e.g. the run)
    JESreader * operator() (const Event & event);
};

}

#endif
