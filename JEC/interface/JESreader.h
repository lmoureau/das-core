#ifndef JESreader_h
#define JESreader_h

#include <map>
#include <vector>
#include <string>
#include <fstream>

#include <TString.h>

#include "CondFormats/JetMETObjects/interface/FactorizedJetCorrector.h"
#include "CondFormats/JetMETObjects/interface/JetCorrectorParameters.h"
#include "CondFormats/JetMETObjects/interface/JetCorrectionUncertainty.h"

#include "Core/Objects/interface/Jet.h"

class TF1;

namespace DAS {

////////////////////////////////////////////////////////////////////////////////
/// Class to acces Jet Energy Scale files from JetMET and apply them.
class JESreader  {

    bool //isMC, //!< flag, 1 = MC, 0 = data
         wFl,  //!< flag "with flavour" (i.e. w. L5 correction)
         smooth; //!< use smoothed version of JEC factors (homemade)

    FactorizedJetCorrector *L1Fast,         //!< level 1, for PU
                           *L2Relative,     //!< level 2, relative correction
                           *L3Absolute,     //!< level 3, absolute correction
                           *L2L3Residual;   //!< data residual corrections 
    std::map<int, FactorizedJetCorrector *> L5Flavor; //!< level 5, for flavour correction

    JetCorrectionUncertainty *uncProv; //!< provider for total uncertainty
public:
    std::vector<JetCorrectionUncertainty*> uncSources; //!< provider for single sources of uncertainty

private:
    std::vector<double> JMEy; //!< rapidity edges for corrections
    std::vector<TF1 *> SmoothL2; //!< smooth functions for L2 corrections

    void InitSmoothL2 (const char * name);

public:

    ////////////////////////////////////////////////////////////////////////////////
    /// Apply L1 correction on transverse momentum (to recover from PU effects)
    double GetL1Fast
        (double pt,  //!< transverse momentum
         double eta,  //!< pseudorapidity
         double area, //!< jet area
         double rho) //!< soft activity
        const;

    ////////////////////////////////////////////////////////////////////////////////
    /// Apply L2 relative correction on transverse momentum
    double GetL2Relative
        (double pt,  //!< transverse momentum
         double eta) //!< pseudorapidity
        const;

    ////////////////////////////////////////////////////////////////////////////////
    /// Apply SMOOTHED L2 relative correction on transverse momentum
    double GetSmoothL2Relative
        (double pt, //!< transverse momentum
         double eta) //!< pseudorapidity
        const;

    ////////////////////////////////////////////////////////////////////////////////
    /// Apply L3  absolute correction on transverse momentum
    double GetL3Absolute
        (double pt,  //!< transverse momentum
         double eta) //!< pseudorapidity
        const;

    ////////////////////////////////////////////////////////////////////////////////
    /// Apply L5 flavour corrections
    double GetL5Flavor
        (double pt, //!< transverse momentum
         double eta, //!< pseudorapidity
         int partonFlavour) //!< parton flavour (DIFFERENT from hadron flavour!)
             const;

    ////////////////////////////////////////////////////////////////////////////////
    /// Apply L2L3 residual corrections to the transverse momentum and to the pseudorapidity
    ///
    /// *Note*: for data only!
    double GetL2L3Residual 
        (double pt,  //!< transverse momentum
         double eta) //!< pseudorapidity
        const;


    ////////////////////////////////////////////////////////////////////////////////
    /// Get a pair of factor for nominal and (symmetric) systematic variations 
    double getJEC
        (const RecJet& jet, //!< reconstructed jet
         double  rho) const;      //!< soft activity (from `PileUp` object)
    double getJEC
        (double pt, double eta, double area,
         double  rho,       //!< soft activity (from `PileUp` object)
         int pFl = 0) const;

    ////////////////////////////////////////////////////////////////////////////////
    /// get total uncertainty, up and down separately
    std::pair<double,double> getTotalUnc (double pt, double eta) const;

    ////////////////////////////////////////////////////////////////////////////////
    /// get total uncertainty up and down in a vector
    std::vector<float> getTotalUncVec (double pt, double eta) const;

    ////////////////////////////////////////////////////////////////////////////////
    /// get vector with all uncertainties
    /// (up and down for each source are stored in row)
    std::vector<float> getUncVec (double pt, double eta) const;

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor
    JESreader 
        (std::string dir,   //!< directory containing the text files
         std::string name,  //!< tag to identify to right version of the JES corrections
         float radius,      //!< radius of the jets will decide whether ak4 or ak8 JES will be used
         std::string SmoothFile = ""); ///!< full path to smoothed corrections

    ////////////////////////////////////////////////////////////////////////////////
    /// Static function to return the names of all uncertainties.
    ///
    /// See https://twiki.cern.ch/twiki/bin/viewauth/CMS/JECUncertaintySources
    /// for more detailed information on their meaning
    static std::vector<TString> getJECUncNames();

};

}

#endif
