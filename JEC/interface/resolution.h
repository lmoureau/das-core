#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/Greta.h"
#include <TH1.h>

#include <iostream>
#include <cmath>

using namespace DAS;
using namespace std;

static const size_t nN  = 8; //!< order of Chebyshev polynomial for global normalisation of DCB
static const size_t nmu = 3; //!< order of Chebyshev polynomial for global mean of DCB
static const size_t nkL = 2; //!< order of Chebyshev polynomial for global LHS tail transition of DCB
static const size_t nnL = 5; //!< order of Chebyshev polynomial for global LHS tail steepness of DCB
static const size_t nkR = 2; //!< order of Chebyshev polynomial for global RHS tail transition of DCB
static const size_t nnR = 5; //!< order of Chebyshev polynomial for global RHS tail steepness of DCB

inline double safelog (double x) { return x > 0 ? log(x) : 0; };

////////////////////////////////////////////////////////////////////////////////
/// Calculate numerical derivative of a histogram
///
/// https://en.wikipedia.org/wiki/Numerical_differentiation
TH1 * Derivative (TH1 * h, double (*f)(double) = identity)
{
    const char * name = Form("derivative_%s", h->GetName());
    TH1 * d = dynamic_cast<TH1*>(h->Clone(name));
    d->Reset();
    for (int i = 2; i < d->GetNbinsX(); ++i) {
        auto x1 = h->GetBinCenter(i-1),
             x2 = h->GetBinCenter(i+1),
             f1 = f(h->GetBinContent(i-1)),
             f2 = f(h->GetBinContent(i+1));
        auto dx = x2-x1;
        if (abs(dx) < 1e-20) continue;
        auto df = f2-f1;
        d->SetBinContent(i, df/dx);
    }
    return d;
}

////////////////////////////////////////////////////////////////////////////////
/// Calculate numerical derivative of a histogram
///
/// https://en.wikipedia.org/wiki/Numerical_differentiation
TH1 * DerivativeFivePointStencil (TH1 * h, double (*f)(double) = identity)
{
    const char * name = Form("derivative_%s", h->GetName());
    TH1 * d = dynamic_cast<TH1*>(h->Clone(name));
    d->Reset();
    auto w = d->GetBinWidth(2);
    for (int i = 3; i < d->GetNbinsX()-1; ++i) {
        assert(abs(w - d->GetBinWidth(i)) < 1e-10);
        auto f1 = f(h->GetBinContent(i-2)),
             f2 = f(h->GetBinContent(i-1)),
             f3 = f(h->GetBinContent(i+1)),
             f4 = f(h->GetBinContent(i+2));
        auto dx = 12*w;
        if (abs(dx) < 1e-20) continue;
        auto df = -f4 + 8*f3 - 8*f2 + f1;
        d->SetBinContent(i, df/dx);
    }
    return d;
}

////////////////////////////////////////////////////////////////////////////////
/// Calculate numerical second derivative of a histogram
///
/// Note: seems to work less good that just applying twice Derivative()...
///
/// https://en.wikipedia.org/wiki/Numerical_differentiation
TH1 * SecondDerivative (TH1 * h, double (*f)(double) = identity)
{
    const char * name = Form("secondderivative_%s", h->GetName());
    TH1 * d2 = dynamic_cast<TH1*>(h->Clone(name));
    d2->Reset();
    for (int i = 2; i < d2->GetNbinsX(); ++i) {
        auto x0 = h->GetBinCenter(i-1),
             x1 = h->GetBinCenter(i  ),
             x2 = h->GetBinCenter(i+1),
             f0 = f(h->GetBinContent(i-1)),
             f1 = f(h->GetBinContent(i  )),
             f2 = f(h->GetBinContent(i+1));
        auto dx2 = (x2-x1)*(x1-x0);
        if (abs(dx2) < 1e-20) continue;
        auto d2f = f2-2*f1+f0;
        d2->SetBinContent(i, d2f/dx2);
    }
    return d2;
}

////////////////////////////////////////////////////////////////////////////////
/// Resolution width as a function of gen pt
///
///  - p[0] = Noise
///  - p[1] = Stochastic
///  - p[2] = Constant
///  - p[3] = modification parameter
///
/// Note that the freedom for the sign of the first term is also a modification
/// of the original NSC function.
double mNSC (double * x, double * p)
{
    return sqrt( p[0]*abs(p[0])/(x[0]*x[0]) + p[1]*p[1]/pow(x[0],p[3]) + p[2]*p[2]);
}

////////////////////////////////////////////////////////////////////////////////
/// Resolution with modified Gaussian with simple exponentional tail
///
/// Note: more stable than Crystal-Ball, but only one parameter
///
/// https://home.fnal.gov/~souvik/GaussExp/GaussExp.pdf
double GausExp (double * x, double * p)
{
    double N = p[0],
           mu = p[1],
           sigma = p[2],
           kR = p[3];

    auto z = [&](double x) { return (x - mu)/sigma; };

    double zx = z(*x);

    if (*x > kR) {
        double zk = z(kR);
        return N*exp(0.5*pow(zk,2)-zk*zx);
    }
    else {
        return N*exp(-0.5*pow(zx,2));
    }
}

////////////////////////////////////////////////////////////////////////////////
/// Asymmetric Gaussian, a.k.a. Novosibirsk
///
/// TODO: find reference
double Novosibirsk (double * x, double * p)
{
    double N = p[0],
           mu = p[1],
           sigma = p[2],
           tau = p[3];

    if (sigma <= 0) return 0;

    double y = (*x-mu)/sigma;

    static const double C = sqrt(log(4.));
    double coeff = sinh(C*tau) / C;

    double z2 = pow( log(1.+coeff*y)/tau, 2) + pow(tau,2);
    return N*exp(-0.5*z2);
}

////////////////////////////////////////////////////////////////////////////////
/// Resolution with modified Gaussian with simple power-law tail
///
/// Note: not very stable because of n**n behaviour
/// -> the trick when fitting is to find the k by looking at the derivative
///    of the log of the function, since the Gaussian should be a straight line...
/// 
/// NB: code should be carefully checked ...
/// 
/// https://en.wikipedia.org/wiki/Crystal_Ball_function
double CrystalBall (double * x, double * p)
{
    double N = p[0],
           mu = p[1],
           sigma = p[2],
           k = p[3],
           n = p[4];

    double z =    (*x - mu)/sigma, 
           a = abs( k - mu)/sigma;

    double expa2 = exp(-pow(a,2)/2.);

    double C = n * expa2 / (a*(n-1)),
           D = sqrt(M_PI/2.)*(1 + erf(a/sqrt(2)));

    N /= sigma * (C + D);

    if (*x > k) {
        return N*exp(-pow(z,2)/2.);
    }
    else {
        double A = pow(n/a, n) * expa2,
               B = n/a - a;
        return N*A*pow(B-z,-n);
    }
}

////////////////////////////////////////////////////////////////////////////////
/// Advanced function to fit resolution including non-Gaussian deviations and pile-up jets
double ModifiedGaussianCore (double * x, double * p)
{
    double N = p[0], // Gaussian core
           mu = p[1],
           sigma = max(1e-8,abs(p[2])),
           B = p[3],
           C = p[4];

    return N*exp(-  pow((*x-mu)/sigma,2)/2
                 -B*pow((*x-mu)      ,3)/3
                 -C*pow((*x-mu)      ,4)/4);
}

////////////////////////////////////////////////////////////////////////////////
/// Chebyshev fit as a function log(pt)
///
/// (slightly different from Greta, since the result is not exponentiated...)
///
/// `n` corresponds to the order of the polynomials -> `n=0` means 1 parameter
template<const size_t n> class Chebyshev {

    const double m, M;

    double T (double x, size_t k)
    {
        switch (k) {
            case 0: return 1.;
            case 1: return x;
            default: return 2*x*T(x,k-1)-T(x,k-2);
        }
    }

public:
    Chebyshev (double minimum, double maximum) :
        m(log(minimum)), M(log(maximum))
    {
        assert(M > m);
    }

    double operator() (double * x, double * p)
    {
        double result = 0;
        double nx = -1 + 2* (log(*x) - m) / (M - m);
        for (size_t i = 0; i <= n; ++i)
            result += p[i]*T(nx,i);
        return result;
    }
};

////////////////////////////////////////////////////////////////////////////////
/// 2D function to fit differential resolution
template<const size_t nmu, const size_t nkL, const size_t nnL, const size_t nkR, const size_t nnR,
         typename DBC> class ThunbergDoubleCrystalBall {

    Thunberg<nmu,log,identity> mu;
    Thunberg<nkL,log,identity> kL;
    Thunberg<nnL,log,exp     > nL;
    Thunberg<nkR,log,identity> kR;
    Thunberg<nnR,log,exp     > nR;

public:

    const size_t imu, isigma, ikL, inL, ikR, inR, N;
    DBC dbc;

    ThunbergDoubleCrystalBall (double minimum, double maximum) :
        mu(minimum, maximum),
        kL(minimum, maximum), nL(minimum, maximum),
        kR(minimum, maximum), nR(minimum, maximum),
        imu(0), isigma(mu.npars),
        ikL(isigma + 3), inL(ikL + kL.npars),
        ikR(inL + nL.npars), inR(ikR + kR.npars),
        N(inR + nR.npars)
    { }

    double operator() (double * x /*{pt, res}*/, double * p)
    {
        double q[] = { 1, // normalised to unity by definition
                       mu   (&x[0], &p[imu   ]),
                       mNSC (&x[0], &p[isigma]),
                       kL   (&x[0], &p[ikL   ]),
                       nL   (&x[0], &p[inL   ]),
                       kR   (&x[0], &p[ikR   ]),
                       nR   (&x[0], &p[inR   ]) };

        return dbc(&x[1], q);
    }
};

////////////////////////////////////////////////////////////////////////////////
/// 2D function to fit differential resolution
template<const size_t nmu, const size_t nkL, const size_t nkR, 
         typename DBC> class ThunbergDoubleCrystalBallHist {

    Thunberg<nmu,log,identity> mu;
    Thunberg<nkL,log,identity> kL;
    TH1 * nL; // TODO: make this work
    Thunberg<nkR,log,identity> kR;
    TH1 * nR;

public:

    const size_t imu, isigma, ikL, ikR, N;
    DBC dbc;

    ThunbergDoubleCrystalBallHist (double minimum, double maximum, TH1 * pnL, TH1 * pnR) :
        mu(minimum, maximum),
        kL(minimum, maximum), nL(pnL),
        kR(minimum, maximum), nR(pnR),
        imu(0), isigma(mu.npars),
        ikL(isigma + 3), ikR(ikL + nkL),
        N(ikR + nkR)
    { }

    double operator() (double * x /*{pt, res}*/, double * p)
    {
        int ipt = nL->FindBin(x[0]);
        double q[] = { 1, // normalised to unity by definition
                       mu  (&x[0], &p[imu   ]),
                       mNSC(&x[0], &p[isigma]),
                       kL  (&x[0], &p[ikL   ]),
                       nL->GetBinContent(ipt),
                       kR  (&x[0], &p[ikR   ]),
                       nR->GetBinContent(ipt) };

        return dbc(&x[1], q);
    }
};
