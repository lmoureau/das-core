#include <random>
#include <limits>
#include <filesystem>

#include "Core/CommonTools/interface/toolbox.h"

#include "Core/JEC/interface/JER.h"

#include "Core/JEC/bin/matching.h"

#include <TFile.h>

using namespace std;
using namespace DAS;

namespace fs = filesystem;

static const auto eps = numeric_limits<double>::epsilon();

JER::Mode JER::GetEnumMode (TString s)
{
    if (s.Contains("stochasticOnly"))    return JER::Mode::stochasticOnly;
    if (s.Contains("scalingAllMatched")) return JER::Mode::scalingAllMatched;
    if (s.Contains("scalingCoreOnly"))   return JER::Mode::scalingCoreOnly;
    cerr << "Mode `" << s << "` was not recognised\n";
    exit(EXIT_FAILURE);
}

JER::JER (const string& JERtables, const string& tag, float radius, int seed) :             
    resolution   (JERtables + '/' + tag + "_MC_PtResolution_AK" + to_string(radius < 0.6f ? 4 : 8) + "PFchs.txt"),
    resolution_sf(JERtables + '/' + tag + "_MC_SF_AK"           + to_string(radius < 0.6f ? 4 : 8) + "PFchs.txt"),
    m_random_generator(seed)
{
    cout << "Initialising DAS::JER" << endl;

    fs::path tails = JERtables + '/' + tag + "_MC_PtResolution_FatTails_AK" + to_string(radius < 0.6f ? 4 : 8) + "PFchs.root"; // TODO: store in text format
    if (fs::exists(tails)) {
        auto fTails = TFile::Open(tails.c_str(), "READ");
        Matching<RecJet, GenJet>::h_kL = dynamic_cast<TH3*>(fTails->Get("h_kL"));
        Matching<RecJet, GenJet>::h_kR = dynamic_cast<TH3*>(fTails->Get("h_kR"));
        Matching<RecJet, GenJet>::h_kL->SetDirectory(0);
        Matching<RecJet, GenJet>::h_kR->SetDirectory(0);
        fTails->Close();
    }
    else cout << "Tails will be ignored in the matching." << endl;
}

JME::JetParameters JER::getParams (float pt, float eta, float rho) const
{
    JME::JetParameters params = {{JME::Binning::JetPt , pt  },
                                 {JME::Binning::JetEta, eta },
                                 {JME::Binning::Rho   , rho }};
    return params;
}

JME::JetParameters JER::getParams (const RecJet& recjet, float rho) const
{
    auto p4 = recjet.p4;
    p4.Scale(recjet.JECs.front());
    auto recpt  = p4.Pt(),
         receta = p4.Eta();
    return getParams(recpt, receta, rho);
}

vector<float> JER::vJECs (const RecJet& recjet, const function<float(Variation)>& var) 
{
    auto scale = recjet.JECs.front();
    return { scale * var(Variation::NOMINAL),
             scale * var(Variation::UP     ),
             scale * var(Variation::DOWN   ) };
}

vector<float> JER::ScalingMethod (const RecJet& recjet, const GenJet& genjet, float rho)
{
    auto params = getParams(recjet, rho);

    auto var = [&](Variation v) {
        auto jer_sf = resolution_sf.getScaleFactor(params, v);
        auto recpt = recjet.CorrPt();
        auto dPt = recpt - genjet.p4.Pt();
        return max(0., 1. + (jer_sf - 1.) * dPt / recpt);
    };

    return vJECs(recjet, var);
}

vector<float> JER::StochasticMethod (const RecJet& recjet, float rho) 
{
    auto params = getParams(recjet, rho);
    float sigma = resolution.getResolution(params); // plain MC resolution from tables

    normal_distribution<float> d{0, sigma}; // TODO: make sure that this is the fastest solution
    auto delta = d(m_random_generator);
    auto var = [&](Variation v) {
        auto jer_sf = resolution_sf.getScaleFactor(params, v);
        return max(0., 1. + delta * sqrt(max(eps, jer_sf * jer_sf - 1.)));
    };

    return vJECs(recjet, var);
}
