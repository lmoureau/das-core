#include "Core/JEC/interface/JEShandle.h"
#include "Core/JEC/interface/JESreader.h"

#include <cstdlib>
#include <cassert>

#include <iostream>
#include <limits>

#include <TString.h>
#include <TRegexp.h>

#include <filesystem>

#include "Core/Objects/interface/Event.h"

using namespace std;
using namespace DAS;

namespace fs = filesystem;
//const std::map<int, std::map<int, TString>> *erasNow; // TODO: fix the dirty hack

static const map<int, TString> DefaultJECs {
    {2016, "Summer16_07Aug2017_V11"},
    {2017, "Summer19UL17_V5"},
    {2018, "Summer19UL18_V5"}
};

static const TString DAS_WORKAREA = getenv("DAS_WORKAREA");

// Before (and *not* including) V32
static const std::map<int, std::map<int, TString>> erasNow //BeforeV32
{
    { 2016,
        {
            { 272007, "BCD"},
            { 276831, "EF" },
            { 278802, "GH" }
        }
    },

    { 2017,
        {
            //{ 294034, "A" },
            { 297000, "B" },
            { 299337, "C" },
            { 302030, "D" },
            { 303435, "E" }, // TODO: merge D & E?
            { 304911, "F" }
        }
    },

    { 2018,
        {
            { 315257, "A" },
            { 317080, "B" },
            { 319337, "C" },
            { 320413, "D" }
        }
    }
};

//// After (and including V32)
//static const std::map<int, std::map<int, TString>> erasAfterV32 {
//    { 2016,
//        {
//            { 272007, "BCD"},
//            { 276831, "EF" },
//            { 278802, "GH" }
//        }
//    },
//
//    { 2017,
//        {
//            //{ 294034, "A" },
//            { 297000, "B" },
//            { 299337, "C" },
//            { 302030, "DE" }, // here is the difference...
//            { 304911, "F" }
//        }
//    },
//
//    { 2018,
//        {
//            { 315257, "A" },
//            { 317080, "B" },
//            { 319337, "C" },
//            { 320413, "D" }
//        }
//    }
//
//};

TString JEShandle::GetLocation (TString d)
{
    // removing last character if it is a slash (or several)
    while (d(d.Length()-1) == '/')
        d = d(0, d.Length() - 1);
    return d;
}

TString JEShandle::GetName (TString d)
{
    // keep only last part in directory's name
    TString name(d);
    if (d.Contains('/'))
        name = d(d.Last('/')+1, name.Length()+1);
    return name;
}

TString JEShandle::GetGlobalTag (TString name)
{
    cout << name << endl;
    if (name.Contains("UL"))
        return name(TRegexp("^[a-zA-Z]*19UL1[6-8]"))/* + "_Run"*/;
    else
        return name(TRegexp("^[a-zA-Z]*[0-9]*_[0-9]*[a-zA-Z]*[0-9]*"));
    // examples: Summer16_07Aug2017_V17  Summer16_07Aug2017_V20  Summer19UL17_V5  Summer19UL18_V5
}

TString JEShandle::GetVersion (TString name)
{
    TString version(name(TRegexp("_V[0-9]*")));
    version = version(2,version.Length()+1);

    //// Nasty hack by RADEK (see comment above)
    //if (version.Atoi() >= 32)
    //    erasNow = &erasAfterV32;
    //else
    //    erasNow = &erasBeforeV32;

    return version;
}

TString JEShandle::guessL5 (TString input)
{
    // Pythia & MadGraph use the same shower from Pythia
    if (input.Contains("Pythia") || input.Contains("MadGraph"))
        return "Pythia8";
    // while Herwig is totally independent
    else if (input.Contains("Herwig"))
        return "Herwig";
    else 
        return "";
}

//Deprecated: necessary only if you use refined method
[[ deprecated ]]
TString JEShandle::getDATAsubdir (MetaInfo& metainfo, TString era)
{
    TString GT = globaltag;
    if (GT.Contains("UL")) GT += "_Run";
    return GT + era + "_V" + version + "_DATA";
}

//Deprecated: necessary only if you use refined method
[[ deprecated ]]
TString JEShandle::getMCsubdir (MetaInfo& metainfo, TString input)
{
    TString L5 = guessL5(input);

    if (metainfo.year() == 2016 && L5 != "" && version.Contains("V15") &&
            abs(metainfo.radius() - 0.4f) < numeric_limits<float>::epsilon() )
        return globaltag + "_V" + version + "_Flavor_" + L5 + "_MC";
    else
        return globaltag + "_V" + version +                   "_MC";
}

TString JEShandle::GetSmoothCorrection (MetaInfo& metainfo, TString name)
{
    float radius = metainfo.radius();
    int iradius = round(radius*10.);
    return Form("%s/tables/JES/smooth/ak%d/%s.txt", DAS_WORKAREA.Data(), iradius, name.Data());
}

////////////////////////////////////////////////////////////////////////////////
/// constructor
JEShandle::JEShandle 
    (TString input, //!< name of input root file with *n*-tuple
     MetaInfo& metainfo, //!< meta information from *n*-tuple
     TString directory) : //!< root directory of corrections 
    year(metainfo.year()), radius(metainfo.radius()),
    location(GetLocation(directory)), name(GetName(location)),
    globaltag(GetGlobalTag(name)), version(GetVersion(name)),
    smoothL2(GetSmoothCorrection(metainfo, name)),
    singleJES(nullptr)
{
    assert(erasNow.count(year));
    assert(globaltag != "");

    fs::path dir(location.Data());
    cout << dir << endl;
    if (!fs::is_directory(dir)) {
        cerr << "Not a directory\n";
        exit(EXIT_FAILURE);
    }

    // A) guess the case:

    bool ContainsTables = true, ContainsDirectories = true;
    for (fs::directory_entry entry: fs::directory_iterator(dir)) {
        auto p = fs::path(entry);
        string extension = p.extension();
        static const vector<string> allowed {".C", ".md", ".root", ".pdf", ".py"};
        if (find(allowed.begin(), allowed.end(), extension) != allowed.end()) continue;
        cout << "entry: " << p << endl;
        ContainsDirectories = ContainsDirectories && fs::is_directory(entry);
        ContainsTables      = ContainsTables      && p.extension() == ".txt";
    }
    cout << "ContainsDirectories: " << ContainsDirectories << '\n'
         << "ContainsTables: " << ContainsTables << endl;

    // B) 1) IF it contains (only) text files
    //       THEN just use what the user wants and don't bother
    //            (even if it does not make sense)
    //    2) IF it contains (only) subdirectories 
    //       THEN try to be smart (according meta-info)

    // 1) brute force method
    if (ContainsTables && !ContainsDirectories)
        singleJES = new JESreader(location.Data(), name.Data(), radius, smoothL2.Data());

    // 2) refined approach
    else if (ContainsDirectories && !ContainsTables) { 

        //cerr << red << "Warning: if you are using UL16 datasets, better process each era separately and point the tool directly to the subdirectory containing the tables in text format\n" << normal;
        cerr << "Warning: if you are using UL16 datasets, better process each era separately and point the tool directly to the subdirectory containing the tables in text format\n" << endl;


        auto checkPath = [](fs::path subdir) {
            if (!fs::is_directory(subdir)) {
                cout << "Cannot find " << subdir << endl;
                exit(EXIT_FAILURE);
            }
        };

        if (metainfo.isMC()) { // here, one just need to check for  L5
            const TString name = getMCsubdir(metainfo, input);
            fs::path subdir = dir; subdir /= name.Data(); checkPath(subdir);
            singleJES = new JESreader(subdir.c_str(), name.Data(), radius, smoothL2.Data());
        }
        else { // here, one needs to declare one instance per era
            for (auto& era: erasNow.at(year)) {
                const TString name = getDATAsubdir(metainfo, era.second);
                fs::path subdir = dir; subdir /= name.Data(); checkPath(subdir);
                multiJES[era.second] = new JESreader(subdir.c_str(), name.Data(), radius, smoothL2.Data());
            }
        }
    }
    else {
        cerr << "The directory should either contain only subdirectories or only tables in text files.\n";
        exit(EXIT_FAILURE);
    }
}

////////////////////////////////////////////////////////////////////////////////
/// constructor with default JECs
JEShandle::JEShandle (TString input, MetaInfo& metainfo) :
    JEShandle(input, metainfo,
            DAS_WORKAREA + "/tables/JES/" + DefaultJECs.at(metainfo.year()))
{}

////////////////////////////////////////////////////////////////////////////////
/// Overload of () operator applying the right JES as a function of the
/// information in the Event (e.g. the run)
JESreader * JEShandle::operator() (const Event& event)
{
    // MC or brute force
    if (singleJES)
        return singleJES;
    
    // else 

    auto run = event.runNo;

    auto &eraNow = erasNow.at(year); //for given year

    auto it = eraNow.begin();
    while(run >= it->first && it != eraNow.end())
        ++it;
    --it;
    TString era = it->second;
    assert(run >= it->first);
    //if(run >= it->first)
    //cout<<"run: "<<run<<" it->first: "<<it->first<<endl; // run is larger than edge of the period
    ++it;
    if(it != eraNow.end()) assert(run < it->first); // run is smaller than edge of the next period
    
    JESreader * JES =  multiJES[era];
    assert(JES);
    return JES;
}
