#include "Core/CommonTools/interface/Greta.h"
#include "Core/JEC/interface/JESreader.h"

#include <algorithm>
#include <fstream>
#include <filesystem>

#include <TF1.h>

using namespace DAS;
using namespace edm;
using namespace std;

namespace fs = filesystem;

double JESreader::GetL1Fast
    (double pt,  //!< transverse momentum
     double eta,  //!< pseudorapidity
     double area, //!< jet area
     double rho)  //!< soft activity
    const
{
    L1Fast->setJetPt(pt);
    L1Fast->setJetEta(eta);
    L1Fast->setJetA(area);
    L1Fast->setRho(rho);
    return L1Fast->getCorrection();
}

double JESreader::GetL2Relative
    (double pt, //!< transverse momentum
     double eta) //!< pseudorapidity
    const
{
    L2Relative->setJetPt(pt);
    L2Relative->setJetEta(eta);
    return L2Relative->getCorrection();
}

void JESreader::InitSmoothL2 (const char * name)
{
    JMEy.clear();
    SmoothL2.clear();

    ifstream file(name);

    TString ymin, ymax, ptmin, ptmax, formula; int Nargs;
    file >> ymin >> ymax >> ptmin >> ptmax >> formula >> Nargs;
    assert(ymin == "ymin");
    assert(ymax == "ymax");
    assert(ptmin == "ptmin");
    assert(ptmax == "ptmax");

    cout << "Formula: " << formula << endl;

    while (file.good()) {

        // extracting
        double y1, y2, pt1, pt2;
        vector<double> p(Nargs);
        file >> y1 >> y2 >> pt1 >> pt2;
        if (!file.good()) break;
        for (int i = 0; i < Nargs; ++i) file >> p[i];

        cout << y1 << ' ' << y2 << ' ' << pt1 << ' ' << pt2 << endl;

        // defining function
        TString name = Form("%.2f", y1);
        name.ReplaceAll("-", "m");
        name.ReplaceAll(".", "_");
        TF1 * f = nullptr;
        if (formula == "Greta") {
            Greta greta(Nargs-1, pt1, pt2);
            f = new TF1(name, greta, pt1, pt2, Nargs);
        }
        else
            f = new TF1(name, formula, pt1, pt2);
        f->SetParameters(p.data());
        
        // push backs to members
        JMEy.push_back(y1);
        JMEy.push_back(y2); // we push both to get the last bins...
        SmoothL2.push_back(f);
    }
    sort(JMEy.begin(), JMEy.end());
    auto it = unique(JMEy.begin(), JMEy.end());
    JMEy.resize( distance(JMEy.begin(), it) );
    assert(JMEy.size() == SmoothL2.size()+1);

    file.close();
}

double JESreader::GetSmoothL2Relative
    (double pt, //!< transverse momentum
     double eta) //!< pseudorapidity
    const
{
    eta = min( 2.9999, eta);
    eta = max(-2.9999, eta);

    // from https://root.cern.ch/doc/master/TAxis_8cxx_source.html#l00279 L303
    // bin = 1 + TMath::BinarySearch(fXbins.fN,fXbins.fArray,x);

    static const int N = JMEy.size();
    int i = TMath::BinarySearch(N,JMEy.data(),eta);

    TF1 * f = SmoothL2.at(i);

    //double logptmin, logptmax;
    //f->GetRange(logptmin, logptmax);

    //double logpt = min(logptmax - 0.0001, log10(pt));
    //       logpt = max(logptmin + 0.0001, log10(pt));

    //return f->Eval(logpt);

    double ptmin, ptmax;
    f->GetRange(ptmin, ptmax);

    pt = min(ptmax - 0.0001, pt);
    pt = max(ptmin + 0.0001, pt);

    return f->Eval(pt);
}

double JESreader::GetL3Absolute
    (double pt, //!< transverse momentum
     double eta) //!< pseudorapidity
    const
{
    L3Absolute->setJetPt(pt);
    L3Absolute->setJetEta(eta);
    return L3Absolute->getCorrection();
}

double JESreader::GetL5Flavor 
    (double pt, //!< transverse momentum
     double eta, //!< pseudorapidity
     int partonFlavour) //!< parton flavour (DIFFERENT from hadron flavour!)
     const
{
    FactorizedJetCorrector * l5flavor = L5Flavor.at(abs(partonFlavour));
    assert(l5flavor != nullptr);

    l5flavor->setJetPt(pt);
    l5flavor->setJetEta(eta);
    return l5flavor->getCorrection();
}

double JESreader::GetL2L3Residual (double pt, double eta) const
{
    L2L3Residual->setJetPt(pt);
    L2L3Residual->setJetEta(eta);
    return L2L3Residual->getCorrection();
}

double JESreader::getJEC (double pt, double eta, double area, double rho, int pFl) const
{
    double L1corr = GetL1Fast(pt, eta, area, rho);
    pt *= L1corr;

    double L2corr = 1, L3corr = 1;
    if (wFl) {
        // this one replaces L2(L3)Relative
        L2corr = GetL5Flavor(pt, eta, pFl); // 
        pt *= L2corr;
    }
    else {
        // if L3 is trivial, this one is L2L3Relative (distinct from L2L4Residual, only for data)
        using JESmethod = double (JESreader::*)(double, double) const;
        static JESmethod fn = smooth ? &JESreader::GetSmoothL2Relative : &JESreader::GetL2Relative;
        L2corr = (this->*fn)(pt, eta);
        pt *= L2corr;

        // this one is probably trivial, but just in case...
        L3corr = GetL3Absolute(pt, eta);
        pt *= L3corr;
    }

    double L2L3residual = GetL2L3Residual(pt, eta);
    //double L2L3residual = !isMC ? GetL2L3Residual(pt, eta) : 1.;

    return L1corr * L2corr * L3corr * L2L3residual;
}

double JESreader::getJEC (const RecJet & jet, double  rho) const
{
    double pt = jet.p4.Pt();
    double eta = jet.p4.Eta();
    double area = jet.area;
    int pFl = jet.partonFlavour;
    return getJEC(pt, eta, area, rho, pFl);
}

pair<double,double> JESreader::getTotalUnc (double pt, double eta) const
{
    // up
    uncProv->setJetEta(eta);
    uncProv->setJetPt(pt);
    double uncUp = uncProv->getUncertainty(true); 

    // down
    uncProv->setJetEta(eta);
    uncProv->setJetPt(pt);
    double uncDn = uncProv->getUncertainty(false); 

    return {uncUp, uncDn};
}


vector<float> JESreader::getTotalUncVec (double pt, double eta) const
{   
    vector<float> vec;
    vec.reserve(3); // nominal 0, up 1, down 2
    vec.push_back(1.);
    // up
    uncProv->setJetEta(eta);
    uncProv->setJetPt(pt);
    float uncUp = uncProv->getUncertainty(true); 
    vec.push_back(1.+uncUp);

    // down
    uncProv->setJetEta(eta);
    uncProv->setJetPt(pt);
    float uncDn = uncProv->getUncertainty(false); 
    vec.push_back(1.-uncDn);

    return vec;
}

vector<float> JESreader::getUncVec (double pt /* raw */, double eta) const
{
   vector<float> vec;
   vec.reserve(uncSources.size()+1);
   vec.push_back(1.); //central variation
   for(auto uncSource: uncSources) {
       // note:
       // float JetCorrectionUncertainty::getUncertainty (bool fDirection)

       // up
       uncSource->setJetEta(eta);
       uncSource->setJetPt(pt);
       float uncUp = uncSource->getUncertainty(true); // TODO: L5?
       vec.push_back(1.+uncUp);

       // down
       uncSource->setJetEta(eta);
       uncSource->setJetPt(pt);
       float uncDn = uncSource->getUncertainty(false); // TODO: L5?
       vec.push_back(1.-uncDn);
   }
   return vec;
}

JESreader::JESreader (string dir, string name, float radius, string SmoothFile) :
        // flags
        //isMC(dir.find("MC") != string::npos),
        wFl(dir.find("MC") != string::npos && dir.find("AK4") && dir.find("Flavor") != string::npos),
        smooth(fs::exists(fs::path(SmoothFile))),
        // corrections
        L1Fast(nullptr), L2Relative(nullptr),
        L3Absolute(nullptr), L2L3Residual(nullptr),
        // uncertainties
        uncProv(nullptr)
{
    //if (!isMC) assert(!wFl);
    int iradius = radius < 0.6f ? 4 : 8;

    cout << radius << ' ' << iradius << endl;
    cout << dir << '\n' << name << endl;
    // examples:
    // Summer16_07Aug2017_V15_Flavor_Herwig_MC/Summer16_07Aug2017_V15_Flavor_Herwig_MC_b_L5Flavor_AK4PFchs.txt
    // Summer16_07Aug2017_V14/Summer16_07Aug2017BCD_V14_DATA/Summer16_07Aug2017BCD_V14_DATA_L1FastJet_AK4PFchs.txt
    auto WholePath = [&,name,radius,dir](string type) {
        assert(dir != "");
        return dir + "/" + name + "_" + type + "_AK" + to_string(iradius) + "PFchs.txt";
    };

    auto initCorr = [&](string corrType) {
        auto n = WholePath(corrType);
        cout << "File with correction is " << n << endl;
        auto corPars = new JetCorrectorParameters(n);
        vector<JetCorrectorParameters> corParsVec;
        corParsVec.push_back(*corPars);
        return new FactorizedJetCorrector(corParsVec);
    };

    L1Fast = initCorr("L1FastJet");
    L2Relative = initCorr("L2Relative");
    L3Absolute = initCorr("L3Absolute");
    //if (!isMC) L2L3Residual = initCorr("L2L3Residual");
    L2L3Residual = initCorr("L2L3Residual");

    if (SmoothFile.empty()) cout << "No smooth corrections given" << endl;
    else                    cout << "File with correction is " << SmoothFile << endl;

    if (smooth) InitSmoothL2(SmoothFile.c_str());
    else cout << "NOT applying smooth correction!" << endl;

    if (wFl) {

        // gluon or undefined
        L5Flavor[0] = initCorr("g_L5Flavor");
        L5Flavor[21] = L5Flavor[0];

        // u or d have the same corrections
        L5Flavor[1] = initCorr("ud_L5Flavor");
        L5Flavor[2] = L5Flavor[1];

        // others are unique
        L5Flavor[3] = initCorr("s_L5Flavor");
        L5Flavor[4] = initCorr("c_L5Flavor");
        L5Flavor[5] = initCorr("b_L5Flavor");
    }

    uncProv = new JetCorrectionUncertainty(WholePath("Uncertainty"));    

    for(auto n: getJECUncNames()) {
       JetCorrectorParameters *p = new JetCorrectorParameters(WholePath("UncertaintySources"), n.Data());
       uncSources.push_back(new JetCorrectionUncertainty(*p));
    }

}

vector<TString> JESreader::getJECUncNames()
{
    vector<TString> srcnames 
    {
        //flat absolute scale uncertainties. Main uncertainties are combined photon, Z->ee (electron) and Z->mumu (tracking) reference scale (AbsoluteScale) and correction for FSR+ISR (MPFBias). (AbsoluteFlavMap is obsolete and always zero)
        "AbsoluteStat", "AbsoluteScale",  "AbsoluteMPFBias",
        //"AbsoluteFlavMap",

        //high pT extrapolation. Based on Pythia6 Z2/Herwig++2.3 differences in fragmentation and underlying event (FullSim). (not yet updated since Run I)
        "Fragmentation",

        //high pT extrapolation. Based on propagation of +/-3% variation in single particle response in ECAL to PF Jets (FastSim). (shape and magnitude from Run I)
        "SinglePionECAL",
        //high pT extrapolation. Based on propagation of +/-3% variation in single particle response in HCAL to PF Jets (FastSim). (shape from Run I, magnitude refit in Run II)
        "SinglePionHCAL",

        // jet flavor. Based on Pythia6 Z2/Herwig++2.3 differences in uds/c/b-quark and gluon responses. Default flavor uncertainty is FlavorQCD. There exist alternative flavor uncertainty sources for other flavor compositions: FlavorZJet (Z+jet events), FlavorPhotonJet (gamma+jet events), FlavorPureGluon (only gluons), FlavorPureQuark (only uds), FlavorPureCharm (only c) and FlavorPureBottom (only b). (not yet updated since Run I)
        "FlavorQCD",
        //"FlavorZJet", "FlavorPhotonJet", "FlavorPureGluon", "FlavorPureQuark", "FlavorPureCharm", "FlavorPureBottom",

        //JEC time dependence between BCD, EF, G and H. The L2L3Residual is now corrected per epoch, so !TimePtEta! uncertainty is taken as a difference between the luminosity-weighted average of corrections per epoch, and a single L2L3Residual correction derived on the full BCDEFGH sample. The uncertainties for individual epochs (_TimeRun[BCD][EF][G][H]) are calculated as a weighted difference to BCDEFGH such that their luminosity-weighted sum in quadrature equals TimePtEta.
        "TimePtEta",
        //"TimeRunBCD", "TimeRunEF", "TimeRunGH",

        //eta-dependence uncertainty from jet pT resolution (JER). Derived by varying JER scale factors up and down by quoted uncertainties. The JER uncertainties are assumed fully correlated for endcap within tracking (EC1), endcap outside tracking (EC2) and hadronic forward (HF).
        "RelativeJEREC1", "RelativeJEREC2", "RelativeJERHF",

        //half-difference between MPF method log-linear (default) and constant fits versus pT. The pT-dependence has increased considerably since Run I, but the reason is not yet understood, and leads to more ambiguity in the parameterization. Therefore these uncertainties may not be entirely reliable yet.
        "RelativePtBB", "RelativePtEC1", "RelativePtEC2", "RelativePtHF",

        //full difference between log-linear fits of MPF and pT balance methods. This difference was negligible in Run I, but is inexplicably large in Run II. The reason is not yet understood, and this uncertainty source may still underestimate the true uncertainty.
        "RelativeBal",

        // eta-dependent uncertainty due to difference between relative residuals observed with dijet, Z+jets and gamma+jets (New, added in JEC for "07Aug" ReReco)
        "RelativeSample",

        //eta-dependence uncertainty due to correction for initial and final state radiation, estimated from difference between MPF log-linear L2Res from Pythia8 and Herwig++, after each has been corrected for their own ISR+FSR correction.
        "RelativeFSR",

        //statistical uncertainty in determination of eta-dependence, calculated from the error matrix of the FSR correction fit vs eta or L2Res log-linear fit vs pT. Latter is only important in endcap (EC) and in HF, where the uncertainty is provided as correlated over these wider regions.
        "RelativeStatFSR", "RelativeStatEC", "RelativeStatHF",

        //estimating 5% uncertainty on the data/MC scale factor for offset correction, which roughly covers the variation versus mu seen in the Random Cone method applied on Zero Bias data and neutrino gun MC.
        "PileUpDataMC",

        //pile-up offset dependence on jet pT is estimated from matched MC with and without PU overlay. The uncertainty is taken as difference to Random Cone method (i.e. essentially difference of PU inside and outside of jets), propagated through L2 (BB,EC,HF) and L3 (Ref) data-driven methods. The component absorbed in L2 and L3 data-driven methods is provided as special optional PileUpMuZero source, which should be used instead of PileUpPt source for no pile-up (or almost zero pileup, Mu=Zero) data. The original MC truth and Random Cone difference is provided as PileUpEnvelope source for expert use.
        "PileUpPtRef", "PileUpPtBB", "PileUpPtEC1", "PileUpPtEC2", "PileUpPtHF",
        //"PileUpMuZero", "PileUpEnvelope",

        //Total Unertainties
        //"SubTotalPileUp",
        //"SubTotalRelative",
        //"SubTotalPt",
        //"SubTotalScale",
        //"SubTotalAbsolute",
        //"SubTotalMC",
        //"Total",
        //"TotalNoFlavor",
        //"TotalNoTime",
        //"TotalNoFlavorNoTime",

        /*
           [CorrelationGroupMPFInSitu]
           [CorrelationGroupIntercalibration]
           [CorrelationGroupbJES]
           [CorrelationGroupFlavor]
           [CorrelationGroupUncorrelated]
           */
    };
    return  srcnames;
}
