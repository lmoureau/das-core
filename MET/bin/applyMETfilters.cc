#include <cstdlib>
#include <cassert>
#include <iostream>

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"
#include "Core/CommonTools/interface/ControlPlots.h"

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TProfile.h>

#include "Math/VectorUtil.h"

#include "Core/MET/interface/applyMETfilters.h"

struct METfractionCut {

    const TString name;

    // from MET
    TH1 * Et, * SumEt, * Fraction, * Pt, * Phi;

    TH2 * pt_y;

    METfractionCut (TString Name) : //!< Constructor, initialises all histograms
        name    (Name),
        Et      (new TH1F(Name + "Et", "Et", 100, 1, 1000)),
        SumEt   (new TH1F(Name + "SumEt", "SumEt", 650, 1, 6500)),
        Fraction(new TH1F(Name + "Fraction", "Fraction", 100, 0, 1)),
        Pt      (new TH1F(Name + "Pt", "Pt", 100, 1, 1000)),
        Phi     (new TH1F(Name + "Phi", "Phi", 314, -M_PI, M_PI)),
        pt_y    (new TH2F(Name + "pt_y", "pt_y", nRecBins, recBins.data(), nYbins, y_edges.data()))
    {
        for (TH1 * h: {Et, SumEt, Fraction, Pt, Phi})
            h->SetDirectory(0);
        pt_y->SetDirectory(0);
    }

    void Fill //!< Fill all histograms directly
        (Event * ev, //!< event info (run number, etc, but also weight)
         MET * met,  //!< MET info
         vector<RecJet> * recJets) //!< rec jets
    {
        auto w = ev->recWgts.front();
        if (ev->genWgts.size() > 0) w *= ev->genWgts.front();

        // from MET
        Et->Fill(met->Et, w);
        SumEt->Fill(met->SumEt, w);
        Fraction->Fill(met->Et/met->SumEt, w);
        Pt->Fill(met->Pt, w);
        Phi->Fill(met->Phi, w);

        // jet
        for (const RecJet& jet: *recJets)  {
            float jW = jet.weights.front();
            pt_y->Fill(jet.CorrPt(), jet.AbsRap(), w*jW);
        }
    }

    void Write (TFile * f) const
    {
        f->cd();
        TDirectory * d = f->mkdir(name);
        d->cd();
        for (TH1 * h: {Et, SumEt, Fraction, Pt, Phi,
                        dynamic_cast<TH1*>(pt_y)}) {
            h->SetDirectory(d);
            TString n = h->GetName();
            n.ReplaceAll(name,"");
            h->Write(n);
        }
        f->cd();
    }
};

////////////////////////////////////////////////////////////////////////////////
/// Apply MET filters, both for MC and Data
/// following the official recommendations
void applyMETfilters 
             (TString input,  //!< name of input root file 
              TString output, //!< name of output root file
              int nSplit = 1, //!< number of jobs/tasks/cores
              int nNow = 0)   //!< index of job/task/core
{

    // Get old file, old tree and set top branch address
    TChain * oldchain = new TChain("inclusive_jets"); // arg = path inside the ntuple
    oldchain->Add(input);

    MET * met = nullptr;
    Event * event = nullptr;
    vector<RecJet> * recJets = nullptr;
    oldchain->SetBranchAddress("event", &event);
    oldchain->SetBranchAddress("met", &met);
    oldchain->SetBranchAddress("recJets", &recJets);

    TFile * file = TFile::Open(output, "RECREATE");
    TTree * newtree = oldchain->CloneTree(0);

    MetaInfo metainfo(newtree);
    if (nNow == 0) metainfo.Print();
    metainfo.AddCorrection("METfilter");
    bool isMC = metainfo.isMC();
    int year = metainfo.year();
    assert(year > 2015 && year < 2019);

    METfilters metfilters(input, year);
    cout<<"year "<<year<<endl;

    /**** declaring a few histograms to control effect of filters ****/

    METfractionCut beforeCut("beforeMETcut"), afterCut("afterMETcut");

    ControlPlots METbefore("METbefore"),
                 METafterAllMETfilters("METafterAllMETfilters"),
                 METafterMETfraction("METafterMETfraction"),
                 METafterAllMETfiltersAndMETfraction ("METafterAllMETfiltersAndMETfraction");

    vector<ControlPlots> METfilter ;
    for (TString METname: metfilters.METnames)
            METfilter.push_back(ControlPlots(METname));

    auto totRecWgt = [&](size_t i) {
        return (isMC ? event->genWgts.front() : 1) * event->recWgts.at(i);
    };

    /**** looping ****/

    Looper looper(__func__, oldchain, nSplit, nNow);
    while (looper.Next()) {

        // sanity check
        for (RecJet& jet: *recJets)
            if (jet.weights.size() == 0) jet.weights.push_back(1);

        METbefore(*recJets, totRecWgt(0));

        beforeCut.Fill(event, met, recJets);

        if (met->Et < 0.3 * met->SumEt){ 
            afterCut.Fill(event, met, recJets);
            METafterMETfraction(*recJets, totRecWgt(0));
        }


        for (size_t ibit = 0; ibit < metfilters.METbitsToApply.size(); ++ibit) {
            bool bit = met->Bit.at(ibit);
            if (bit)
                METfilter.at(ibit)(*recJets, totRecWgt(0));
        }

        // IMPORTANT NOTE:
        // in case the MET fraction based cut would be used,
        // the description of the MET class should be changed!!
        
        metfilters(met, event, recJets);
        METafterAllMETfilters(*recJets, totRecWgt(0));
    
        if (met->Et < 0.3 * met->SumEt)
            METafterAllMETfiltersAndMETfraction(*recJets, totRecWgt(0));

        newtree->Fill();
    }

    /* close */
    newtree->Write();
    beforeCut.Write(file);
    afterCut.Write(file);
    METbefore.Write(file);
    METafterMETfraction.Write(file);
    METafterAllMETfilters.Write(file);
    METafterAllMETfiltersAndMETfraction.Write(file);
    for (size_t i = 0; i < METfilter.size(); ++i)
        METfilter.at(i).Write(file);
    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    gROOT->SetBatch();
    TH1::SetDefaultSumw2();

    if (argc < 3) {
        cout << argv[0] << " input output [nSplit [nNow]]\n"
             << "\twhere\tinput and output are n-tuples\n"
             << "NB: if the input name contains 'Herwig16', then BadPFMuonFilter will be skipped" << endl;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2];
    int nNow = 0, nSplit = 1;
    if (argc > 3) nSplit = atoi(argv[3]);
    if (argc > 4) nNow = atoi(argv[4]);

    applyMETfilters(input, output, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
