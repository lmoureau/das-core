#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <vector>
#include <filesystem>
#include <limits>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"

#include <TROOT.h>
#include <TString.h>
#include <TFile.h>
#include <TH2D.h>

#include "Math/VectorUtil.h"

using namespace std;
using namespace DAS;

namespace fs = filesystem;

static const auto feps = numeric_limits<float>::epsilon();

vector<double> GetTriggerTurnons (int year, int R)
{
    ifstream file;
    fs::path p = getenv("DAS_WORKAREA");
    p /= Form("tables/triggers/%d/ak%d.txt", year, R);
    assert(fs::exists(p));
    file.open(p.c_str());

    vector<double> edges(1, 30);
    float HLT, PF;
    do {
        file >> HLT >> PF;
        if (PF > edges.back()) edges.push_back(PF);
    }
    while (file.good() && !file.fail());
    edges.push_back(6500);

    file.close();

    return edges;
}

////////////////////////////////////////////////////////////////////////////////
/// Get MET fraction for each jet trigger separately
void getMETfraction 
             (const fs::path& input,  //!< name of input root file 
              const fs::path& output, //!< name of output root file
              int nSplit = 1, //!< number of jobs/tasks/cores
              int nNow = 0)   //!< index of job/task/core
{
    assert(fs::exists(input));
    auto source = TFile::Open(input.c_str(), "READ");
    auto tree = dynamic_cast<TTree *>(source->Get("inclusive_jets"));
    assert(tree != nullptr);

    MetaInfo metainfo(tree);
    if (nNow == 0) metainfo.Print();
    int year = metainfo.year();
    int R = metainfo.radius()*10;
    assert(R > 0);

    Event * ev = nullptr;
    MET * met = nullptr;
    tree->SetBranchAddress("event", &ev);
    tree->SetBranchAddress("met", &met);
    
    vector<RecJet> * recJets = nullptr;
    tree->SetBranchAddress("recJets", &recJets);

    vector<double> MET_edges;
    for (float edge = 0; edge <= 1; edge += 0.01)
        MET_edges.push_back(edge);
    int nMETbins = MET_edges.size()-1;

    vector<double> trig_edges = GetTriggerTurnons(year, R);
    int nTrigBins = trig_edges.size() - 1;

    if (fs::exists(output))
        cerr << output << " will be overwritten\n";
    auto file = TFile::Open(output.c_str(), "RECREATE");

    TH2 * METfraction = new TH2F("METfraction", ";MET fraction;p^{leading}_{T}   (GeV)",
                                    nMETbins, MET_edges.data(),
                                    nTrigBins, trig_edges.data());

    Looper looper(__func__, tree, nSplit, nNow);
    while (looper.Next()) {

        //cout << "=====\n";
        if (recJets->empty()) continue;
        static auto rapAcc = y_edges.back();
        if (abs(recJets->front().Rapidity()) >= rapAcc) continue;
        if (abs(met->SumEt) < feps) continue;

        auto pt = recJets->front().CorrPt();
        auto frac = met->Et/met->SumEt;
        auto w = ev->genWgts.front() * ev->recWgts.front();

        //cout << setw(15) << pt << setw(15) << frac << setw(15) << w << endl;

        // we do not use the jet weight,
        // because the pt is only determined 
        // to find the right trigger window
        // -> only the event weight matters
        METfraction->Fill(frac, pt, w);
    }
    file->Write();
    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 3) {
        cout << argv[0] << " input output [nSplit [nNow]]" << endl;
        return EXIT_SUCCESS;
    }

    fs::path input = argv[1],
         output = argv[2];
    int nNow = 0, nSplit = 1;
    if (argc > 3) nSplit = atoi(argv[3]);
    if (argc > 4) nNow = atoi(argv[4]);

    getMETfraction(input, output, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
