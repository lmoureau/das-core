#include <vector>
#include <iostream>
#include <TString.h>

#include "Core/CommonTools/interface/variables.h"
#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Jet.h"

using namespace std;
using namespace DAS;


static const TString METprefix = "MET";

struct METfilters {
    vector<TString> METnames;
    vector<int> METbitsToApply;

    // NOTE: this is taken from the python config file, check that the name
    //       still make sense.....
    METfilters (TString input, const int year)
    {
        switch (year) {
            case 2016:
                METnames = {"goodVertices", "globalSuperTightHalo2016Filter", "HBHENoiseFilter",
                            "HBHENoiseIsoFilter", "EcalDeadCellTriggerPrimitiveFilter",
                            "BadPFMuonFilter",  /*this one destroys Herwig16, for some reason*/
                            "BadPFMuonDzFilter", //always return null value, to be better studied
                            "BadChargedCandidateFilter", /*this one too, but anyway, it is no longer recommended for any */
                            "eeBadScFilter" /*this one is useless*/};
                
                METbitsToApply = {0, 1, 2, 3, 4, 5, /*6, 7,*/ 8};
                break;
            case 2017:
            case 2018:
                // filters 6 and 9 always null, to be better investigated!!! filters 7 and 8 not recommended
                METnames = {"goodVertices", "globalSuperTightHalo2016Filter", "HBHENoiseFilter",
                            "HBHENoiseIsoFilter", "EcalDeadCellTriggerPrimitiveFilter",
                            "BadPFMuonFilter",  /*this one destroys Herwig16, for some reason*/
                            "BadPFMuonDzFilter",
                            "BadChargedCandidateFilter",
                            "eeBadScFilter",
                            "hfNoisyHitsFilter",
                            "ecalBadCalibFilter"};
                METbitsToApply = {0, 1, 2, 3, 4, 5, /*6, 7,*/ 8, /*9,*/ 10};
                break;
            default:
                cerr << year << " is not (currently) not handled. Aborting.\n";
                exit(EXIT_FAILURE);
        }
        if(year==2016)
        // flag to avoid destroying the Herwig sample
        if (input.Contains("Herwig16")) {
            cout << "BadPFMuonFilter WILL BE SKIPPED!!!!!" << endl;
            for (auto it = METbitsToApply.begin(); it != METbitsToApply.end(); /* nothing */) {
                if (*it == 5) it = METbitsToApply.erase(it);
                else          ++it;
            }
        }

        cout << "MET bits:";
        for (auto bit: METbitsToApply)
            cout << ' ' << bit;
        cout << endl;

    }

    // define histos for CP: energy fraction for each trigger
    //fraction = new TH1F("fraction", "fraction", 100, 0, 0.6);

    void operator() (MET * met, Event * event, vector<RecJet> * recJets)
    {

        // testing MET filters
        bool passFilters = true;
        // BadChargedCandidateFilter is no longer recommended
        for (size_t ibit = 0; ibit < METnames.size(); ++ibit) {

            bool bit = met->Bit.at(ibit);

            if (find(METbitsToApply.begin(), METbitsToApply.end(), ibit) != METbitsToApply.end())
                passFilters = passFilters && bit;
        }

        if (!passFilters)
            // reconstructed jets should be removed, so one may be tempted to remove the event...
            // but for MC, generated jets should *not* be removed -> just empty the list of rec jets
            recJets->clear();
    }

};
