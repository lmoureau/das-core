#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <filesystem>
#include <algorithm>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/terminal.h"
#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"

#include "common.h"

#include <TROOT.h>
#include <TString.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TProfile.h>

#include "Math/VectorUtil.h"

using namespace std;
using namespace DAS;

namespace fs = filesystem;

namespace MN_Helper {

////////////////////////////////////////////////////////////////////////////////
/// Contains the histograms that are filled in getMNobservables.
/// These histograms are filled through the different Fill() methods.
/// Beware that the three Fill() methods are different.
/// The binning of the histograms have been chosen randomly. (TODO: change this)
///
/// The attributes of this structure are the following:
/// - name        --> name of the instance to avoid memory leak
/// - njets       --> histogram of the number of jets in the event
/// - nevtdeta    --> number of Mueller-Navelet jets events binned in delta eta
/// - dsigmadphi  --> $(\pi - \Delta \phi)$ between the MN Jets
/// - ptleading   --> $p_T$ of the leading jet (in term of pt)
/// - ptratmn     --> ratio of pt of the mnjets (see the Obs2Jets)
/// - pt          --> pt of all the jets in the event
/// - ptmnfwd     --> pt of the most fwd MN jet
/// - ptmnbwd     --> pt of the most bwd MN jet
/// - njetsptave  --> 2D histogram jets multiplicity and average pt of the MN jets
/// - njetsdeta   --> 2D histogram jets multiplicity and delat eta btw the MN jets
/// - ptavedeta   --> 2D histogram binned in $p_{T,MN,ave}$ and $ \Delta \eta$
/// - dphideta    --> 2D histogram binned in $\Delta\phi$ and $\Delta\eta$
/// - cos1,2,3    --> TProfile containing $ \left\langle \cos \left( n \left( \pi - \Delta \phi \right) \right) \right\rangle $ for $n=1,2,3$
/// - ptavemini   --> See Obs2Jets
/// - detaavemini --> See Obs2Jets
/// - retaavemini --> See Obs2Jets
/// - rptexpdeta  --> See Obs2Jets
struct Hist {
    TString name;

    TH1F njets, nevtsdeta, dsigmadphi, ptleading, ptratmn, pt, ptmnfwd, ptmnbwd;
    TH2F njetsptave, njetsdeta, ptavedeta, dphideta;
    TProfile cos1, cos2, cos3;

    TH1F ptavemini, detaavemini, retaavemini, rptexpdeta;

    Hist (TString thename) :
        name(thename),
        njets(name+"njets","njets",20,0,20),
        nevtsdeta(name+"nevtsdeta","nevtsdeta",30,0,10),
        dsigmadphi(name+"dsigmadphi","dsigmadphi",30,-M_PI,M_PI),
        ptleading(name+"ptlead","pt of the leading jet", 30, 0, 300),
        ptratmn(name+"ptratmn","ptratmn",30,0,1),
        pt(name+"pt","pt",200,0,1000),
        ptmnfwd(name+"ptmnfwd","ptmnfwd",200,0,300),
        ptmnbwd(name+"ptmnbwd","ptmnbwd",200,0,300),
        njetsptave(name+"njetsptave","njetsptave",30,0,300,20,0,20),
        njetsdeta(name+"njetsdeta","njetsdeta",30,0,10,10,0,10),
        ptavedeta(name+"ptavedeta","ptavedeta",30,0,300,30,0,20),
        dphideta(name+"dphideta","dphideta",30, -M_PI, M_PI, 30, 0, 20),
        cos1(name+"cos1","cos1",30,0,10,0,1),
        cos2(name+"cos2","cos2",30,0,10,0,1),
        cos3(name+"cos3","cos3",30,0,10,0,1),
        ptavemini(name+"ptavemini","ptavemini",50,0,80),
        detaavemini(name+"detaavemini","detaavemini",50,0,5),
        retaavemini(name+"retaavemini","retaavemini",50,0,1),
        rptexpdeta(name+"rptexpdeta","rptexpdeta",50,0,2)
    { }

    void Fill (vector<RecJet>& recJets, float evweight){
        auto jetswgt = (recJets.front()).weights.front();
        for (auto it = next(recJets.begin()) ; it != recJets.end() ; ++it)
            jetswgt *= (it->weights).front();
        for (auto it = recJets.begin() ; it != recJets.end() ; ++it)
            pt.Fill(it->CorrPt(), evweight * (it->weights).front());
        njets.Fill( recJets.size(), evweight*jetswgt );
        ptleading.Fill( (recJets.front()).CorrPt(), evweight * (recJets.front()).weights.front() );
    }

    void Fill (vector<GenJet>& genJets, float evweight){
        auto jetswgt = (genJets.front()).weights.front();
        for (auto it = next(genJets.begin()) ; it != genJets.end() ; ++it)
            jetswgt *= (it->weights).front();
        for (auto it = genJets.begin() ; it != genJets.end() ; ++it)
            pt.Fill( (it->p4).Pt(), evweight * (it->weights).front() );
        njets.Fill(genJets.size(), evweight * jetswgt);
        ptleading.Fill( (genJets.front()).p4.Pt(), evweight * (genJets.front()).weights.front() );
    }

    void Fill (Obs2Jets& mn2Jets, ObsMiniJets& miniJets, float evweight){
        float w1 = evweight * mn2Jets.weight;
        float w2 = evweight * miniJets.weight;
        dsigmadphi.Fill( mn2Jets.DPhi() , w1 );
        njetsptave.Fill( mn2Jets.PtAve(), miniJets.size()+2, w1 );
        njetsdeta.Fill( mn2Jets.DEta(), miniJets.size()+2, w1 );
        ptratmn.Fill( mn2Jets.PtRatMN(), w1 );
        ptavedeta.Fill( mn2Jets.PtAve(), mn2Jets.DEta(), w1 );
        dphideta.Fill( mn2Jets.DPhi(), mn2Jets.DEta(), w1 );

        ptmnfwd.Fill( mn2Jets.PtFWD(), evweight * mn2Jets.weightFWD );
        ptmnbwd.Fill( mn2Jets.PtBWD(), evweight * mn2Jets.weightBWD );

        cos1.Fill( mn2Jets.DEta(), mn2Jets.CosDPhi(1), w1 );
        cos2.Fill( mn2Jets.DEta(), mn2Jets.CosDPhi(2), w1 );
        cos3.Fill( mn2Jets.DEta(), mn2Jets.CosDPhi(3), w1 );

        ptavemini.Fill(miniJets.PtAveMini(), w2 );
        detaavemini.Fill(miniJets.DEtaAveMini(), w2 );
        retaavemini.Fill(miniJets.REtaAveMini(), w2 );
        rptexpdeta.Fill(miniJets.RPtExpDEta(), w2 );
    }

private:
    template<typename HIST> void Loop (vector<HIST *> hists, TDirectory * dir)
    {
        for (HIST * h: hists){
            h->SetDirectory(dir);
            TString histoname(h->GetName());
            histoname.ReplaceAll(name,"");
            h->Write(histoname);
        }
    }

public:
    void Write (TDirectory * dir)
    {
        dir->cd();
        auto subdir = dir->mkdir(name);
        subdir->cd();
        Loop<TH1F>({&njets, &nevtsdeta, &dsigmadphi, &ptleading, &ptratmn, &ptmnfwd, &ptmnbwd, &pt,
                    &ptavemini, &detaavemini, &retaavemini, &rptexpdeta}, subdir);
        Loop<TH2F>({&njetsptave, &njetsdeta, &ptavedeta, &dphideta}, subdir);
        Loop<TProfile>({&cos1, &cos2, &cos3}, subdir);
    }
};

} // end of namespace

using namespace MN_Helper;

////////////////////////////////////////////////////////////////////////////////
///  Calculate the observables that can be used to caracterise Mueller-Navelet Jets
void getMNobservables 
             (const fs::path& input,  //!< name of input root file
              const fs::path& output, //!< name of output root file
              int nSplit = 1, //!< number of jobs/tasks/cores
              int nNow = 0)   //!< index of job/task/core
{
    assert(fs::exists(input));
    auto source = TFile::Open(input.c_str(), "READ");
    auto tree = dynamic_cast<TTree *>(source->Get("inclusive_jets"));

    MetaInfo metainfo(tree);
    if (nNow == 0) metainfo.Print();
    bool isMC = metainfo.isMC();

    Event * ev = nullptr;
    tree->SetBranchAddress("event", &ev);

    vector<RecJet> * recJets = nullptr;
    vector<GenJet> * genJets = nullptr;
    tree->SetBranchAddress("recJets", &recJets);

    if (isMC)
        tree->SetBranchAddress("genJets", &genJets);

    if (fs::exists(output))
        cerr << red << output << " will be overwritten!\n" << normal;
    auto file = TFile::Open(output.c_str(), "RECREATE");

    Hist genHist("gen"), recHist("rec");

    Looper looper(__func__, tree, nSplit, nNow);
    while (looper.Next()) {

        if (recJets->size() >= 2){
            float evweight = ev->recWgts.front();
            if (isMC) evweight*=ev->genWgts.front();
            recHist.Fill(*recJets, evweight);

            sort(recJets->begin(), recJets->end(),
                    [](const RecJet& Jet1, const RecJet& Jet2)
			{ return Jet1.CorrEta() > Jet2.CorrEta(); });

            auto LeadingMN = recJets->begin();
            auto SubleadingMN = prev(recJets->end());
            Obs2Jets mn2Jets(*LeadingMN, *SubleadingMN);
            recJets->erase(LeadingMN);
            recJets->erase(SubleadingMN);
            ObsMiniJets miniJets(*recJets);

            recHist.Fill( mn2Jets, miniJets, evweight);
        }

        if (isMC && genJets->size() >= 2){
            float evweight = ev->genWgts.front();
            genHist.Fill(*genJets, evweight);

            sort(genJets->begin(), genJets->end(),
                [](const GenJet& Jet1, const GenJet& Jet2)
                    { return Jet1.p4.Eta() > Jet2.p4.Eta(); });

            auto LeadingMN=genJets->begin();
            auto SubleadingMN=prev(genJets->end());
            Obs2Jets mn2Jets(*LeadingMN, *SubleadingMN);
            genJets->erase(LeadingMN);
            genJets->erase(SubleadingMN);
            ObsMiniJets miniJets(*genJets);

            genHist.Fill(mn2Jets, miniJets, evweight);
        }
    }
    file->cd();
    recHist.Write(file);
    genHist.Write(file);
    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 3) {
        cout << argv[0] << " input output [nSplit [nNow]]\n"
             << "where\tinput = path to the root ntuple (in DAS format)\n"
             << "     \toutput = path to the output .root file containing histograms\n"
             << flush;
        return EXIT_SUCCESS;
    }

    fs::path input = argv[1],
             output = argv[2];
    int nNow = 0, nSplit = 1;
    if (argc > 3) nSplit = atoi(argv[3]);
    if (argc > 4) nNow = atoi(argv[4]);

    getMNobservables(input, output, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
