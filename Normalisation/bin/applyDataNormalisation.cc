#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <map>
#include <filesystem>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/terminal.h"
#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/ControlPlots.h"

#include <TROOT.h>
#include <TFile.h>
#include <TChain.h>
#include <TH2.h>

#include "applyDataNormalisation.h"
#include "TriggerEff.h"

using namespace std;
using namespace DAS;

namespace fs = filesystem;
////////////////////////////////////////////////////////////////////////////////
/// Apply the the kinematic selection, ensuring that events in the dataset have fired a trigger and
/// correcting the weight of the events with the associated trigger prescales, trigger efficiency,
/// and total luminosity.
/// The weight $\frac{ \text{Prescale} * \mathcal{L}^{-1} }{ \epsilon_\text{trigger} } $ is applied by event.
/// The argument 'strategy' defines the selection criterion of the events and the calculation of the weight
/// (see functor in applyDataNormalisation.h).
void applyDataNormalisation
        (const fs::path& input,          //!< name of input root file
         const fs::path& output,         //!< name of output root file
         const fs::path& lumi_file,      //!< path to text file with effective luminosities
         const fs::path& turnon_file,    //!< path to text file with turn-on points
         const fs::path& trigger_curves, //!< file with efficiency curves
         const string& strategy,               //!< 'eta'-> sort the jets in eta and try to match the most fwd and second most fwd,
                                         //!< 'pt'->  assume that the jets are sorted in pt and try to match the leading jet
         const string& method,           //!< 'presc'-> use the prescales in dataset, 'lumi'-> use lumis in 'lumi_file'
         int nSplit = 1,             //!< number of jobs/tasks/cores
         int nNow = 0)               //!< index of job/task/core
{
    assert(fs::exists(input));
    TChain * oldchain = new TChain("inclusive_jets");
    oldchain->Add(input.c_str());

    vector<RecJet> * recJets = nullptr;
    vector<FourVector> * hltJets = nullptr;
    Event * event = nullptr;
    Trigger * trigger = nullptr;
    oldchain->SetBranchAddress("recJets", &recJets);
    oldchain->SetBranchAddress("event", &event);
    oldchain->SetBranchAddress("trigger", &trigger);
    oldchain->SetBranchAddress("hltJets", &hltJets);

    if (fs::exists(output))
        cerr << red << output << " will be overwritten\n" << normal;
    auto newfile = TFile::Open(output.c_str(), "RECREATE");
    auto newtree = oldchain->CloneTree(0);

    MetaInfo metainfo(newtree);
    if (nNow == 0) metainfo.Print();
    metainfo.AddCorrection("normalised");
    if (strategy == string("eta"))
        metainfo.AddCorrection("eta_ordered");

    int year = metainfo.year();
    assert(year > 2015 && year < 2019);

    // Calling functor to apply data prescales
    DataNormalisation normalisation (lumi_file, turnon_file, trigger_curves, strategy, method, year);
    newfile->cd();

    // a few control plots
    ControlPlots::isMC = false;
    ControlPlots corrNoTrigEff("corrNoTrigEff");

    vector<ControlPlots> corr ;
    for (int i = 0 ; i < metainfo.GetNRecEvWgts() ; i++)
        corr.push_back(ControlPlots(metainfo.GetRecEvWgt(i)));

    cout << "looping over events:" << endl;

    Looper looper(__func__, oldchain, nSplit, nNow);
    while (looper.Next()) {
        RecJet leadingJet;
        if (recJets->size() == 0) continue;
        leadingJet = normalisation(*event, *recJets, *hltJets, *trigger); // the normalisation it self is done in normalisation

        // TODO: reimplement the logic to avoid a selection on the weight
        if (event->recWgts.front()<=0) continue;

        newtree->Fill();

        double evtWgts = event->recWgts.front();
        double efficiency = normalisation.eff(leadingJet);

        if (efficiency<=0) continue;
        corrNoTrigEff(*recJets, evtWgts*efficiency); // compensate
        for (size_t i = 0; i < corr.size(); ++i)
            corr.at(i)(*recJets, event->recWgts.at(i));

        // Exclusive curves are filled (i.e. one event can populate only a trigger curve).
        // The ibit value is determined by
        // the leading jet but also the other jets of the event can populate the trigger curve.
        for (auto& jet: *recJets) {
            auto y = abs(jet.Rapidity());
            auto w = event->recWgts.front();
            normalisation.eff.contribs.at(normalisation.ibit)->Fill(jet.CorrPt(), y, w);
        }
    }

    cout << "saving" << endl;
    newtree->AutoSave();
    corrNoTrigEff.Write(newfile);
    for (size_t i = 0; i < corr.size(); ++i)
        corr.at(i).Write(newfile);
    TDirectory * controlplots = newfile->mkdir("controlplots");
    controlplots->cd();
    for (TH2 * h: normalisation.eff.contribs) {
        h->SetDirectory(controlplots);
        h->Write();
    }

    if (nNow == 0) {
        normalisation.eff.h->SetDirectory(controlplots);
        normalisation.eff.h->Write();
    }

    newfile->Write();
    newfile->Close();
    delete oldchain;
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    gROOT->SetBatch();

    if (argc < 5) {
        cout << argv[0] << " input output lumi_file turnon_file trigger_curves [nSplit [nNow]]\n"
             << "where\tinput = data n-tuple\n"
             << "     \toutput = new n-tuple\n"
             << "     \tlumi_file = 2-column file with 'trigger lumi' (should be in `$DAS_WORKAREA/tables/lumi`)\n"
             << "     \tturnon_file = 2-column file with 'trigger turn-on' (output of `getTriggerTurnons`, should also be in `$DAS_WORKAREA/tables/triggers`)\n"
             << "     \ttrigger_turnons = root file with output of `getTriggerTurnons`\n"
             << "     \tstrategy = 'eta'-> sort the jets in eta and try to match the most fwd and second most fwd,\n"
             << "\t \t \t    'pt' -> assume that the jets are sorted in pt and try to match the leading jet\n"
             << "     \tmethod = 'presc' -> apply the prescales, 'lumi' --> use the effective luminosities"
             << endl;
        return EXIT_SUCCESS;
    }

    fs::path input = argv[1],
             output = argv[2],
             lumi_file = argv[3],
             turnon_file = argv[4],
             trigger_curves = argv[5];

    string strategy = argv[6],
           method = argv[7];
    int nNow = 0, nSplit = 1;
    if (argc > 8) nSplit = atoi(argv[8]);
    if (argc > 9) nNow = atoi(argv[9]);

    applyDataNormalisation(input, output, lumi_file, turnon_file, trigger_curves, strategy, method, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
