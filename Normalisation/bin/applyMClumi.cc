#include <cstdlib>
#include <thread>
#include <iostream>
#include <filesystem>
#include <optional>

#include <TChain.h>
#include <TFile.h>
#include <TROOT.h>
#include <TH1D.h>
#include <TRegexp.h>

#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Jet.h"

#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/terminal.h"
#include "Core/CommonTools/interface/ControlPlots.h"

#include "Core/PUstaubSauger/interface/sigma.h"

using namespace std;
using namespace DAS;

namespace fs = filesystem;

////////////////////////////////////////////////////////////////////////////////
/// Sum the weights from root file
float getSumOfWeights
    (const fs::path& sumOfWeights) //!< path to root file containing a trivial histogram with the sum of the weights
{
    // get sum of weights from dummy histogram
    TFile * file = TFile::Open(sumOfWeights.c_str(), "OPEN");
    auto hSumWgt = dynamic_cast<TH1*>(file->Get("hSumWgt"));
    auto sumWgts = hSumWgt->GetBinContent(1);
    assert(sumWgts > 0);
    file->Close();
    return sumWgts;
}

////////////////////////////////////////////////////////////////////////////////
/// Normalise with sum of weights (to be computed) and cross section (given)
///
/// *Note*: a cut-off for events with hard scale above 5 TeV is applied, since these events are not realistic
void applyMClumi
        (const fs::path& input,         //!< name of input root file with raw *n*-tuple
         const fs::path& output,        //!< name of output root file with normalised *n*-tuple
         const fs::path& correction,    //!< name of txt file with the parameters for cut-off function
         float xsection,        //!< float value for cross section of the MC sample
         int nSplit = 1,        //!< number of jobs/tasks/cores
         int nNow = 0)          //!< index of job/task/core
{
    assert(fs::exists(input));

    // normalisation factor
    auto sumw = getSumOfWeights(input);
    auto factor = xsection/sumw;
    cout << xsection << '\t' << sumw << '\t' << factor << endl;
    assert(factor > 0);

    // input
    TChain * oldchain = new TChain("inclusive_jets"); // arg = path inside the ntuple
    oldchain->Add(input.c_str());

    // saving MC weight 

    if (fs::exists(output)) 
        cerr << red << output << " will be overwritten\n" << normal;
    auto newfile = TFile::Open(output.c_str(), "RECREATE");
    auto newtree = oldchain->CloneTree(0);

    // meta information
    MetaInfo metainfo(newtree);
    if (nNow == 0) metainfo.Print();
    metainfo.AddCorrection("normalisation");
    assert(metainfo.isMC());

    // declaring branches
    Event * evnt = nullptr;
    PileUp * pileup = nullptr;
    oldchain->SetBranchAddress("event", &evnt);
    vector<GenJet> * genjets = nullptr;
    vector<RecJet> * recjets = nullptr;
    oldchain->SetBranchAddress("recJets", &recjets);
    oldchain->SetBranchAddress("genJets", &genjets);
    oldchain->SetBranchAddress("pileup", &pileup);

    // control plot
    TString name = input.filename().c_str();
    name.ReplaceAll(".root", "");
    TString prefix = name(TRegexp("^[A-Za-z]*1[6-8][A-Za-z]*_")); // e.g. Pythia16_ or Pythia16Flat_
    name.ReplaceAll(prefix, "");
    cout << name << endl;
    ControlPlots::isMC = true;
    ControlPlots plots("controlplots");

    optional<int> i;
    if (fs::exists(correction))
        i = 42;

    optional<Sauge> sauge = nullopt;
    if (fs::exists(correction))
        sauge = Sauge(correction.c_str());

    Looper looper(__func__, oldchain, nSplit, nNow);
    while (looper.Next()) {

        // cut-off for 5 TeV events
        double scale = evnt->hard_scale;
        if (scale > 5000) continue;

        // sanity check
        assert(evnt->genWgts.size() == 1 && evnt->recWgts.size() == 1); // but the weight could be different than one!
        if (recjets->size() > 0) assert(recjets->front().JECs.size() <= 1); // there could be events w. gen jets but no rec jets...

        // renormalisation
        evnt->genWgts.front() *= factor;
        newtree->Fill();

        // control plot
        plots(*genjets, evnt->genWgts.front()                        );
        plots(*recjets, evnt->genWgts.front() * evnt->recWgts.front());

        // plots used later on for PU staub sauger
        if (sauge) (*sauge)(pileup, evnt, recjets, genjets);
    }

    /* closing */
    newtree->AutoSave();
    
    plots.Write(newfile);

    // saving PU staub plots
    if (sauge) sauge->Write(newfile);

    newfile->Close();
    delete oldchain;
    cout << "Done " << nNow << endl;
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    gROOT->SetBatch();

    if (argc < 5) {
        cout << argv[0] << " input output sumOfWeights xsection [nSplit [nNow]]\n"
             << "\twhere\tinput = n-tuple\n"
             << "\t     \toutput = new n-tuple\n"
             << "\t     \tcorrection = txt file with parameters of function to cut off bad events (put dummy word to deactivate)\n"
             << "\t     \txsection = cross section (check tables)" << endl;
        return EXIT_SUCCESS;
    }

    fs::path input = argv[1],
             output = argv[2],
             correction = argv[3];
    float xsection = atof(argv[4]);

    int nSplit = 1, nNow = 0;
    if (argc > 5) nSplit = atoi(argv[5]);
    if (argc > 6) nNow   = atoi(argv[6]);

    applyMClumi(input, output, correction, xsection, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
