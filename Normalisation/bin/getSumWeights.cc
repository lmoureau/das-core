#include <cstdlib>
#include <thread>
#include <iostream>

#include <TChain.h>
#include <TFile.h>
#include <TROOT.h>
#include <TH1D.h>

#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"

using namespace std;
using namespace DAS;

////////////////////////////////////////////////////////////////////////////////
/// Sum the weights from trivial root file
void getSumWeights
        (TString input,         //!< name of input root file with *n*-tuple
         TString output,        //!< name of (trivial) output root file
         int nSplit = 1,        //!< number of jobs/tasks/cores
         int nNow = 0)          //!< index of job/task/core
{
    TChain * oldchain = new TChain("inclusive_jets"); // arg = path inside the ntuple
    oldchain->Add(input);
      
    Event * evnt = nullptr;
    oldchain->SetBranchAddress("event", &evnt);

    TFile * file = TFile::Open(output, "RECREATE");
    TH1 * hSumWgt = new TH1D("hSumWgt","Sum of wgt", 1, -0.5, 0.5); //dummy thread-safe histogram

    Looper looper(__func__, oldchain, nSplit, nNow);
    while (looper.Next()) {
        double w = evnt->genWgts.front();
        hSumWgt->Fill(0.0, w);
    }
    hSumWgt->Print();
    hSumWgt->Write();
    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 3) {
        cout << "getSumWeights input output [nSplit [nNow]]\n"
             << "\twhere\tinput = n-tuple\n"
             << "\t     \toutput = root file" << endl;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2];

    int nSplit = 1, nNow = 0;
    if (argc > 3) nSplit = atoi(argv[3]);
    if (argc > 4) nNow = atoi(argv[4]);

    getSumWeights(input, output, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
