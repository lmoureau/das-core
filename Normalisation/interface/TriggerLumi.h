#ifndef __TRIGGER_LUMI__
#define __TRIGGER_LUMI__

#include <vector>
#include <map>
#include <filesystem>

#include "Core/Objects/interface/Jet.h"

#include <TString.h>
#include <TH1.h>

namespace DAS {

////////////////////////////////////////////////////////////////////////////////
/// Simply structure to gather the trigger, turnon and 1/efflumi
struct TriggerLumi {
    int turnon; //!< determined with `getTriggerCurves` and `getTriggerTurnons`
    float weight; //!< obtained from JSON files after CRAB
    TH1 * h; //!< just for some control plots (to show the contributions from the different triggers to the total spectrum)
    TriggerLumi (int trigger);
    //TriggerLumi ();
    void Fill (const std::vector<RecJet>& jets);
};

std::map<int, TriggerLumi> GetLumiFromFiles
            (const std::filesystem::path& lumi_file,   //!< path to text file with effective luminosities
             const std::filesystem::path& turnon_file); //!< path to text file with turn-on points

}

#endif
