# BrilCalc

Important note: we have only managed to run at CERN so far (although in principle, it should be feasible to run it with tunnelling).

To set it up, follow [official instructions](https://cms-service-lumi.web.cern.ch/cms-service-lumi/brilwsdoc.html).

Once this is done, after compilation of the DAS framework, the script can be used directly from the shell prompt as a command, and is expected to be executed in `$DAS_WORKAREA/tablesluminosity/201?`, in which you should have copied the `processedLumi.json` files obtained after running the CRAB jobs for each era separately.
