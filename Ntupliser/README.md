# N-tupliser

This package contains the generic configuration and commands to generate *n*-tuples.
The *n*-tuple format is described in [Objects](../Objects/README.md).
To run the *n*-tupliser, the input data sets must be stored on Tier2 servers (`T2_*`) in [`MINIAOD` format](https://twiki.cern.ch/twiki/bin/viewauth/CMS/MiniAOD).

We first provide technical details on the data sets, then on the concept of campaign, finally on the running of the *n*-tupliser itself.

## Data sets

### Certificate

General: as soon as you want to deal with CMS data sets, you need a valid grid certificate.

You should check with your computing admin in your institute how to obtain a valid grid certificate.
Here, we only remind the essential steps to activate your grid:

At DESY, on certain machines, you may have to activate the grid commands by running the following in your shell:
```bash
source /cvmfs/grid.desy.de/etc/profile.d/grid-ui-env.sh
```
At CERN, this should not be necessary.
In other institutes, check with your admin.

Then you can activate your grid certificate:
```bash
voms-proxy-init -voms cms
```
By default, your certificate will be valid for 24h; to run longer, use `-rfc -valid 192:00`.
To check if any certificate has already been activated:
```bash
voms-proxy-info
```

*Note*: activating your grid is only necessary to run commands that deal with CMS data sets: once the *n*-tuples are being or have been produced, you don't need it anymore.
In other words: there is no point sourcing it each time you source the environment for daily analysis.

### Browsing

To investigate data set, one standard command is `dasgoclient`, reachable in any CMSSW environment.
The most basic type of call goes as follows:
```bash
dasgoclient -query "/JetHT/*/MINIAOD"
```
to see all existing `JetHT` data sets in `MINIAOD` (careful: the list may be quite long).
More advanced commands may be run for example to investigate to location of data sets; for instance:
```bash
$ dasgoclient -query "file dataset=/JetHT/Run2016G-07Aug17-v1/AOD"

Showing 1-10 out of 36115 results, for more results use --idx/--limit options

/store/data/Run2016G/JetHT/AOD/07Aug17-v1/50000/80D2B128-EE8C-E711-BBB7-001E673972AB.root
/store/data/Run2016G/JetHT/AOD/07Aug17-v1/50000/80CAE826-EE8C-E711-9733-001E677925A0.root
/store/data/Run2016G/JetHT/AOD/07Aug17-v1/50000/80A9E9AE-A88D-E711-8D2E-001E67F67372.root
/store/data/Run2016G/JetHT/AOD/07Aug17-v1/50000/807C7D29-818D-E711-9472-001E67792514.root
/store/data/Run2016G/JetHT/AOD/07Aug17-v1/50000/8031D892-268D-E711-90D9-002590200934.root
/store/data/Run2016G/JetHT/AOD/07Aug17-v1/50000/802D756F-298D-E711-B242-002590200984.root
/store/data/Run2016G/JetHT/AOD/07Aug17-v1/50000/7EECEA90-348D-E711-B111-002590200934.root
/store/data/Run2016G/JetHT/AOD/07Aug17-v1/50000/7EC9C4E9-C98D-E711-B761-002590200B34.root
/store/data/Run2016G/JetHT/AOD/07Aug17-v1/50000/7E5EC79B-998D-E711-A8D3-001E67397D05.root
/store/data/Run2016G/JetHT/AOD/07Aug17-v1/50000/7E58EC0D-598D-E711-9CA4-002590200868.root
```
or
```bash
$ dasgoclient -query "file dataset=/QCD_Pt_15to30_TuneCUETP8M1_13TeV_pythia8/RunIISummer16DR80Premix-PUMoriond17_80X_mcRun2_asymptotic_2016_TrancheIV_v6-v1/AODSIM"

Showing 1-10 out of 3436 results, for more results use --idx/--limit options

/store/mc/RunIISummer16DR80Premix/QCD_Pt_15to30_TuneCUETP8M1_13TeV_pythia8/AODSIM/PUMoriond17_80X_mcRun2_asymptotic_2016_TrancheIV_v6-v1/70000/FED18A1B-21AD-E611-8736-0CC47A7C3572.root
/store/mc/RunIISummer16DR80Premix/QCD_Pt_15to30_TuneCUETP8M1_13TeV_pythia8/AODSIM/PUMoriond17_80X_mcRun2_asymptotic_2016_TrancheIV_v6-v1/70000/FEC35242-63AD-E611-B8B9-0025905B8612.root
/store/mc/RunIISummer16DR80Premix/QCD_Pt_15to30_TuneCUETP8M1_13TeV_pythia8/AODSIM/PUMoriond17_80X_mcRun2_asymptotic_2016_TrancheIV_v6-v1/70000/FE951C01-94AE-E611-9266-0CC47A4C8E2A.root
/store/mc/RunIISummer16DR80Premix/QCD_Pt_15to30_TuneCUETP8M1_13TeV_pythia8/AODSIM/PUMoriond17_80X_mcRun2_asymptotic_2016_TrancheIV_v6-v1/70000/FE715E49-9CAE-E611-BFBF-0025905A60D2.root
/store/mc/RunIISummer16DR80Premix/QCD_Pt_15to30_TuneCUETP8M1_13TeV_pythia8/AODSIM/PUMoriond17_80X_mcRun2_asymptotic_2016_TrancheIV_v6-v1/70000/FE6C3D21-A0AD-E611-AD0A-0025905B8574.root
/store/mc/RunIISummer16DR80Premix/QCD_Pt_15to30_TuneCUETP8M1_13TeV_pythia8/AODSIM/PUMoriond17_80X_mcRun2_asymptotic_2016_TrancheIV_v6-v1/70000/FE40B6C4-0FAD-E611-ACCC-0025905A605E.root
/store/mc/RunIISummer16DR80Premix/QCD_Pt_15to30_TuneCUETP8M1_13TeV_pythia8/AODSIM/PUMoriond17_80X_mcRun2_asymptotic_2016_TrancheIV_v6-v1/70000/FE26834B-A2AE-E611-B24F-0CC47A7C351E.root
/store/mc/RunIISummer16DR80Premix/QCD_Pt_15to30_TuneCUETP8M1_13TeV_pythia8/AODSIM/PUMoriond17_80X_mcRun2_asymptotic_2016_TrancheIV_v6-v1/70000/FE0FAD2B-A0AE-E611-AA91-0025905A6090.root
/store/mc/RunIISummer16DR80Premix/QCD_Pt_15to30_TuneCUETP8M1_13TeV_pythia8/AODSIM/PUMoriond17_80X_mcRun2_asymptotic_2016_TrancheIV_v6-v1/70000/FCAAF7EF-6CAD-E611-914A-0025905A608E.root
/store/mc/RunIISummer16DR80Premix/QCD_Pt_15to30_TuneCUETP8M1_13TeV_pythia8/AODSIM/PUMoriond17_80X_mcRun2_asymptotic_2016_TrancheIV_v6-v1/70000/F8FF7BE8-9DAE-E611-B438-0025905A60B8.root
```
Run `dasgoclient -h` to get some help, and `dasgoclient -examples` to see an extensive list of examples with this command.

An alternative way to get the location of a file is to use `edmFileUtil`, also reachable within any CMSSW environment:
```bash
$ edmFileUtil -d /store/data/Run2016G/JetHT/AOD/07Aug17-v1/50003/56319AEC-1A7F-E711-A64F-001E677923E6.root
dcap://dcache-cms-dcap.desy.de//pnfs/desy.de/cms/tier2/store/data/Run2016G/JetHT/AOD/07Aug17-v1/50003/56319AEC-1A7F-E711-A64F-001E677923E6.root
```
or
```bash
$ edmFileUtil -d /store/mc/RunIISummer16DR80Premix/QCD_Pt_15to30_TuneCUETP8M1_13TeV_pythia8/AODSIM/PUMoriond17_80X_mcRun2_asymptotic_2016_TrancheIV_v6-v1/70000/FED18A1B-21AD-E611-8736-0CC47A7C3572.root
dcap://dcache-cms-dcap.desy.de//pnfs/desy.de/cms/tier2/store/mc/RunIISummer16DR80Premix/QCD_Pt_15to30_TuneCUETP8M1_13TeV_pythia8/AODSIM/PUMoriond17_80X_mcRun2_asymptotic_2016_TrancheIV_v6-v1/70000/FED18A1B-21AD-E611-8736-0CC47A7C3572.root
```

To investigate the content of a file, use `edmDumpEventContent`:
```bash
$ edmDumpEventContent root://xrootd-cms.infn.it//store/mc/RunIISummer16DR80Premix/QCD_Pt_15to30_TuneCUETP8M1_13TeV_pythia8/AODSIM/PUMoriond17_80X_mcRun2_asymptotic_2016_TrancheIV_v6-v1/70000/FED18A1B-21AD-E611-8736-0CC47A7C3572.root
```
(output may be quite long... just use grep to focus on what you're looking for).

### Rucio request

If the data sets are not available on a Tier2 server, but exist on a Tier1 server or on tape, you may need to make a [Rucio request](https://twiki.cern.ch/twiki/bin/viewauth/CMS/Rucio)

A few commands have to be run on `lxplus`.
First, you need to set up Rucio:
```bash
source /cvmfs/cms.cern.ch/cmsset_default.sh
source /cvmfs/cms.cern.ch/rucio/setup-py3.sh
export RUCIO_ACCOUNT=$USER
```
Then you can "submit rules", e.g.:
```bash
rucio add-rule --ask-approval --lifetime 2592000 cms:/SingleMuon/Run2016G-TkAlMuonIsolated-21Feb2020_UL2016-v1/ALCARECO#5ca78b2c-101d-4e20-8bcd-8b509e7eaf28 1 T2_CH_CERN
```
where
 - you should adapt the dataset (`/*/*/*`),
 - the hash for the lumi block is not compulsory (after the `#`),
 - and you should adapt the destination Tier2 server.
Each call of a this command will return an ID: save it somewhere.
You can add several rules, each one will have its own ID.
To check the status of your request:
```bash
rucio list-rules --account $USER
```

Then write an e-mail to the [Computing Operation Team](mailto:cms-comp-ops-transfer-team@cern.ch), similar to:
> Dear CompOps Team,
>
> please find the following list of rucio requests awaiting for an approval which are needed at `T2_XXX`.
> Would you be so kind as to process them as soon as possible?
>
> YYY1
> YYY2
> ...
>
> Thank you in advance,
> ZZZ
where you should replace
 - `T2_XXX` with the server where you would like to have the data sets,
 - `YYY*` with the request ID(s),
 - and `ZZZ` with your name.
You may have to wait for few days to see the data sets appearing on the Tier2 server.

## Running the *n*-tupliser

The use of campaign is not mandatory to execute any of the commands to produce the *n*-tuple, but is recommended to ensure reproducibility and documentation of an analysis.

In general, to avoid reinventing the wheel, we resort to CRAB commands whenever possible.
Only the submission is partly re-written.

At DESY, the *n*-tuples will be stored on `pnfs`.
This LFS disk may only be used to storage the *n*-tuples produced with CRAB.
See [here](https://indico.desy.de/indico/event/2295/contribution/9/material/slides/0.pdf) for more information.

### `mkNtuples`

The script may be found in `scripts`, but after `scram b` should be available directly as a command in the prompt.
It should be run from the workarea or any large file storage (LFS) area.
It takes a JSON config file as input; an example of config file may be found in `test`.

### CRAB3

Here, we only present the essential commands.
Many TWikis provide additional information:
 - [Guide](https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideCrab)
 - [Troubleshooting](https://twiki.cern.ch/twiki/bin/view/CMSPublic/CRAB3Troubleshoot)

To initialise CRAB, you may have to use the following (for instance at DESY):
```bash
source /cvmfs/cms.cern.ch/crab3/crab.sh
```
This is done in the default initialisation script of the framework, in case you use it.
All CRAB commands start with `crab`; to get a list of the available commands, enter `crab help`.

#### Submit jobs

To submit, in principle, one may use `crab submit config.py` where a [CRAB configuration in Python format](https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookCRAB3Tutorial#CMSSW_configuration_file_example) must be provided; in practice, here, we rather rely on a [custom command](scripts/createNtuples), reachable anywhere in the shell after sourcing CMSSW.
It makes use of the [CRAB API](https://twiki.cern.ch/twiki/bin/view/CMSPublic/CRABClientLibraryAPI).

One submission per data set (`/*/*/*`) is required (this is transparent when using the custom command).
A new directory with local details for each submission: think twice before deleting it.

#### Babysitting

Check the status:
```bash
crab status [path/to/dir]
```
This will show you if the submission has succeeded and if it is still running.
You may have to resubmit failed jobs:
```bash
crab resubmit [path/to/dir]
```
If the jobs keep failing:
 - extend the running time (see commands options);
 - check the logs (in the job directory);
 - or contact the Computing group.

For real data, it is absolutely essential to reach 100% of the data set.
Instead, for MC, it is acceptable if you only reach 95% or 99% (the events are distributed uniformly, and as long as the whole phase space is covered by real data is also covered by MC with decent statistics).

#### After the run

This output of the CRAB job may be needed for instance to calculate the luminosity:
```bash
crab report [path/to/dir]
```
