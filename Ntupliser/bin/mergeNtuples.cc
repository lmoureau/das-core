#include <cstdlib>
#include <cassert>
#include <iostream>
#include <algorithm>
#include <cstring>

#include "Core/CommonTools/interface/terminal.h"
#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"
#include "Core/Objects/interface/Event.h"

#include <TROOT.h>
#include <TFile.h>
#include <TChain.h>
#include <TH1.h>

#include "Core/Ntupliser/interface/mergeNtuples.h"
#include "Core/PUprofile/interface/applyPUlatest.h"

namespace fs = std::filesystem;
////////////////////////////////////////////////////////////////////////////////
/// Get fractioned *n*-tuples from `pnfs` and merge them into a single file.
///
/// The code makes use of the (experimental) filesystem library of C++17.
///
/// *Note*: It is necessary to provide the meta information.
void mergeNtuples
            (TString input,      //!< path to pnfs directory containing files to merge
             const fs::path& output, //!< merged n-tuple
             bool isMC,      //!< MC or DATA
             int year,       //!< 201X
             int version,    //!< n-tuple/code version
             float radius,   //!< parameter of anti-kt jet clustering algorithm (e.g. 0.4 or 0.8)
             const fs::path& pileup_file,  //!< name of JSON file with latest pile-up description
             int nSplit = 1, 
             int nNow = 0)
{
    //Get old file, old tree and set top branch address
    TChain * oldchain = new TChain("ntupliser/inclusive_jets"); // arg = path inside the ntuple
    for (auto f: GetFiles(input))
        oldchain->Add(f.c_str());

    Event * event = nullptr;
    PrimaryVertex * vtx = nullptr;
    PileUp * pu = nullptr;
    vector<RecJet> * recJets = nullptr;
    vector<GenJet> * genJets = nullptr;

    oldchain->SetBranchAddress("event", &event);
    oldchain->SetBranchAddress("primaryvertex", &vtx);
    oldchain->SetBranchAddress("recJets", &recJets);
    if (isMC)
        oldchain->SetBranchAddress("genJets", &genJets);

    optional<PUlatest> pu_functor;
    if (fs::exists(pileup_file)) {
        oldchain->SetBranchAddress("pileup", &pu);
        pu_functor = make_optional<PUlatest>(pileup_file);
    }
    else if (!isMC)
        cerr << red << "Warning: no pileup_latest.txt file provided.\n" << normal;

    if (fs::exists(output))
        cerr << red << "Overwriting existing output file\n" << normal;
    auto newfile = TFile::Open(output.c_str(), "RECREATE");
    auto newtree = oldchain->CloneTree(0);

    // dummy thread-safe histogram, used especially in MC for normalisation
    TH1 * hSumWgt = nullptr;
    if (isMC) hSumWgt = new TH1D("hSumWgt","Sum of wgt", 1, -0.5, 0.5);

    MetaInfo metainfo(newtree, isMC, year, version, radius);
    if (nNow == 0) metainfo.Print();
    metainfo.AddCorrection("merged");
    if(!isMC)
        metainfo.AddCorrection("PUlatest");

    Looper looper(__func__, oldchain, nSplit, nNow);
    while (looper.Next()) {

        if (abs(vtx->z) > 24 /* cm */ || abs(vtx->Rho) > 2 /* cm */ ||  vtx->fake) // good PV
            recJets->clear();

        RemoveLowPt<RecJet>(recJets, minpt);
        if (isMC)
            RemoveLowPt<GenJet>(genJets, minpt);

        if (pu_functor)
                (*pu_functor)(event, recJets, pu);
        
        newtree->Fill();

        if (!isMC) continue;
        auto w = event->genWgts.front();
        hSumWgt->Fill(0.0, w);
    }

    cout << "saving" << endl;

    newtree->AutoSave();
    if (isMC)
        hSumWgt->Write();
    newfile->Close();
    delete oldchain;
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    gROOT->SetBatch();

    if (argc < 6) {
        cout << argv[0] << " input output isMC year version radius [nSplit [nNow]]\n"
             << "\twhere\tinput = path to pnfs directory containing files to merge\n" 
             << "\t     \toutput = merged n-tuple\n"
             << "\t     \tisMC = MC or DATA\n"
             << "\t     \tyear = 201X\n"
             << "\t     \tversion = n-tuple/code version\n"
             << "\t     \tradius = parameter of anti-kt jet clustering algorithm (e.g. 0.4 or 0.8)\n" 
             << "\t     \tpu_file = name of JSON file with latest pile-up description, use none if you don't want to include the PU information\n"
             << "Note: this is NOT the extended version, applying ONLY PU latest." << endl;
        return EXIT_SUCCESS;
    }

    TString input = argv[1];
    fs::path output = argv[2];

    bool isMC = false; for (TString flag: {"MC", "yes", "true", "1"}) isMC = isMC || TString(argv[3]).Contains(flag);

    int year = atoi(argv[4]),
        version = atoi(argv[5]);
    float radius = atof(argv[6]);

    fs::path pileup_file = argv[7];
    int nSplit = 1, nNow = 0;
    if (argc > 8) nSplit = atoi(argv[8]);
    if (argc > 9) nNow   = atoi(argv[9]);

    mergeNtuples(input, output, isMC, year, version, radius, pileup_file, nSplit, nNow);

    return EXIT_SUCCESS;
}
#endif /* DOXYGEN_SHOULD_SKIP_THIS */
