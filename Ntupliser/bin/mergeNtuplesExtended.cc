#include <cstdlib>
#include <cassert>
#include <iostream>
#include <algorithm>
#include <cstring>

#define BOOST_BIND_GLOBAL_PLACEHOLDERS // TODO: this shuts up Boost (a better solution would be to solve the origin of the warning)
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/info_parser.hpp>

#include "Core/CommonTools/interface/terminal.h"
#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"
#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/Objects/interface/Event.h"

#include <TROOT.h>
#include <TFile.h>
#include <TChain.h>
#include <TH1.h>
#include <TString.h>
#include <TH2.h>

#include "Math/VectorUtil.h"

#include "Core/Ntupliser/interface/mergeNtuples.h"
#include "Core/HotKiller/interface/applyConservativeHotKiller.h"
#include "Core/MET/interface/applyMETfilters.h"
#include "Core/PUprofile/interface/applyPUlatest.h"

namespace fs = filesystem;
namespace pt = boost::property_tree;

////////////////////////////////////////////////////////////////////////////////
/// Get fractioned *n*-tuples from `pnfs` and merge them into a single file.
///
/// The code makes use of the filesystem library of C++17.
///
/// *Note*: It is necessary to provide the meta information.
void mergeNtuplesExtended
            (TString input,      //!< path to pnfs directory containing files to merge
             const fs::path& output, //!< merged n-tuple
             bool isMC,      //!< MC or DATA
             int year,       //!< 201X
             int version,    //!< n-tuple/code version
             float radius,   //!< parameter of anti-kt jet clustering algorithm (e.g. 0.4 or 0.8)
             const fs::path& pileup_file,  //!< name of JSON file with latest pile-up description
             int nSplit = 1, 
             int nNow = 0)
{
    //Get old file, old tree and set top branch address
    auto oldchain = new TChain("ntupliser/inclusive_jets"); // arg = path inside the ntuple
    for (auto f: GetFiles(input))
        oldchain->Add(f.c_str());

    Event * event = nullptr;
    MET * met = nullptr;
    PrimaryVertex * vtx = nullptr;
    PileUp * pu = nullptr;
    vector<RecJet> * recJets = nullptr;
    vector<GenJet> * genJets = nullptr;
    vector<FourVector> * hltJets = nullptr;
    
    oldchain->SetBranchAddress("event", &event);
    oldchain->SetBranchAddress("met", &met);
    oldchain->SetBranchAddress("primaryvertex", &vtx);
    if (!isMC)
        oldchain->SetBranchAddress("pileup", &pu);
    oldchain->SetBranchAddress("recJets", &recJets);
    if (isMC)
        oldchain->SetBranchAddress("genJets", &genJets);
    if (!isMC)
        oldchain->SetBranchAddress("hltJets", &hltJets);

    METfilters metfilters(input, year);

    if (!isMC)
        assert(fs::exists(pileup_file));

    optional<PUlatest> pu_functor;
    if (fs::exists(pileup_file))
        pu_functor = make_optional<PUlatest>(pileup_file);
    

    fs::path hotmap = getenv("DAS_WORKAREA");
    hotmap /= "tables/hotzones";
    switch (year) {
        case 2016: hotmap /= "2016/BCDEFGH.root"; break;
        case 2017: hotmap /= "2017/BCDEF.root";   break;
        case 2018: hotmap /= "2018/ABCD.root";   break;
        default: cerr << "No map for year " << year << '\n';
                 exit(EXIT_FAILURE);
    }
    cout << hotmap << endl;
    assert(fs::exists(hotmap));
    auto hotkiller = ConservativeVeto(hotmap.c_str());

    cout << output << endl;
    if (fs::exists(output))
        cerr << red << "Overwriting existing output file\n" << normal;
    auto newfile = TFile::Open(output.c_str(), "RECREATE");
    auto newtree = oldchain->CloneTree(0); // modified tree
    auto hSumWgt = isMC ? new TH1F("hSumWgt","Sum of wgt", 1, -0.5, 0.5) : nullptr; // dummy thread-safe histogram for MC for normalisation
    Looper looper(__func__, oldchain, nSplit, nNow);

    MetaInfo metainfo(newtree, isMC, year, version, radius);
    if (nNow == 0) metainfo.Print();
    metainfo.AddCorrection("merged");
    metainfo.AddCorrection("METfilters");
    metainfo.AddCorrection("HotKillers");
    if(!isMC)
        metainfo.AddCorrection("PUlatest");
    while (looper.Next()) {

        if (abs(vtx->z) > 24 /* cm */ || abs(vtx->Rho) > 2 /* cm */ || vtx->fake) // good PV
            recJets->clear();

        RemoveLowPt<RecJet>(recJets, minpt);
        if (isMC)
            RemoveLowPt<GenJet>(genJets, minpt);

        metfilters(met, event, recJets);
        hotkiller(recJets);
        if (!isMC)
            hotkiller(hltJets);
        if (pu_functor)
                (*pu_functor)(event, recJets, pu);
        newtree->Fill();
        if (!isMC) continue;
        auto w = event->genWgts.front();
        hSumWgt->Fill(0.0, w);
    }

    cout << "saving" << endl;

    if (isMC) {
        hSumWgt->SetDirectory(newfile);
        hSumWgt->Write();
    }
    newtree->Write();

    newfile->Close();
    delete oldchain;
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    gROOT->SetBatch();

    if (argc < 7) {
        cout << argv[0] << " input output isMC year version radius [nSplit [nNow]]\n"
             << "\twhere\tinput = path to pnfs directory containing files to merge (if multiple directories, use commas to separate)\n" 
             << "\t     \toutput = merged n-tuple\n"
             << "\t     \tisMC = MC or DATA\n"
             << "\t     \tyear = 201X\n"
             << "\t     \tversion = n-tuple/code version\n"
             << "\t     \tradius = parameter of anti-kt jet clustering algorithm (e.g. 0.4 or 0.8)\n"
             << "\t     \tpu_file = name of JSON file with latest pile-up description, use none if you don't want to include the PU information\n"
             << "Note: this is the extended version, applying also the hot killers, the MET filters, the PU latest." << endl;
        return EXIT_SUCCESS;
    }
    
    TString input = argv[1];
    fs::path output = argv[2];

    bool isMC = false; for (TString flag: {"MC", "yes", "true", "1"}) isMC = isMC || TString(argv[3]).Contains(flag);

    int year = atoi(argv[4]),
        version = atoi(argv[5]);
    float radius = atof(argv[6]);
    int nSplit = 1, nNow = 0;

    fs::path pileup_file = argv[7];
    if (argc > 8) nSplit = atoi(argv[8]);
    if (argc > 9) nNow   = atoi(argv[9]);

    mergeNtuplesExtended(input, output, isMC, year, version, radius, pileup_file, nSplit, nNow);

    return EXIT_SUCCESS;
}
#endif /* DOXYGEN_SHOULD_SKIP_THIS */
