#include <vector>
#include <filesystem>

#include <TString.h>
#include <TObjString.h>

#include "Core/Objects/interface/Jet.h"

using namespace std;
using namespace DAS;

namespace fs = filesystem;

static const float minpt = 10;

////////////////////////////////////////////////////////////////////////////////
/// Get the list of the *n*-tuples from `pnfs`.
///
/// In principle, this should only be used in the `mergeNtuples` function.
vector<fs::path> GetFiles
                (const TString& sources) //!< comma-separated directories or files
{
    vector<fs::path> files;

    TObjArray * sourcesArr = sources.Tokenize(',');
    for (int i = 0; i < sourcesArr->GetEntries(); ++i) {
        TObject * o = sourcesArr->At(i);
        TObjString * os = dynamic_cast<TObjString*>(o);
        TString s = os->String();
        fs::path source = s.Data();
        assert(fs::exists(source));
        if (fs::is_directory(source))
        for (auto& file: fs::recursive_directory_iterator(source)) {
            if (!fs::is_regular_file(file.status()) || file.path().extension() != ".root") continue;
            if (file.path().string().find("failed") != string::npos) continue;
            files.push_back(file.path());
        }
        else
            files.push_back(source);
    }
    assert(files.size() > 0);
    return files;
}

////////////////////////////////////////////////////////////////////////////////
/// Template to remove low pt jets (rec or gen) from vector of jets
template<typename Jet> void RemoveLowPt (vector<Jet> * & jets, float minpt)
{
    for (auto jet = jets->begin(); jet != jets->end(); /* nothing */) {
        if (jet->p4.Pt() < minpt) jet = jets->erase(jet);
        else ++jet;
    }
}
