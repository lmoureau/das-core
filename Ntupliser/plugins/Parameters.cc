#include "Parameters.h"

#include <iostream>

using namespace edm;
using namespace reco;
using namespace pat;
using namespace std;

#define gP cfg.getParameter
#define iCc iC.consumes

set<string> DAS::Parameters::getHLTjet_triggerNames (vector<string> triggerNames)
{
    set<string> HLTjet_triggerNames;
    for (string triggerName: triggerNames_)
        if (triggerName.find("PFJet")) 
            HLTjet_triggerNames.insert(triggerName);
    return HLTjet_triggerNames;
}

DAS::Parameters::Parameters(ParameterSet const& cfg,
                       ConsumesCollector && iC) :
    isMC_(gP<bool>("isMC")),
    year(gP<int>("year")),
    flavour(gP<bool>("flavour_flag")),
    muons(gP<bool>("muons_flag"))
{
    // jets
    if (isMC_) {
        genEvtInfoToken = iCc<GenEventInfoProduct>(InputTag("generator")); 
        genjetsToken    = iCc<GenJetCollection>(gP<InputTag>("genjets"));
        if (flavour)
            jetFlavourInfosToken = iCc<JetFlavourInfoMatchingCollection>( gP<InputTag>("jetFlavourInfos"));
    }
    recjetsToken = iCc<JetCollection>(gP<InputTag>("recjets"));
    if (flavour)
        secVertexInfoToken = iCc<vector<reco::VertexCompositePtrCandidate>>(gP<InputTag>("SV_infos"));

    // muons
    if (muons) {
        if (isMC_) 
            genparticleToken = iCc<GenParticleCollection>(gP<InputTag>("genparticles")); 
        recmuonsToken = iCc<pat::MuonCollection>(gP<InputTag>("recmuons")); // need to specify the namespace to avoid conflict
    }

    // pile-up
    rhoToken     = iCc<double>(gP<InputTag>("rho"));
    recVtxsToken = iCc<VertexCollection>(gP<InputTag>("vertices"));
    pileupInfoToken       = iCc<std::vector<PileupSummaryInfo> >(cfg.getUntrackedParameter<edm::InputTag>("pileupInfo"));

    // trigger & MET
    metToken = iCc<METCollection>(gP<InputTag>("met"));
    metNames_             = gP<std::vector<std::string> >("metNames");
    metResultsToken       = iCc<TriggerResults>(gP<InputTag>("metResults"));
    if (!isMC_) {
        triggerNames_         = gP<std::vector<std::string> >("triggerNames");
        triggerResultsToken   = iCc<TriggerResults>(gP<InputTag>("triggerResults"));
        triggerPrescalesToken = iCc<PackedTriggerPrescales>(gP<InputTag>("triggerPrescales"));
        triggerPrescalesl1minToken = iCc<PackedTriggerPrescales>(gP<InputTag>("triggerPrescalesl1min"));
        triggerPrescalesl1maxToken = iCc<PackedTriggerPrescales>(gP<InputTag>("triggerPrescalesl1max"));
        triggerObjectsToken   = iCc<TriggerObjectStandAloneCollection>(gP<InputTag>("triggerObjects"));
        HLTjet_triggerNames = getHLTjet_triggerNames (triggerNames_);
    }

#ifdef PS_WEIGHTS
    // model
    if (isMC_)
        lheToken = iCc<LHEEventProduct,edm::InEvent>(gP<edm::InputTag>("lhe"));
#endif
}
