# Plugins

Contains the code of the *n*-tupliser itself.
It is split into three couples of `.h` and `.cc` files to speed up the compilation and ease the reading:
 - `Ntupliser.*`: initialisation, event loop, etc.
 - `Parameters.*`: interpret the information from the [Python config file](../Ntupliser/Ntupliser.py)
 - `helper.*`: a couple of function, factoured out from the main piece of code
(Note: we may not exactly respect the CMSSW guidelines.)

## Documentation

 - [MINIAOD](https://twiki.cern.ch/twiki/bin/viewauth/CMS/MiniAOD)
 - [DeepFlavour](https://twiki.cern.ch/twiki/bin/view/CMS/DeepFlavour)

