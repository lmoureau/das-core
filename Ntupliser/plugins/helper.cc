#include "helper.h"
#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Lepton.h"

#include <iostream>
#include <limits>

using namespace std;

////////////////////////////////////////////////////////////////////////////////
/// Constructor
///
/// Only giving parameters in reference
DAS::Helper::Helper (DAS::Parameters& parameters) : p(parameters) {}

////////////////////////////////////////////////////////////////////////////////
/// Helper to get `DAS::GenJet` from MiniAOD without flavour
DAS::GenJet DAS::Helper::GetGenJet (const reco::Jet &ijet)
{
    DAS::GenJet jjet;

    // kinematics
    jjet.p4 = DAS::FourVector(ijet.p4());

    return jjet;
}

////////////////////////////////////////////////////////////////////////////////
/// Helper to get `DAS::GenJet` from MiniAOD with flavour
DAS::GenJet DAS::Helper::GetGenJet (const reco::JetFlavourInfoMatching & ijet)
{
    const reco::Jet& Jet = *(ijet.first.get());
    const reco::JetFlavourInfo& Info = ijet.second;

    DAS::GenJet jjet = GetGenJet(Jet);

    // parton flavour
    jjet.partonFlavour = Info.getPartonFlavour();

    // heavy-flavour hadrons
    jjet.nBHadrons = Info.getbHadrons().size();
    jjet.nCHadrons = Info.getcHadrons().size();

    return jjet;
}

////////////////////////////////////////////////////////////////////////////////
/// Helper to get `DAS::RecJet` from MiniAOD
DAS::RecJet DAS::Helper::GetRecJet (const pat::Jet &ijet) 
{
    DAS::RecJet jjet;

    // kinematics
    jjet.p4      = ijet.correctedP4("Uncorrected");

    // JEC
    jjet.area    = ijet.jetArea();

    if (p.flavour) {
        // parton flavour
        jjet.partonFlavour = ijet.partonFlavour();

        // TODO: add flags to (de)activate HF tagging (or jet constituents, etc.)

        // heavy-flavour hadrons
        jjet.nBHadrons = ijet.jetFlavourInfo().getbHadrons().size();
        jjet.nCHadrons = ijet.jetFlavourInfo().getcHadrons().size();

        // heavy-flavour tagging
        // https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideBtagValidation

        //// DeepCSV
        //jjet.DeepCSV.probb    = ijet.bDiscriminator("pfDeepCSVJetTags:probb");
        //jjet.DeepCSV.probbb   = ijet.bDiscriminator("pfDeepCSVJetTags:probbb"); 
        //jjet.DeepCSV.probc    = ijet.bDiscriminator("pfDeepCSVJetTags:probc");
        //jjet.DeepCSV.probcc   = ijet.bDiscriminator("pfDeepCSVJetTags:probcc"); 
        //jjet.DeepCSV.probudsg = ijet.bDiscriminator("pfDeepCSVJetTags:probudsg"); 
        ////DeepCSV "negative" tag
        //jjet.DeepCSVneg.probb    = ijet.bDiscriminator("pfNegativeDeepCSVJetTags:probb");
        //jjet.DeepCSVneg.probbb   = ijet.bDiscriminator("pfNegativeDeepCSVJetTags:probbb"); 
        //jjet.DeepCSVneg.probc    = ijet.bDiscriminator("pfNegativeDeepCSVJetTags:probc");
        //jjet.DeepCSVneg.probcc   = ijet.bDiscriminator("pfNegativeDeepCSVJetTags:probcc"); 
        //jjet.DeepCSVneg.probudsg = ijet.bDiscriminator("pfNegativeDeepCSVJetTags:probudsg"); 
        
        // DeepJet
        // https://twiki.cern.ch/twiki/bin/view/CMS/DeepJet#94X_installation_recipe_X_10
        jjet.DeepJet.probb    = ijet.bDiscriminator("pfDeepFlavourJetTags:probb");
        jjet.DeepJet.probbb   = ijet.bDiscriminator("pfDeepFlavourJetTags:probbb"); 
        jjet.DeepJet.problepb = ijet.bDiscriminator("pfDeepFlavourJetTags:problepb"); 
        jjet.DeepJet.probc    = ijet.bDiscriminator("pfDeepFlavourJetTags:probc");
        //jjet.DeepJet.probcc   = ijet.bDiscriminator("pfDeepFlavourJetTags:probcc"); 
        jjet.DeepJet.probuds  = ijet.bDiscriminator("pfDeepFlavourJetTags:probuds"); 
        jjet.DeepJet.probg    = ijet.bDiscriminator("pfDeepFlavourJetTags:probg"); 

        static const auto eps = 10*numeric_limits<float>::epsilon();
        if (abs(jjet.DeepJet.probb + jjet.DeepJet.probbb + jjet.DeepJet.problepb + jjet.DeepJet.probc + /*jjet.DeepJet.probcc +*/ jjet.DeepJet.probuds + jjet.DeepJet.probg - 1) > eps)
            cerr << abs(jjet.DeepJet.probb + jjet.DeepJet.probbb + jjet.DeepJet.problepb + jjet.DeepJet.probc + /*jjet.DeepJet.probcc +*/ jjet.DeepJet.probuds + jjet.DeepJet.probg - 1) << ' '  << eps << endl;

        //// DeepJet "negative" tag 
        //jjet.DeepJetneg.probb    = ijet.bDiscriminator("pfNegativeDeepFlavourJetTags:probb");
        //jjet.DeepJetneg.probbb   = ijet.bDiscriminator("pfNegativeDeepFlavourJetTags:probbb"); 
        //jjet.DeepJetneg.problepb = ijet.bDiscriminator("pfNegativeDeepFlavourJetTags:problepb"); 
        //jjet.DeepJetneg.probc    = ijet.bDiscriminator("pfNegativeDeepFlavourJetTags:probc");
        ////jjet.DeepJet.probcc   = ijet.bDiscriminator("pfNegativeDeepFlavourJetTags:probcc"); 
        //jjet.DeepJetneg.probuds  = ijet.bDiscriminator("pfNegativeDeepFlavourJetTags:probuds"); 
        //jjet.DeepJetneg.probg    = ijet.bDiscriminator("pfNegativeDeepFlavourJetTags:probg"); 

        //// SV
        ////jjet.SVM = SVs.front().mass(); 
        //// other taggers
        //// https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookMiniAOD2017#Jets (might be outdated)
        ////CSVv2
        //jjet.CSVv2 = ijet.bDiscriminator("pfCombinedInclusiveSecondaryVertexV2BJetTags");
        ////CSVv2 "negative"
        //jjet.CSVv2neg = ijet.bDiscriminator("pfNegativeCombinedInclusiveSecondaryVertexV2BJetTags");
        //
        //jjet.JP = ijet.bDiscriminator("pfJetProbabilityBJetTags");
        //jjet.JPneg = ijet.bDiscriminator("pfNegativeOnlyJetProbabilityBJetTags");


        // https://github.com/cms-sw/cmssw/blob/master/PhysicsTools/PatAlgos/python/recoLayer0/bTagging_cff.py

//        cout << "---" << endl;
//
//        cout << "Radek dummy discriminator "<<ijet.bDiscriminator("Radek") << endl;
//
//        cout << "CSVv2 tagger " << '\t' << "Negative Tag" << endl;
//        cout << ijet.bDiscriminator("pfCombinedInclusiveSecondaryVertexV2BJetTags") << '\t' << ijet.bDiscriminator("pfNegativeCombinedInclusiveSecondaryVertexV2BJetTags") << endl;
//       
//        cout << "JP tagger " <<  '\t' << "Negative Tag" << endl;
//        cout << ijet.bDiscriminator("pfJetProbabilityBJetTags")                     << '\t' << ijet.bDiscriminator("pfNegativeOnlyJetProbabilityBJetTags")                 << endl;
//
//        cout << "DeepCSV tagger " << '\t' << "Negative Tag" << endl;
//        cout << ijet.bDiscriminator("pfDeepCSVJetTags:probb")        << '\t'  << ijet.bDiscriminator("pfNegativeDeepCSVJetTags:probb")        << endl;
//        cout << ijet.bDiscriminator("pfDeepCSVJetTags:probbb")       << '\t'  << ijet.bDiscriminator("pfNegativeDeepCSVJetTags:probbb")       << endl;
//        cout << ijet.bDiscriminator("pfDeepCSVJetTags:probc")        << '\t'  << ijet.bDiscriminator("pfNegativeDeepCSVJetTags:probc")        << endl;
//        cout << ijet.bDiscriminator("pfDeepCSVJetTags:probcc")       << '\t'  << ijet.bDiscriminator("pfNegativeDeepCSVJetTags:probcc")       << endl;
//        cout << ijet.bDiscriminator("pfDeepCSVJetTags:probudsg")     << '\t'  << ijet.bDiscriminator("pfNegativeDeepCSVJetTags:probudsg")     << endl;
//
//
//        cout << "DeepJet tagger " << '\t' << "Negative Tag" << endl;                                                                     
//        cout << ijet.bDiscriminator("pfDeepFlavourJetTags:probb")    << '\t'  << ijet.bDiscriminator("pfNegativeDeepFlavourJetTags:probb")    << endl;
//        cout << ijet.bDiscriminator("pfDeepFlavourJetTags:probbb")   << '\t'  << ijet.bDiscriminator("pfNegativeDeepFlavourJetTags:probbb")   << endl;
//        cout << ijet.bDiscriminator("pfDeepFlavourJetTags:problepb") << '\t'  << ijet.bDiscriminator("pfNegativeDeepFlavourJetTags:problepb") << endl;
//        cout << ijet.bDiscriminator("pfDeepFlavourJetTags:probc")    << '\t'  << ijet.bDiscriminator("pfNegativeDeepFlavourJetTags:probc")    << endl;
//        cout << ijet.bDiscriminator("pfDeepFlavourJetTags:probuds")  << '\t'  << ijet.bDiscriminator("pfNegativeDeepFlavourJetTags:probuds")  << endl;
//        cout << ijet.bDiscriminator("pfDeepFlavourJetTags:probg")    << '\t'  << ijet.bDiscriminator("pfNegativeDeepFlavourJetTags:probg")    << endl;

    }

    jjet.JECs.resize(1, 1.0);

    return jjet;
}

////////////////////////////////////////////////////////////////////////////////
/// Testing loose ID definition for jets (official from JetMET)
///
/// see https://twiki.cern.ch/twiki/bin/viewauth/CMS/JetID
///
/// /!\ these values are intended for 2016 only!!
bool DAS::Helper::LooseID (const pat::Jet &jet)
{
    assert(p.year == 2016); // LooseID is only defined for 2016

    const auto abseta = abs(jet.eta());

    auto NHF = jet.neutralHadronEnergyFraction();
    auto NEMF = jet.photonEnergyFraction();
    auto NumConst = jet.neutralMultiplicity() + jet.chargedMultiplicity();

    auto CHF = jet.chargedHadronEnergyFraction();
    auto CHM = jet.chargedHadronMultiplicity();
    auto CEMF = jet.chargedEmEnergyFraction();

    if (abseta <= 2.7)
        return (NHF<0.99 && NEMF<0.99 && NumConst>1)
            && ((abseta<=2.4 && CHF>0 && CHM>0 && CEMF<0.99) || abseta>2.4);

    auto NumNeutralParticle = jet.neutralMultiplicity();

    if (abseta <= 3.0)
        return NHF<0.98 && NEMF>0.01 && NumNeutralParticle>2;
    
    return NEMF<0.90 && NumNeutralParticle>10;
}

////////////////////////////////////////////////////////////////////////////////
/// Testing tight ID definition for jets (official from JetMET)
///
/// see https://twiki.cern.ch/twiki/bin/viewauth/CMS/JetID
bool DAS::Helper::TightID (const pat::Jet &jet)
{
    const auto abseta = abs(jet.eta());

    auto NHF = jet.neutralHadronEnergyFraction();
    auto NEMF = jet.photonEnergyFraction();
    auto NumConst = jet.neutralMultiplicity() + jet.chargedMultiplicity();

    auto CHF = jet.chargedHadronEnergyFraction();
    auto CHM = jet.chargedHadronMultiplicity();
    auto CEMF = jet.chargedEmEnergyFraction();

    if (abseta <= 2.7)
        switch (p.year) {
            case 2016:
                return (NHF<0.90 && NEMF<0.90 && NumConst>1)
                    && ((abseta<=2.4 && CHF>0 && CHM>0 && CEMF<0.99) || abseta>2.4);
            case 2017:
                return (NHF<0.90 && NEMF<0.90 && NumConst>1)
                    && ((abseta<=2.4 && CHF>0 && CHM>0             ) || abseta>2.4);
            case 2018:
                return (abseta<=2.6 && CHM>0 && CHF>0 && NumConst>1 && NEMF<0.9 && NHF < 0.9 )
                                   || (CHM>0 && NEMF<0.99 && NHF < 0.9);
            default:
                cerr << "Only 2016, 2017 or 2018 is possible\n";
                exit(EXIT_FAILURE);
        }

    auto NumNeutralParticle = jet.neutralMultiplicity();

    if (abseta <= 3.0)
        switch (p.year) {
            case 2016:
                return NHF<0.98 && NEMF>0.01 && NumNeutralParticle>2;
            case 2017:
            case 2018:
                return NEMF>0.02 && NEMF<0.99 && NumNeutralParticle>2;
            default:
                cerr << "Only 2016, 2017 or 2018 is possible\n";
                exit(EXIT_FAILURE);
        }
    
    switch (p.year) {
        case 2016:
            return NEMF<0.90 && NumNeutralParticle>10;
        case 2017:
            return NEMF<0.90 && NHF>0.02 && NumNeutralParticle>10;
        case 2018:
            return NEMF<0.90 && NHF>0.2 && NumNeutralParticle>10;
        default:
            cerr << "Only 2016, 2017 or 2018 is possible\n";
            exit(EXIT_FAILURE);
    }
}

////////////////////////////////////////////////////////////////////////////////
/// Testing tight ID definition for jets (official from JetMET)
///
/// see https://twiki.cern.ch/twiki/bin/viewauth/CMS/JetID
bool DAS::Helper::TightLepVetoID (const pat::Jet &jet)
{
    const auto abseta = abs(jet.eta());

    auto NHF = jet.neutralHadronEnergyFraction();
    auto NEMF = jet.photonEnergyFraction();
    auto NumConst = jet.neutralMultiplicity() + jet.chargedMultiplicity();

    auto CHF = jet.chargedHadronEnergyFraction();
    auto CHM = jet.chargedHadronMultiplicity();
    auto CEMF = jet.chargedEmEnergyFraction();
    auto MUF  = jet.muonEnergyFraction();

    if (abseta <= 2.7)
        switch (p.year) {
            case 2016:
                return (NHF<0.90 && NEMF<0.90 && NumConst>1 && MUF<0.80)
                    && ((abseta<=2.4 && CHF>0 && CHM>0 && CEMF<0.90) || abseta>2.4);
            case 2017:
                return (NHF<0.90 && NEMF<0.90 && NumConst>1 && MUF<0.80)
                    && ((abseta<=2.4 && CHF>0 && CHM>0 && CEMF<0.80) || abseta>2.4);
            case 2018:
                return (abseta<=2.6 && MUF<0.80 && CEMF<0.80 && CHM>0 && NHF<0.9 && NEMF<0.90 && CHF>0 && NumConst>1)
                                   || (MUF<0.80 && CEMF<0.80 && CHM>0 && NHF<0.9 && NEMF<0.99);
            default:
                cerr << "Only 2016, 2017 or 2018 is possible\n";
                exit(EXIT_FAILURE);
        }

    auto NumNeutralParticle = jet.neutralMultiplicity();

    if (abseta <= 3.0)
        switch (p.year) {
            case 2016:
                return NHF<0.98 && NEMF>0.01 && NumNeutralParticle>2;
            case 2017:
            case 2018:
                return NEMF>0.02 && NEMF<0.99 && NumNeutralParticle>2;
            default:
                cerr << "Only 2016, 2017 or 2018 is possible\n";
                exit(EXIT_FAILURE);
        }
    
    switch (p.year) {
        case 2016:
            return NEMF<0.90 && NumNeutralParticle>10;
        case 2017:
            return NEMF<0.90 && NHF>0.02 && NumNeutralParticle>10;
        case 2018:
            return NEMF<0.90 && NHF>0.2 && NumNeutralParticle>10;
        default:
            cerr << "Only 2016, 2017 or 2018 is possible\n";
            exit(EXIT_FAILURE);
    }
}

////////////////////////////////////////////////////////////////////////////////
/// Helper to get `DAS::GenLep` from MiniAOD
///
/// /!\ these values are intended for 2016 only!!
DAS::GenLep DAS::Helper::GetGenMu (const reco::GenParticle &mu)
{
    DAS::GenLep Mu;

    // kinematics
    Mu.p4 = mu.p4();

    return Mu;
}

////////////////////////////////////////////////////////////////////////////////
/// Helper to get `DAS::RecLep` from MiniAOD
DAS::RecLep DAS::Helper::GetRecMu (const pat::Muon &mu) 
{
    DAS::RecLep Mu;

    // kinematics
    Mu.p4 = mu.p4();

    return Mu;
}

