#ifndef __HELPER__
#define __HELPER__

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Lepton.h"
#include "Core/Objects/interface/Event.h"

// rec jet
#include "DataFormats/PatCandidates/interface/Jet.h"

// gen jet
#include "DataFormats/JetMatching/interface/JetFlavourInfo.h"
#include "DataFormats/JetMatching/interface/JetFlavourInfoMatching.h"

// muons
#include "DataFormats/PatCandidates/interface/Muon.h"

#include "Parameters.h"

namespace DAS {

struct Helper {

    DAS::Parameters& p;

    Helper (DAS::Parameters& parameters);

    DAS::GenJet GetGenJet (const reco::JetFlavourInfoMatching &ijet);
    DAS::GenJet GetGenJet (const reco::Jet &ijet);
    DAS::RecJet GetRecJet (const pat::Jet &ijet);

    bool LooseID (const pat::Jet &jet);
    bool TightID (const pat::Jet &jet);
    bool TightLepVetoID (const pat::Jet &jet);

    DAS::GenLep GetGenMu (const reco::GenParticle &mu);
    DAS::RecLep GetRecMu (const pat::Muon &mu);

};

}

#endif
