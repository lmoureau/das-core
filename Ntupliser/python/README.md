# Steering

[TWiki](https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideAboutPythonConfigFile) about Python Config Files.

## *n*-tupliser

The steering files in CMSSW are mostly written in Python.
In particular, the steering file of the *n*-tupliser is `Ntupliser.py`.
It contains options like jet size, triggers, datasets, etc. which may vary from time to time.
Most of them are guessed automatically from the year or from the type of data (simulated vs real); other options are provided with the help of a configuration file in JSON format.

To test the *n*-tupliser directly, you can use the standard CMSSW command:
```bash
cmsRun Ntupliser.py
```
or 
```bash
cmsRun Ntupliser.py inputFiles=root://cms-xrd-global.cern.ch/[path] maxEvents=100
```
where `[path]` corresponds to a path to a data set file (which you can obtain with `dasgoclient` from a generic data set name, as explained [here](../README.md).
In general, CMSSW provides a [command line parser](https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideCommandLineParsing) for `cmsRun`; in this framework, we only use it to provide a config file in JSON format (see `../test/example.json`).

If the data sets are reachable locally, it is technically possible to run the *n*-tupliser locally too, using the local farm, but for better reproducibility and greater generality, it is better to rely on CRAB.
The essential commands to run CRAB are explained [above](../README.md); in practice, a [custom command](../scripts/createNtuples) is available to submit several data sets at a time.
All other actions rely on standard CRAB commands (e.g. resubmit, kill, etc.).

Documentation:
 - [Reclustering with JetToolBox](https://twiki.cern.ch/twiki/bin/viewauth/CMS/JetToolbox)

# Calculate luminosity

First produce the lumi files from the CRAB jobs:
```bash
crab report CRAB/[dir]
```
It will produce the JSON files in `CRAB/[dir]/results`

Then take example from the following script:
```bash
files=CRAB/darwin_*_JetHT_Run2017*

jsonFiles=
for f in $files
do
    json=`echo $f/results/crab*.json`
    #echo $json
    jsonFiles="$jsonFiles $json"
done
allFiles=`echo $jsonFiles | sed 's/\/afs/$USER@naf-cms.desy.de:\/afs/g'`

for f in files/*.json
    do
    echo $f
    brilcalc lumi --normtag /cvmfs/cms-bril.cern.ch/cms-lumi-pog/Normtags/normtag_PHYSICS.json -u /fb -i $f | grep -A2 totrecorded | tail -1 | awk '{print $12}'
done
```
Output for data17 lumis [fb-1]:
```
files/crab_das1_JetHT_Run2017B-31Mar2018-v1.json 4.794
files/crab_das1_JetHT_Run2017C-31Mar2018-v1.json 9.617
files/crab_das1_JetHT_Run2017D-31Mar2018-v1.json 4.248
files/crab_das1_JetHT_Run2017E-31Mar2018-v1.json 9.314
files/crab_das1_JetHT_Run2017F-31Mar2018-v1.json 13.535
```
