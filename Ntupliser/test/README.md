# Test

Different types of tests are in principle possible.
Here we do not directly test any piece of code, but use a [standard hack](https://github.com/cms-sw/cmssw/tree/master/FWCore/Utilities/test) to execute a shell script that only tests the example config file.

Documentation:
 - [Developer's guide](https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideDevelopersGuide)
