#!/bin/zsh
set -e

cd $CMSSW_BASE/src/Core/Ntupliser
python python/ULRun2.py

for configFile in `ls test/*.json`
do
    echo Testing $configFile
    python python/ULRun2.py configFile=$configFile
    mkNtuples $configFile
done
