#ifndef _Leptons_
#define _Leptons_

#include <vector>
#include <iostream>

#include "Core/CommonTools/interface/variables.h"

namespace DAS {

////////////////////////////////////////////////////////////////////////////////
/// class Lepton
///
/// Contains four-momentum 
struct GenLep {

    FourVector p4; //!< Four-momentum

    std::vector<float> weights; //!< muon weight 

    GenLep (); //!< Constructor (trivial)
};

////////////////////////////////////////////////////////////////////////////////
/// class RecLep
struct RecLep : public GenLep {

    FourVector p4;

    //
    RecLep (); //!< Constructor (trivial)
};

}

#if defined(__ROOTCLING__)
#pragma link C++ class DAS::GenLep +;
#pragma link C++ class std::vector<DAS::GenLep> +;

#pragma link C++ class DAS::RecLep +;
#pragma link C++ class std::vector<DAS::RecLep> +;

#endif

#endif

