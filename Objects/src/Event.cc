#include "Core/Objects/interface/Event.h"
#include <cassert>

#define DUMMY -999

using namespace DAS ;

void Event::clear ()
{
    runNo = DUMMY; evtNo = DUMMY; lumi = DUMMY;
    hard_scale = DUMMY;
    genWgts.clear();
    recWgts.clear();
}

void Trigger::clear ()
{
    Bit.clear();
    PreHLT.clear();
    PreL1min.clear();
    PreL1max.clear();
}

void MET::clear ()
{
    Et = DUMMY; SumEt = DUMMY; Pt = DUMMY; Phi = DUMMY;
    Bit.clear();
}

// these are the recommended values for Run-2 at 13 TeV
float PileUp::MBxsec = 69200 /* mb */,
      PileUp::MBxsecRelUnc = 0.046; // 4.6%

float PileUp::GetTrPU (const char v) const
{
    assert(trpu != DUMMY);
    switch (v) {
        case '+': return trpu * (1+MBxsecRelUnc);
        case '-': return trpu * (1-MBxsecRelUnc);
        case '0':
        default:  return trpu;
    }
}

TRandom3 PileUp::r3(42);

float PileUp::GetInTimePU (const char v, bool useDefault) const 
{
    if (useDefault) {
        assert(intpu != DUMMY);
        switch (v) {
            case '+': return intpu * (1+MBxsecRelUnc);
            case '-': return intpu * (1-MBxsecRelUnc);
            case '0':
            default:  return intpu;
        }
    }
    else {
        assert(trpu != DUMMY);
        float trpu = GetTrPU(v);
        return r3.Poisson(trpu);
    }
}

void PileUp::clear ()
{
    nVtx = DUMMY; rho = DUMMY;
    trpu = DUMMY; intpu = DUMMY;
    pthatMax = DUMMY;
}

void PrimaryVertex::clear ()
{
    Rho = DUMMY; z = DUMMY;
    ndof = DUMMY; chi2 = DUMMY;
}

void SecondaryVertex::clear () 
{
    position = TriVector();
    chi2 = DUMMY;
    ndof = DUMMY;
}

