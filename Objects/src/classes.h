#ifndef _DAS_CLASSES_
#define _DAS_CLASSES_

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Lepton.h"
#include "Core/Objects/interface/Event.h"

DAS::GenJet genjet;
DAS::RecJet recjet;

DAS::GenLep genlep;
DAS::RecLep reclep;

DAS::Event event;
DAS::Trigger dastrigger;
DAS::PileUp pileup;
DAS::MET met;
DAS::PrimaryVertex primaryvertex;
DAS::SecondaryVertex secondaryvertex;

#endif
