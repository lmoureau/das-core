# Pile-Up profile reweighting

## Produce pile-up profile from data

*Source*:
[TWiki](https://twiki.cern.ch/twiki/bin/view/CMS/PileupJSONFileforData)

### Example
```
pileupCalc.py -i MyAnalysisJSON.txt --inputLumiJSON pileup_latest.txt \
    --calcMode true --minBiasXsec 69200 --maxPileupBin 100 \
    --numPileupBins 100 MyDataPileupHistogram.root
```
where
- `MyAnalysisJSON.txt` is returned by the CRAB job
- `pileup_latest` is the input JSON file ([2016](/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions16/13TeV/PileUp/pileup_latest.txt), [2017](/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions17/13TeV/PileUp/pileup_latest.txt), [2018](/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions18/13TeV/PileUp/pileup_latest.txt))
- 69200 corresponds to the latest estimation of the minimum-bias cross section
  in nanobarns.
- 100 corresponds to the number of bins for the output histogram:
  `TH1D("pileup","pileup",0,100 /*numPileupBins*/, 100 /*maxPileupBin*/)`
- `MyDataPileupHistogram.root` is the name of the output file, in which the
  profile will be stored.

### Procedure

Get the pile-up profile in MC with the *in-time pile-up* (`PileUp::intpu`), and
reweight the MC events so as to match the pile-up profile in data.

### Systematic uncertainties

Vary the minimum-bias cross section with 4.6% (value from TWiki).

## Reweighting of simulation

### `getMCPUprofile`

Extract the PU profile from MC.

### `getPUprofCorrection`

Test the effect of the PU reweighting on MC. Output is purely informative but
not explicitly used.

### `applyPUprofCorrection`

Apply correction on *n*-tuple, exactaly as shown by previous command (same
header file is used).

