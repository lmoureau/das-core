#include <cstdlib>
#include <cassert>
#include <iostream>
#include <limits>

#include <TFile.h>
#include <TH1.h>

using namespace std;

////////////////////////////////////////////////////////////////////////////////
/// Functor to apply the PU profile correction
struct PUcorrection {
    TH1 * nominal, * upper, * lower;

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor
    PUcorrection (TFile * fMC, TFile * fData, auto hname, float maxWeight = std::numeric_limits<float>::max()) :
        nominal(dynamic_cast<TH1*>(fData->Get(hname("nominal", false)))),
        upper  (dynamic_cast<TH1*>(fData->Get(hname("PUup"   , false)))),
        lower  (dynamic_cast<TH1*>(fData->Get(hname("PUdown" , false))))
    {
        for (TH1 * h: {nominal, upper, lower}) {
            assert(h != nullptr);
            h->SetDirectory(0);
            auto integral = h->Integral();
            assert(integral > 0);
            h->Scale(1./integral);
        }

        auto n = hname("nominal", true);
        std::cout << "Fetching " << n << std::endl;
        auto sim = dynamic_cast<TH1*>(fMC->Get(n));
        assert(sim != nullptr);
        auto integral = sim->Integral();
        assert(integral > 0);
        sim->Scale(1./integral);
        for (TH1 * h: {nominal, upper, lower})
            h->Divide(sim);

        for (TH1 *h: {nominal, upper, lower}) {
            for (int i = 0; i <= h->GetNbinsX()+1; ++i) {
                if (h->GetBinContent(i) < maxWeight) continue;
                h->SetBinContent(i, maxWeight);
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Overloading of operator()
    ///
    /// Returns the weight as a function of the value of the in-time pileup,
    /// for a given variation (of the MB cross section).
    float operator() (float pu, char v = '0') const
    {
        TH1 * h = nullptr;
        switch (v) {
            case '0': h = nominal; break;
            case '+': h = upper  ; break;
            case '-': h = lower  ; break;
            default: exit(EXIT_FAILURE);
        }
        assert(h != nullptr);
        int ibin = h->FindBin(pu);
        return h->GetBinContent(ibin);
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Write output to given directory.
    void Write (TDirectory * d, bool reset)
    {
        d->cd();
        for (TH1 * h: {nominal, upper, lower}) {
            h->SetDirectory(d);
            if (reset) h->Reset();
        }
        nominal->Write("nominal");
        upper->Write("upper");
        lower->Write("lower");
    }
};

