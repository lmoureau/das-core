#include <TH1.h>
#include <TH2.h>
#include <TDirectory.h>
#include <TString.h>

#include <cassert>

#include "Core/CommonTools/interface/variables.h"
#include "Core/Objects/interface/Event.h"

using namespace std;
using namespace DAS;

////////////////////////////////////////////////////////////////////////////////
/// Pile-Up plots
///
/// See [TWiki](https://twiki.cern.ch/twiki/bin/viewauth/CMS/PileupJSONFileforData)
/// for explanations on in-time and true pile-up definitions, and how it changes
/// the definition of the calculation mode of `pileupCalc.py`.
struct PUprofPlots {

    const TString name;
    TH1F intprofile, //!< in-time pile-up, corresponding to `--calcMode observed` (the number of pileup events for the in-time bunch crossing is selected from a Poisson distribution with a mean equal to the "true" pileup)
         trprofile,  //!< true pile-up, corresponding to `--calcMode true` (average pileup conditions under which the event is generated)
         rho,  //!< soft activity
         nVtx; //!< number of vertices

    PUprofPlots (TString Name) :
        name(Name),
        intprofile(Name + "intPU", ";in-time PU;N^{ev}_{eff}", nPUbins, 0, nPUbins),
        trprofile (Name + "trPU" ,    ";true PU;N^{ev}_{eff}", nPUbins, 0, nPUbins),
        rho       (Name + "rho"  ,       ";#rho;N^{ev}_{eff}", nPUbins, 0, nPUbins),
        nVtx      (Name + "nVtx" ,    ";N_{vtx};N^{ev}_{eff}", nPUbins, 0, nPUbins)
    {
        cout << __FILE__ << ':' << __func__ << ':' << __LINE__ << '\t' << name << endl;
    }

    void operator() 
        (const PileUp& pu,
         const auto w,
         const char v = '0',
         bool OnTheFly = false)
    {
        trprofile .Fill(pu.GetTrPU(v), w);
        //Filling with 100 values of InTime PU to have smoother profile, add a 0.01 weight when I do the ratio between data and MC 
        const float N = 100;
        for (int i = 0; i < N; ++i)
            intprofile.Fill(pu.GetInTimePU(v, OnTheFly), w/N);
        
        rho .Fill(pu.rho  , w);
        nVtx.Fill(pu.nVtx , w);
    }

    void Write (TDirectory * d)
    {
        assert(d);
        d->cd();
        vector<TH1*> hs{&intprofile, &trprofile, &rho, &nVtx};
        for (TH1* h: hs) {
            TString n = h->GetName();
            cout << n << endl;
            n.ReplaceAll(name,"");
            h->SetDirectory(d);
            h->Write(n);
        }
    }
};
