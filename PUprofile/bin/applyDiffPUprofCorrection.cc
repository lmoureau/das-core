#include <iostream>
#include <vector>
#include <cmath>
#include <filesystem>

#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Jet.h"
#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"
#include "Core/CommonTools/interface/terminal.h"

#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TH2.h>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/info_parser.hpp>

#include "Core/CommonTools/interface/ControlPlots.h"

#include "PUcorrection.h"

using namespace std;
using namespace DAS;

namespace pt = boost::property_tree;
namespace fs = filesystem;

////////////////////////////////////////////////////////////////////////////////
/// Apply the PU profile reweighting to the simulation.
void applyDiffPUprofCorrection
        (const fs::path& input,     //!< name of input root file containing the *n*-tuple
         const fs::path& output,    //!< name of output root file containing the histograms
         const fs::path& MCprof,    //!< name of input root file containing MC profile (from `getPUprofile`)
         const fs::path& DataProf,  //!< name of input root files containing data profiles (from `getDiffPUprofile`)
         const fs::path& turnon_file, //!< name of 2-column text file with trigger turn-on points per HLT pt threshold
         float maxWeight = numeric_limits<float>::max(),
         int nSplit = 1,    //!< number of jobs/tasks/cores
         int nNow = 0)      //!< index of job/task/core
{
    cout << input << endl;
    assert(fs::exists(input));
    TFile * source = TFile::Open(input.c_str(), "READ");
    TTree * tree = dynamic_cast<TTree *>(source->Get("inclusive_jets"));

    cout << output << endl;
    if (fs::exists(output))
        cerr << red << "Overwriting existing output file\n" << normal;
    TFile *newfile = TFile::Open(output.c_str(), "RECREATE");
    TTree *newtree = tree->CloneTree(0);

    MetaInfo metainfo(newtree);
    bool isMC = metainfo.isMC(); 
    assert(isMC);
    if (nNow == 0) metainfo.Print();
    metainfo.AddCorrection("DiffPUprofile");
    metainfo.AddRecEvWgt("PUup");
    metainfo.AddRecEvWgt("PUdown");

    Event * event = nullptr;
    PileUp * pu = nullptr;
    tree->SetBranchAddress("event", &event);
    tree->SetBranchAddress("pileup", &pu);

    vector<RecJet> * recJets = nullptr;
    vector<GenJet> * genJets = nullptr;
    tree->SetBranchAddress("recJets", &recJets);
    tree->SetBranchAddress("genJets", &genJets);

    ControlPlots::isMC = isMC;
    ControlPlots raw("raw"), nominal("nominal"), upper("upper"), lower("lower");

    assert(fs::exists(turnon_file));
    pt::ptree turnons;
    pt::read_info(turnon_file.c_str(), turnons);

    assert(fs::exists(DataProf));
    assert(fs::exists(MCprof));
    auto fMCprof = TFile::Open(MCprof.c_str(), "READ"),
         fDataProf = TFile::Open(DataProf.c_str(), "READ");
    map<float, PUcorrection> corrections;
    for (auto& turnon: turnons) {
        auto threshold = atoi(turnon.first.c_str());
        auto trigger = [threshold](const char * variation, bool global) {
            return global ? Form("%s/intPU"            , variation)
                          : Form("%s/HLT_PFJet%d/intPU", variation, threshold);
        };
        auto pt = turnon.second.get_value<float>();
        PUcorrection correction(fMCprof, fDataProf, trigger,  maxWeight);
        corrections.insert( {pt, correction} );
    }
    const bool hasZB = fDataProf->GetDirectory("nominal/ZB") && dynamic_cast<TH1*>(fDataProf->Get("nominal/ZB/intPU"))->Integral() > 0;
    if (hasZB) {
        auto trigger = [](const char * variation, bool global) {
            return global ? Form("%s/intPU"   , variation)
                          : Form("%s/ZB/intPU", variation);
        };
        PUcorrection correction(fMCprof, fDataProf, trigger,  maxWeight);
        corrections.insert( {0, correction} );
    }
    fDataProf->Close();
    fMCprof->Close();

    cout << "Looping" << endl;
    Looper looper(__func__, tree, nSplit, nNow);
    while (looper.Next()) {

        int intpu = pu->intpu;
        auto weight_raw = event->recWgts.front(); // get current nominal weight before correction

        // 1) find leading jet pt in tracker acceptance (if it exists)
        auto pt0 = corrections.begin()->first; // if ZB, then by default ZB, otherwise it will be trig40
        auto InTk = [](const auto& jet) { return jet.AbsRap() < 2.5; };
        auto leadRecJetInTk = find_if(recJets->begin(), recJets->end(), InTk);
        if (leadRecJetInTk != recJets->end())
            pt0 = leadRecJetInTk->CorrPt();
        else if (!hasZB) {
            auto leadGenJetInTk = find_if(genJets->begin(), genJets->end(), InTk);
            if (leadGenJetInTk != genJets->end()) 
                pt0 = leadGenJetInTk->p4.Pt();
        }

        auto it = find_if(corrections.rbegin(), prev(corrections.rend()),
                [&pt0](const auto& th_corr) { return pt0 > th_corr.first; });
        const auto& correction = it->second;
        //cout << pt0 << ' ' << it->first << endl;

        // 1) loop over existing weights and correct them with the nominal value
        auto nominal_correction = correction(intpu, '0');
        for (auto& weight: event->recWgts)
            weight *= nominal_correction;
 
        auto weight_nominal = event->recWgts.front();

        // 2) add two more weights for upper and lower variations
        auto weight_upper = weight_raw*correction(intpu, '+'),
             weight_lower = weight_raw*correction(intpu, '-');
       
        event->recWgts.push_back(weight_upper);
        event->recWgts.push_back(weight_lower);

        // filling tree
        newtree->Fill();

        // gen level CP
        raw    (*genJets, event->genWgts.front());
        nominal(*genJets, event->genWgts.front());
        upper  (*genJets, event->genWgts.front());
        lower  (*genJets, event->genWgts.front());

        // rec level CP
        raw    (*recJets, event->genWgts.front() * weight_raw    );
        nominal(*recJets, event->genWgts.front() * weight_nominal);
        upper  (*recJets, event->genWgts.front() * weight_upper  );
        lower  (*recJets, event->genWgts.front() * weight_lower  );
    }

    /* closing */
    auto d = newfile->mkdir("corrections");
    for (auto& correction: corrections) {
        d->cd();
        int threshold = correction.first;
        auto it = turnons.begin();
        while (it != turnons.end()) {
            if (it->second.get_value<int>() == threshold) break;
            ++it;
        }
        //auto it = find_if(turnons.begin(), turnons.end(), [threshold](auto& turnon)
        //                    { return threshold == turnon.second.get_value<int>(); });
        if (it != turnons.end()) {
            auto dd = d->mkdir(Form("HLT_PFJet%d", atoi(it->first.c_str())));
            bool reset = nNow > 0;
            correction.second.Write(dd, reset);
        }
        else { // assuming ZB
            auto dd = d->mkdir("ZB");
            bool reset = nNow > 0;
            correction.second.Write(dd, reset);
        }
    }

    newtree->AutoSave();

    for (ControlPlots * plots: {&raw, &nominal, &upper, &lower}) 
        plots->Write(newfile);
    newfile->Close();

    delete tree;
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    if (argc < 7) {
        cout << argv[0] << " input output MC Data [nSplit [nNow]]\n"
             << "where\tinput = n-tuple\n"
             << "     \toutput = new n-tuple\n"
             << "     \tMC = root file from `getPUprofile`\n"
             << "     \tData = root file from `getPUprofile`\n"
             << "     \tturnon_file = fs::path to 2-column file with trigger thresholds (probably in a subdirectory of `$DAS_WORKAREA/tables/trigger/20*/`)\n"
            << "     \tmaxWeight = highest allowed value for the weight (put very high value, e.g. 1e42 to make ineffective)\n"
             << flush;
        return EXIT_SUCCESS;
    }

    fs::path input = argv[1],
         output = argv[2],
         MC = argv[3],
         Data = argv[4],
         turnon_file = argv[5];

    float maxWeight = atof(argv[6]);

    int nNow = 0, nSplit = 1;
    if (argc > 7) nSplit = atoi(argv[7]);
    if (argc > 8) nNow = atoi(argv[8]);

    applyDiffPUprofCorrection(input, output, MC, Data, turnon_file, maxWeight,nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
