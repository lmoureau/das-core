#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <filesystem>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/info_parser.hpp>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/terminal.h"
#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"
#include "Core/CommonTools/interface/ControlPlots.h"

#include "Core/PUprofile/interface/applyPUlatest.h"

#include <TROOT.h>
#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>

#include "Math/VectorUtil.h"

using namespace std;
using namespace DAS;

namespace fs = filesystem;
namespace pt = boost::property_tree;

////////////////////////////////////////////////////////////////////////////////
/// Reads `pileup_latest.txt` (provided centrally by CMS) to store the information
/// directly in the n-tuple.
///
/// Note: `pileup_latest.txt` is a JSON file with following structure:
/// ```
/// {
///     "run": [["LS", "inst lumi", "xsec RMS", "av xsec"]],
///     __etc
/// }
/// ```
/// The average number of pile-up can be estimated as follows:
/// ```latex
/// nPUmean = av xsec * MB xsec 
/// ```
void applyPUlatest 
             (const fs::path& input,  //!< name of input root file 
              const fs::path& output, //!< name of output root file
              const fs::path& pileup_latest, //!< name of JSON file with latest pile-up description
              int nSplit = 1, //!< number of jobs/tasks/cores
              int nNow = 0)   //!< index of job/task/core
{
    assert(fs::exists(input));

    auto oldchain = new TChain("inclusive_jets"); // arg = fs::path inside the ntuple
    oldchain->Add(input.c_str());

    if (fs::exists(output))
        cerr << red << "Overwriting existing output file\n" << normal;
    auto file = TFile::Open(output.c_str(), "RECREATE");
    auto newtree = oldchain->CloneTree(0);

    MetaInfo metainfo(newtree);
    if (nNow == 0) metainfo.Print();
    metainfo.AddCorrection("PUlatest");
    bool isMC = metainfo.isMC();
    assert(!isMC);

    Event * event = nullptr;
    PileUp * pu = nullptr;
    oldchain->SetBranchAddress("event", &event);
    oldchain->SetBranchAddress("pileup", &pu);
    vector<RecJet> * recJets = nullptr;
    oldchain->SetBranchAddress("recJets", &recJets);

    assert(fs::exists(pileup_latest));
    pt::ptree json;
    pt::read_json(pileup_latest.c_str(), json);

    PUlatest pu_functor(pileup_latest);

    Looper looper(__func__, oldchain, nSplit, nNow);
    while (looper.Next()) {

        pu_functor(event, recJets, pu);
        newtree->Fill();
    }
    file->Write();
    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 4) {
        cout << argv[0] << " input output [nSplit [nNow]]\n"
             << "where\tinput = data n-tuple\n"
             << "     \toutput = data n-tuple\n"
             << "     \tpileup_latest = JSON file\n"
             << "The latest JSON file is probably sth like this (use at your own risks):\n"
             << "/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions1?/13TeV/PileUp/pileup_latest.txt\n"
             << flush;
        return EXIT_SUCCESS;
    }

    fs::path input = argv[1],
         output = argv[2],
         pileup_latest = argv[3];
    int nNow = 0, nSplit = 1;
    if (argc > 4) nSplit = atoi(argv[4]);
    if (argc > 5) nNow = atoi(argv[5]);

    applyPUlatest(input, output, pileup_latest, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
