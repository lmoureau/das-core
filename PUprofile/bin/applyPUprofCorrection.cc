#include <iostream>
#include <vector>
#include <filesystem>

#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Jet.h"
#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"
#include "Core/CommonTools/interface/terminal.h"

#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TH2.h>

#include "Core/CommonTools/interface/ControlPlots.h"

#include "PUcorrection.h"

using namespace std;
using namespace DAS;

namespace fs = std::filesystem;

////////////////////////////////////////////////////////////////////////////////
/// Apply the PU profile reweighting to the simulation.
void applyPUprofCorrection
        (const fs::path& input,     //!< name of input root file containing the *n*-tuple
         const fs::path& output,    //!< name of output root file containing the histograms
         const fs::path& MCprof,    //!< name of input root file containing MC profile (from `getPUprofile`)
         const fs::path& DataProf,  //!< name of input root files containing data profiles (from `getPUprofile`)
         float maxWeight = numeric_limits<float>::max(),
         int nSplit = 1,    //!< number of jobs/tasks/cores
         int nNow = 0)      //!< index of job/task/core
{
    assert(fs::exists(input));
    TFile * source = TFile::Open(input.c_str(), "READ");
    TTree * tree = dynamic_cast<TTree *>(source->Get("inclusive_jets"));

    if (fs::exists(output))
        cerr << red << "Overwriting existing output file\n" << normal;
    TFile *newfile = TFile::Open(output.c_str(), "RECREATE");
    TTree *newtree = tree->CloneTree(0);

    MetaInfo metainfo(newtree);
    bool isMC = metainfo.isMC(); 
    assert(isMC);
    if (nNow == 0) metainfo.Print();
    metainfo.AddCorrection("PUprofile");
    metainfo.AddRecEvWgt("PUup");
    metainfo.AddRecEvWgt("PUdown");

    Event * event = nullptr;
    PileUp * pu = nullptr;
    tree->SetBranchAddress("event", &event);
    tree->SetBranchAddress("pileup", &pu);

    vector<RecJet> * recJets = nullptr;
    vector<GenJet> * genJets = nullptr;
    tree->SetBranchAddress("recJets", &recJets);
    tree->SetBranchAddress("genJets", &genJets);

    ControlPlots::isMC = isMC;
    ControlPlots raw("raw"), nominal("nominal"), upper("upper"), lower("lower");
    TH2D* recptLogWgt = new TH2D("recptLogWgt", "recptLogWgt;Jet p_{T}^{rec} (GeV);log(w)", nPtBins, pt_edges.data(), 400, -30, 20);
    TH2D* recptPUwoWeights = new TH2D("recptPUwoWeights", "recptPUwoWeights;Jet p_{T}^{rec} (GeV);intPU", nPtBins, pt_edges.data(), 121, -0.5, 120.5);
    TH2D* recptPUwWeights = new TH2D("recptPUwWeights", "recptPUwWeights;Jet p_{T}^{rec} (GeV);intPU", nPtBins, pt_edges.data(), 121, -0.5, 120.5);

    assert(fs::exists(DataProf));
    assert(fs::exists(MCprof));
    auto fMCprof = TFile::Open(MCprof.c_str(), "READ"),
         fDataProf = TFile::Open(DataProf.c_str(), "READ");
    auto global = [](const char * variation, bool) { return Form("%s/intPU", variation); };
    PUcorrection correction(fMCprof, fDataProf, global, maxWeight); 
    fDataProf->Close();
    fMCprof->Close();

    cout << "Looping" << endl;
    Looper looper(__func__, tree, nSplit, nNow);
    while (looper.Next()) {

        int intpu = pu->intpu;
        auto weight_raw = event->recWgts.front(); // get current nominal weight before correction

        // 1) loop over existing weights and correct them with the nominal value
        auto nominal_correction = correction(intpu, '0');
        for (auto& weight: event->recWgts) 
            weight *= nominal_correction;

        for (auto& recjet: *recJets) {
            recptPUwoWeights->Fill(recjet.p4.Pt(), intpu);
            recptPUwWeights->Fill(recjet.p4.Pt(), intpu, nominal_correction);
        }

        for (const auto& recJet: *recJets)
            recptLogWgt->Fill(recJet.CorrPt(), log(nominal_correction)); 
        
        auto weight_nominal = event->recWgts.front();

        // 2) add two more weights for upper and lower variations
        auto weight_upper   = weight_raw*correction(intpu, '+'),
             weight_lower   = weight_raw*correction(intpu, '-');
        
        event->recWgts.push_back(weight_upper);
        event->recWgts.push_back(weight_lower);

        // filling tree
        newtree->Fill();

        // filling at gen level
        raw    (*genJets, event->genWgts.front());
        nominal(*genJets, event->genWgts.front());
        upper  (*genJets, event->genWgts.front());
        lower  (*genJets, event->genWgts.front());

        // filling at rec level
        raw    (*recJets, event->genWgts.front() * weight_raw    );
        nominal(*recJets, event->genWgts.front() * weight_nominal);
        upper  (*recJets, event->genWgts.front() * weight_upper  );
        lower  (*recJets, event->genWgts.front() * weight_lower  );
    
    }
    newtree->AutoSave();

    /* closing */

    TDirectory * dir = newfile->mkdir("corrections");
    bool reset = nNow > 0;
    correction.Write(dir, reset);

    for (ControlPlots * plots: {&raw, &nominal, &upper, &lower}) 
        plots->Write(newfile);
    newfile->cd();
    recptPUwoWeights->Write("recptPUwoWeights");
    recptPUwWeights->Write("recptPUwWeights");
    recptLogWgt->Write("recptLogWgt");
    newfile->Close();

    delete tree;
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    if (argc < 6) {
        cout << argv[0] << " input output MC Data maxWeight nSplit [nNow]]\n"
             << "where\tinput = n-tuple\n"
             << "     \toutput = new n-tuple\n"
             << "     \tMC = root file from `getPUprofile`\n"
             << "     \tData = root file from getPUprofile`\n"
             << "     \tmaxWeight = highest allowed value for the weight (put very high value, e.g. 1e42 to make ineffective)\n"
             << flush;

        return EXIT_SUCCESS;
    }

    fs::path input = argv[1],
         output = argv[2],
         MC = argv[3],
         Data = argv[4];

    float maxWeight = atof(argv[5]);
    cout << "maxWeight: " << maxWeight <<endl;

    int nNow = 0, nSplit = 1;
    if (argc > 6) nSplit = atoi(argv[6]);
    if (argc > 7) nNow = atoi(argv[7]);

    applyPUprofCorrection(input, output, MC, Data, maxWeight, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
