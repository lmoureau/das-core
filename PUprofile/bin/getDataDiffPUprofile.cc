#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <filesystem>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include <TROOT.h>
#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TF1.h>
#include <TRandom.h>

#include "Math/VectorUtil.h"

using namespace std;
using namespace DAS;
namespace fs = filesystem;

static const map<int, vector<char>> eras {
    { 2016, {'B', 'C', 'D', 'E', 'F', 'G', 'H'} },
    { 2017, {'B', 'C', 'D', 'E', 'F'} },
    { 2018, {'A', 'B', 'C', 'D'} }
};

static const map<int, vector<int>> triggers {
    { 2016, { 40, 60, 80, 140, 200, 260, 320, 400, 450 } },
    { 2017, { 40, 60, 80, 140, 200, 260, 320, 400, 450, 500 } },
    { 2018, { 40, 60, 80, 140, 200, 260, 320, 400, 450, 500 } }, // TODO: add 15 and 25?
};

struct PU {
    int t;
    TH1 * av, * nom, * up, * dn;
    vector<TF1 *> fits;
    PU (int trigger) :
        t(trigger),
        av (new TH1D(Form("%daverage", t), Form("%d average", t), nPUbins, 0, nPUbins)),
        nom(new TH1D(Form("%dnominal", t), Form("%d nominal", t), nPUbins, 0, nPUbins)),
        up (new TH1D(Form("%dupper"  , t), Form("%d upper"  , t), nPUbins, 0, nPUbins)),
        dn (new TH1D(Form("%dlower"  , t), Form("%d lower"  , t), nPUbins, 0, nPUbins))
    {}
    void Write (TFile * f)
    {
        f->cd();
        TDirectory * d = f->mkdir(Form("%d",t));
        d->cd();
        for (TH1 * h: {av, nom, up, dn}) {
            h->SetDirectory(d);
            TString name = h->GetName();
            name.ReplaceAll(Form("%d",t), "");
            cout << "histogram: " << name << endl;
            h->Write(name);
        }
        for (TF1 * f: fits) {
            //f->SetDirectory(d);
            TString name = f->GetName();
            name.ReplaceAll(Form("%d",t), "");
            cout << "function:" << name << endl;
            f->Write(name);
        }
    }
};

vector<PU> Generate (const char * input, int year, int n)
{
    const int seed = 42; // using the same seed on purpose
    TRandom rNom(seed), rUp(seed), rDn(seed);

    vector<PU> pus;
    for (int trg: triggers.at(year)) {
        PU pu(trg);
        for (char era: eras.at(year)) {

            cout << era << '\t' << trg << endl;

            auto fname = [&](const char * suffix) {
                const char * n = Form("%s/%c/%d%s.txt", input, era, trg, suffix);
                //cout << n << endl;
                assert(fs::exists(fs::path(n)));
                return n;
            };
            ifstream infile, infileUp, infileDn;;
            infile  .open(fname("\0"));
            infileUp.open(fname("up"));
            infileDn.open(fname("dn"));
            do {
                float lumi, puAvLS;
                infile >> lumi >> puAvLS;

                if (!infile.good()) break;

                float lumiUp, puAvLSup;
                infileUp >> lumiUp >> puAvLSup;
                float lumiDn, puAvLSdn;
                infileDn >> lumiDn >> puAvLSdn;

                assert(abs(lumi - lumiUp) < 1e-8);
                assert(abs(lumi - lumiDn) < 1e-8);

                pu.av->Fill(puAvLS, lumi);

                //cout << lumi << ' ' << puAvLS << '\t';
                double w = lumi/n;
                for (int i = 0; i < n; ++i) {
                    pu.nom->Fill(rNom.Poisson(puAvLS  ), w);
                    pu.up ->Fill(rUp .Poisson(puAvLSup), w);
                    pu.dn ->Fill(rDn .Poisson(puAvLSdn), w);
                }
                //cout << '\n';
            }
            while (!infile.eof());
            //cout << flush
        }
        for (TH1 * h: {pu.nom, pu.up, pu.dn})
            h->Scale(1./h->Integral());
        pus.push_back(pu);
    }
    return pus;
}

TString rn () { return Form("%d", rand()); }

[[ deprecated ]]
void Fit (vector<PU>& pus, int year)
{
    vector<const char *> vars { "nominal", "upper", "lower" };
    for (PU& pu: pus) 
    for (TH1 * h: {pu.nom, pu.up, pu.dn}) {

        TString formula = "gaus(0)";
        TF1 * f = new TF1(rn(), formula, 1, nPUbins);
        h->Fit(f);
        for (int n = 1; n < 5; ++n) { // adding Gaussians one by one
            formula += Form("+gaus(%d)", n*3);
            TF1 * ff = new TF1(rn(), formula, 1, nPUbins);
            for (int m = 0; m < n; ++m) // looping over former fitted Gaussians
            for (int k = 0; k < 3; ++k) { // looping over parameters of one formerly fitted Gaussian
                int index = 3*m+k;
                double parameter = f->GetParameter(index);
                ff->SetParameter(index, parameter);
            }
            // guessing three new parameters before fit
            ff->SetParameter(3*n + 0, f->GetParameter(3*n-3)/4); // should be smaller Gaussian
            ff->SetParameter(3*n + 1, f->GetParameter(3*n-2)+10); // should be shifted to the right w.r.t. to formerly fit Gaussians
            ff->SetParameter(3*n + 2, f->GetParameter(3*n-1)*2); // should be wider

            h->Fit(ff);

            f = ff; // keep last fit for next iteration
        }
        f->SetName(Form("f_%s", h->GetName()));
        pu.fits.push_back(f);
    }
}

////////////////////////////////////////////////////////////////////////////////
/// makes a fit of the PU profile separately for each trigger 
/// (and each variation of the inelastic xsect)
void getDataDiffPUprofile 
             (const char * input, //!< name of input root file 
              TString output,     //!< name of output root file
              int year,           //!< 4-digit year
              int n)              //!< number of events in pile-up distribution
{
    assert(fs::is_directory(fs::path(input)));
    assert(n > 0);

    vector<PU> pus = Generate(input, year, n);

    //Fit(pus, year);

    // TODO: make a sum

    TFile * file = TFile::Open(output, "RECREATE");
    for (PU& pu: pus)
        pu.Write(file);
    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    TString DAS_WORKAREA = getenv("DAS_WORKAREA");
    if (argc < 5) {
        cout << argv[0] << " input output\n"
             << "\twhere\tinput = directory of lumi file with 2-column text files obtained from `getLumiFromJSON` (probably already stored in " << DAS_WORKAREA << "/tables/luminosities/[year]/akX/*/)\n"
             << "\t     \toutput = root file with PU profiles that can be used by `applyDiffPUprofile` (which can be stored in << " << DAS_WORKAREA << "/tables/DiffPUprofile/ak4/Run[year].root)\n"
             << "\t     \tyear = 2016-2018\n"
             << "\t     \tnevents = number of events when generating pile-up curves" << endl;
        return EXIT_SUCCESS;
    }

    const char * input = argv[1];
    TString output = argv[2];
    int year = atoi(argv[3]),
        nevents = atoi(argv[4]);

    getDataDiffPUprofile(input, output, year, nevents);
    return EXIT_SUCCESS;
}
#endif

