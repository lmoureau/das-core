#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <vector>
#include <filesystem>

#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Jet.h"

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/terminal.h"
#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"

#include <TROOT.h>
#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>

#include "Math/VectorUtil.h"

#include "PUcorrection.h"
#include "Plots.h"

using namespace std;
using namespace DAS;

namespace fs = filesystem;

////////////////////////////////////////////////////////////////////////////////
/// Get the MC PU profile (analog to the result of `getDataDiffPUprofile`),
/// which uses CMS central tools.
///
/// *Note*: make sure of using the same binning the data and MC!
[[ deprecated ]]
void getMCDiffPUprofile
        (const fs::path& input,     //!< name of input root file containing the *n*-tuple
         const fs::path& output,    //!< name of output root file containing the histograms
         const fs::path& turnon_file, //!< path to text file with turn-on points
         int nSplit = 1,    //!< number of jobs/tasks/cores
         int nNow = 0)      //!< index of job/task/core
{
    //Get old file, old tree and set top branch address
    assert(fs::exists(input));
    TChain * oldchain = new TChain("inclusive_jets"); // arg = path inside the ntuple
    oldchain->AddFile(input.c_str());
    cout << "File " << input << endl;

    Event * event = nullptr;
    PileUp * pileup = nullptr;
    oldchain->SetBranchAddress("event", &event);
    oldchain->SetBranchAddress("pileup", &pileup);

    vector<RecJet> * recJets = nullptr;
    tree->SetBranchAddress("recJets", &recJets);

    if (fs::exists(output))
        cerr << red << "Overwriting existing output file\n" << normal;
    auto file = TFile::Open(output.c_str(), "RECREATE");

    cout << "Opening turn-on file: " << turnon_file << endl;
    ifstream infile;
    infile.open(turnon_file);
    map<float, PUprofPlots&> plots;
    int trigger; float turnon;
    while (infile.good()) { 
        infile >> trigger >> turnon;
        if (infile.eof()) break;
        PUprofPlots p(trigger);
        plots.insert({turnon, p});
    } 
    infile.close(); 
    cout << flush;

    Looper looper(__func__, oldchain, nSplit, nNow);
    while (looper.Next()) {

        auto leadingInTk = recJets->begin();
        while (leadingInTk != recJets->end()
                && abs(leadingInTk->Rapidity()) >= 2.5)
            ++leadingInTk;

        if (leadingInTk == recJets->end()) continue;

        auto leading_pt = leadingInTk->CorrPt();

        auto rIt = plots.rbegin();
        while (rIt != plots.rend()) {
            if (leading_pt >= rIt->first) break;
            ++rIt;
        }
        if (rIt == plots.rend()) continue;

        auto weight = event->genWgts.front()
                    * event->recWgts.front();
        rIt->second(*pileup, weight);
    }

    for (auto& plot: plots) 
        plot.second.Write(file);

    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    if (argc < 3) {
        cout << argv[0] << " input output turnon_file [nSplit [nNow]]\n"
             << "where\tinput = n-tuple\n"
             << "     \toutput = new n-tuple\n"
             << "     \tturnon_file = 2-column file with 'trigger turn-on' (output of `getTriggerTurnons`, should also be in `$DAS_WORKAREA/tables/triggers`)\n"
             << flush;

        return EXIT_SUCCESS;
    }

    fs::path input = argv[1],
             output = argv[2],
             turnon_file = argv[3];

    int nNow = 0, nSplit = 1;
    if (argc > 4) nSplit = atoi(argv[4]);
    if (argc > 5) nNow = atoi(argv[5]);

    getMCDiffPUprofile(input, output, turnon_file, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
