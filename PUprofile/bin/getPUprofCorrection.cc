#include <iostream>
#include <vector>
#include <filesystem>

#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Jet.h"

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/terminal.h"
#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"

#include <TString.h>
#include <TChain.h>
#include <TFile.h>

#include <TH2.h>

#include "PUcorrection.h"
#include "Plots.h"

using namespace std;
using namespace DAS;
namespace fs = std::filesystem

////////////////////////////////////////////////////////////////////////////////
/// Get the plots showing the effect of the PU profile correction.
///
/// *Note*: it is not necessary to run this step to apply the PU profile reweighting on the *n*-tuple.
/// Actually, it is only useful to perform a quick cross-check without rerunning the whole procedure.
[[ deprecated ]]
void getPUprofCorrection
        (const fs::path& input,     //!< name of input root file containing the *n*-tuple
         const fs::path& output,    //!< name of output root file containing the histograms
         const fs::path& MCprof,    //!< name of input root file containing MC profile (from `getMCPUprofile`)
         const fs::path& DataProf,  //!< name of input root files containing data profiles (from `getDataPUprofile`)
         int nSplit = 1,    //!< number of jobs/tasks/cores
         int nNow = 0)      //!< index of job/task/core
{
    assert(fs::exists(input));
    TFile * source = TFile::Open(input.c_str(), "READ");
    TTree * tree = dynamic_cast<TTree *>(source->Get("inclusive_jets"));

    if (fs::exists(output))
        cerr << red << "Overwriting existing output file\n" << normal;
    TFile * file = TFile::Open(output.c_str(), "RECREATE");

    MetaInfo metainfo(tree);
    if (nNow == 0) metainfo.Print();

    Event * event = nullptr;
    PileUp * pileup = nullptr;
    tree->SetBranchAddress("event", &event);
    tree->SetBranchAddress("pileup", &pileup);

    vector<RecJet> * recJets = nullptr;
    vector<GenJet> * genJets = nullptr;
    tree->SetBranchAddress("recJets", &recJets);
    tree->SetBranchAddress("genJets", &genJets);

    ControlPlots raw("raw", isMC), nominal("nominal", isMC), upper("upper", isMC), lower("lower", isMC);

    PUcorrection correction(MCprof, DataProf); 

    Looper looper(__func__, tree, nSplit, nNow);
    while (looper.Next()) {

        int intpu = pileup->intpu;
        double weight_raw     = event->weights.front(),
               weight_nominal = weight_raw*correction(intpu, '0'),
               weight_upper   = weight_raw*correction(intpu, '+'),
               weight_lower   = weight_raw*correction(intpu, '-');

        auto Fill = [&](PUprofPlots plots, double weight) {
            plots.pileup->Fill(intpu, weight);
            for (GenJet& genjet: *genjets)
                plots.genpt->Fill(genjet.p4.Pt(), weight);
            for (RecJet& recjet: *recjets) {
                plots.recpt->Fill(recjet.p4.Pt(), weight);
                plots.pileup_recpt->Fill(recjet.p4.Pt(), intpu, weight);
            }
        };

        Fill(raw    , weight_raw    );
        Fill(nominal, weight_nominal);
        Fill(upper  , weight_upper  );
        Fill(lower  , weight_lower  );
    }
    for (const PUprofPlots& plots: {raw, nominal, upper, lower})
        plots.Write(file);
    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    if (argc < 5) {
        cout << argv[0] << " input output MC Data [nSplit [nNow]]\n"
             << "\twhere\tinput = n-tuple\n"
             << "\t     \toutput = new n-tuple\n"
             << "\t     \tMC = root file from `getMCPUprofile`\n"
             << "\t     \tData = root file from `getDataPUprofile`" << endl;

        return EXIT_SUCCESS;
    }

    fs::path input = argv[1],
         output = argv[2],
         MC = argv[3],
         Data = argv[4];

    int nNow = 0, nSplit = 1;
    if (argc > 5) nSplit = atoi(argv[5]);
    if (argc > 6) nNow = atoi(argv[6]);

    getPUprofCorrection(input, output, MC, Data, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
