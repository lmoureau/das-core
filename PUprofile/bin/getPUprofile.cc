#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <map>
#include <filesystem>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/info_parser.hpp>

#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Jet.h"

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/terminal.h"
#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"

#include <TROOT.h>
#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TRandom3.h>

#include "Math/VectorUtil.h"

#include "Plots.h"

using namespace std;
using namespace DAS;
namespace fs = std::filesystem;
namespace pt = boost::property_tree;

////////////////////////////////////////////////////////////////////////////////
/// Get the PU profile from the n-tuple (data and MC)
void getPUprofile
        (const fs::path& input,       //!< name of input root file containing the *n*-tuple
         const fs::path& output,      //!< name of output root file containing the histograms
         const fs::path& turnon_file, //!< fs::path to text file with turn-on points
         int nSplit = 1,    //!< number of jobs/tasks/cores
         int nNow = 0)      //!< index of job/task/core
{
    //Get old file, old tree and set top branch address
    assert(fs::exists(input));
    cout << "File " << input << endl;
    auto fileIn = TFile::Open(input.c_str(), "READ");
    auto tree = dynamic_cast<TTree*>(fileIn->Get("inclusive_jets"));
    assert(tree != nullptr);

    assert(fs::exists(turnon_file));
    pt::ptree turnons;
    pt::read_info(turnon_file.c_str(), turnons);

    Event * event = nullptr;
    PileUp * pileup = nullptr;
    tree->SetBranchAddress("event", &event);
    tree->SetBranchAddress("pileup", &pileup);
    
    vector<RecJet> * recJets = nullptr;
    tree->SetBranchAddress("recJets", &recJets);

    if (fs::exists(output))
        cerr << red << "Overwriting existing output file\n" << normal;
    auto file = TFile::Open(output.c_str(), "RECREATE");

    MetaInfo metainfo(tree);
    bool isMC = metainfo.isMC();
    int year = metainfo.year();
    if (nNow == 0) metainfo.Print();
    if (!isMC) assert(metainfo.HasCorrection("PUlatest"));

    const size_t NEvWgts = max(1,metainfo.GetNRecEvWgts()), // in data, nominal and pref uncertainties; in sim, nominal and PU prof unc  (TODO: remove the `max`, it's only necessary because of the change of data format))
                 NVar = isMC ? NEvWgts : NEvWgts + 2; // the variations obtained with PileUp::GetTrPU()
    cout << NEvWgts << ' ' << NVar << endl;

    cout << "Declaring histograms" << endl;

    vector<PUprofPlots> global;
    TH2D* recptLogWgt = new TH2D("recptLogWgt", "recptLogWgt", nPtBins, pt_edges.data(), 400, -30, 20);
    map<int, vector<PUprofPlots>> diffs;
    // TODO: also plot without weight
    for (size_t i = 0; i < NVar; ++i) {
        auto var = i < NEvWgts ? metainfo.GetRecEvWgt(i) :
            TString(i == NEvWgts ? "PUup" : "PUdown");
        auto name = var + "_global";
        global.push_back(PUprofPlots(name));
        for (auto& turnon: turnons) {
            name = var + Form("_HLT_PFJet%d", atoi(turnon.first.c_str()));
            int pt = turnon.second.get_value<int>();
            diffs[pt].push_back(PUprofPlots(name));
        }
        name = var + "_ZB";
        diffs[0].push_back(PUprofPlots(name));
    }

    PileUp::r3.SetSeed(nNow); // TODO: improve seed init to avoid reusing same seed for several slices
    cout << "Looping" << endl;

    Looper looper(__func__, tree, nSplit, nNow);
    while (looper.Next()) {

        // The condition is true only for 2016 and 2017, because in 2018 we don't have prefiring
        if ((year==2016) || (year==2017))
            if ((!isMC) && event->recWgts.size() == 1) {
                continue;
            }

        for (size_t i = 0; i < NVar; ++i) {

            // variation: first from the weights
            auto weight = event->recWgts.at(i < NEvWgts ? i : 0);
            if (isMC) weight *= event->genWgts.front();

            // then, if data, from the true pile-up
            char v = '0';
            bool OnTheFly = false;
            if (!isMC) {
                if (i == NEvWgts  ) v = '+';
                if (i == NEvWgts+1) v = '-';
            }
            if (!isMC)
                OnTheFly = true;
                
            // find leading pt in tracker acceptance
            auto leadingInTk = recJets->begin();
            while (leadingInTk != recJets->end()
                    && leadingInTk->AbsRap() >= 2.5)
                ++leadingInTk;

            auto rit = prev(diffs.rend()); // zero-bias trigger by default
            if (leadingInTk != recJets->end()) { // single jet triggers
                auto leading_pt = leadingInTk->CorrPt();
                
                // find the corresponding trigger
                rit = diffs.rbegin();
                while (rit != diffs.rend()) {
                    if (rit->first < leading_pt) break;
                    ++rit;
                }
                if (rit == diffs.rend()) { // in case no single-jet trigger has been found, then it should return to ZB case
                    cerr << red << "No single-jet trigger has been found with `leading_pt = " << leading_pt << "` in event " << event->evtNo << ", therefore will consider ZeroBias trigger.\n" << normal;
                    rit = prev(diffs.rend());
                }
            }

            // fill the plots
            global.at(i)(*pileup, weight, v, OnTheFly);
            auto& diff = rit->second;
            diff  .at(i)(*pileup, weight, v, OnTheFly);
            if (recJets->size() > 0)
                recptLogWgt->Fill(recJets->front().CorrPt(), log(weight));
        }
    }

    cout << "Writing" << endl;
    for (size_t i = 0; i < NVar; ++i) {
        file->cd();
        auto var = i < NEvWgts ? metainfo.GetRecEvWgt(i) :
                    TString(i == NEvWgts ? "PUup" : "PUdown");
        auto d = file->mkdir(var);
        global.at(i).Write(d);
        for (auto& diff: diffs) {
            TString name(diff.second.at(i).name);
            name.ReplaceAll(var + "_", "");
            auto dd = d->mkdir(name);
            diff.second.at(i).Write(dd);
        }
    }

    file->cd();
    recptLogWgt->Write("recptLogWgt");
    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    if (argc < 3) {
        cout << argv[0] << " input output turnon_file [nSplit [nNow]]\n"
             << "where\tinput = n-tuple (for data, only possible after `applyPUlatest`; for MC, preferrably after JER smearing)\n"
             << "     \toutput = new n-tuple\n"
             << "     \tturnon_file = 2-column file with output of `getTriggerTurnons`\n"
             << flush;

        return EXIT_SUCCESS;
    }

    fs::path input = argv[1],
             output = argv[2],
             turnon_file = argv[3];

    int nNow = 0, nSplit = 1;
    if (argc > 4) nSplit = atoi(argv[4]);
    if (argc > 5) nNow = atoi(argv[5]);

    getPUprofile(input, output, turnon_file, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
