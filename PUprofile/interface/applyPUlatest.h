#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <string>
#include <filesystem>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include <TROOT.h>

#include "Math/VectorUtil.h"

namespace pt = boost::property_tree;                          

////////////////////////////////////////////////////////////////////////////////
/// struct pu_latest
///
struct PUlatest {

    pt::ptree json;

    PUlatest (const std::filesystem::path& pileup_file)
    {
        pt::read_json(pileup_file.c_str(), json);
    }

    void operator() (DAS::Event * event, std::vector<DAS::RecJet> * jets, DAS::PileUp * pu)
    {
        auto runNo = event->runNo,
             lumi = event->lumi;

        auto run = json.get_child_optional(std::to_string(runNo));
        if (!run) assert(run);

        //std::cout << runNo << ' ' << lumi << '\t';
        for (auto& LS: *run) {
            auto it = LS.second.begin();  // ["LS", "inst lumi", "xsec RMS", "av xsec"]
            auto LSno = it->second.get_value<int>(); // LS
            if (LSno != lumi) continue;
            ++it; //auto instLumi = it->second.get_value<float>(); // inst lumi
            ++it; //auto xsecRMS = it->second.get_value<float>(); // xsec RMS
            ++it; auto avXsec = it->second.get_value<float>(); // av xsec
            pu->trpu = avXsec * DAS::PileUp::MBxsec;
            //std::cout << instLumi << ' ' << avXsec << ' ' << ' ' << xsecRMS << '\t' << pu->trpu << endl;
            break;
        }   
    }
    
};
