#!/bin/zsh
set -e

cmd=$(basename $0)

if [ "$#" -ne 3 ]
then
    echo $cmd input HLTPATH year
    echo "where\tinputLumiJSON = (copy of) JSON file obtained from \`crab report\`"
    echo "     \tHLTPATH = HLT_PFJet or HLT_AK8PFJet (also used for the output)"
    echo "     \tyear = 2016 or 2017 or 2018"
    echo "NB: runs only if the output does not exist yet"
    exit
fi

inputLumi=$1
HLTPATH=$2
year=$3

if (( year > 2000 ))
then
    (( year -= 2000 ))
fi

if [ -f $HLTPATH ]
then
    ls $HLTPATH
    echo "Output already exists. Exiting."
    exit
fi

# values recommended by Lumi POG for Run 2
typeset -A xsect
xsect[nominal]=69200
unc="0.046"
xsect[lower]=`echo "${xsect[nominal]}*(1-$unc)" | bc`
xsect[upper]=`echo "${xsect[nominal]}*(1+$unc)" | bc`

if [[ -e pileup_latest.txt ]] # for GitLab CI
then
    pileupJSON=pileup_latest.txt
else # normal running conditions
    path1=/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions
    path2=13TeV/PileUp/pileup_latest.txt

    pileupJSON=${path1}${year}/${path2}
fi 

for var in nominal upper lower
do
    mkdir -p $HLTPATH

    cmd="pileupCalc.py -i $inputLumi --inputLumiJSON $pileupJSON --calcMode true --minBiasXsec ${xsect[$var]} --maxPileupBin 150 --numPileupBins 150 --pileupHistName=$var $HLTPATH/$var.root"
    echo $cmd
    $(echo $cmd) > $HLTPATH/$var.out &
done
wait

hadd $HLTPATH.root $HLTPATH/*root
echo Done
