# PU Staub Sauger

*Only for simulation!*

The QCD samples suffers from a bad simulation of the pile-up, in the sense that
it introduces unrelastic contributions even at high scale. This has two bad
effects:
 - a wrong normalisation, where the cross section of the minimum-bias events is
   not taken into accout;
 - a lack of statistics at high transverse momentum.

The goal of the current subsystem is to correc the simulation.

The correction is performed in two steps:
 - first events are removed when the hard scale of a pile-up interaction is larger than the
   hard scale of the matrix elements;
 - then an additional cut is still applied to remove remaing outliers.

*Warning*: for the moment, the code is only tuned for the Pythia 8 flat sample for
2017.

## `getPUstaub`

Test and tune the cut-offsm usefull for the next command as well.

## `applyPUstaubSauger`

Filter the *n*-tuples.
