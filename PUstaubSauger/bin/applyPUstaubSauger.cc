#include <iostream>
#include <vector>
#include <filesystem>

#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Jet.h"

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"
#include "Core/CommonTools/interface/terminal.h"

#include "Core/PUstaubSauger/interface/sigma.h"

#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1.h>

using namespace std;
using namespace DAS;

namespace fs = filesystem;

////////////////////////////////////////////////////////////////////////////////
/// StaubSaugerRescale
///
/// recover the correct distribution of the hard scale before cutoff based on weight
struct StaubSaugerRescale {
    TH1 * correction;
    StaubSaugerRescale (TFile * f)
    {
        correction       = dynamic_cast<TH1 *>(f->Get("dirty/pthat"));
        correction->Divide(dynamic_cast<TH1 *>(f->Get("blink/pthat")));
    }
    StaubSaugerRescale (TString location) :
        StaubSaugerRescale(TFile::Open(location, "READ"))
    { }
    double operator() (double scale)
    {
        int ibin = correction->FindBin(scale);
        return correction->GetBinContent(ibin);
    }
    double operator () (Event * event)
    {
        double scale = event->hard_scale;
        return operator()(scale);
    }
};

////////////////////////////////////////////////////////////////////////////////
/// Remove PU staub (i.e. high-weight events due to bad PU sampling)
void applyPUstaubSauger 
            (const fs::path& input,      //!< input root file
             const fs::path& output,     //!< output root file
             const fs::path& correction, //!< txt file with the parameters for cut-off function
             int nSplit = 1,     //!< number of jobs/tasks/cores
             int nNow = 0)       //!< index of job/task/core
{
    assert(fs::exists(input));
    auto oldchain = new TChain("inclusive_jets"); // arg = fs::path inside the ntuple
    oldchain->AddFile(input.c_str());
    cout << "File " << input << endl;

    // removal / staub sauger
    StaubSaugerRescale rescale(input.c_str());
    assert(fs::exists(correction));
    Sauge sauge(correction.c_str());

    // branches
    Event * event = nullptr;
    PileUp * pileup = nullptr;
    oldchain->SetBranchAddress("event", &event);
    oldchain->SetBranchAddress("pileup", &pileup);
    vector<RecJet> * recjets = nullptr;
    vector<GenJet> * genjets = nullptr;
    oldchain->SetBranchAddress("recJets", &recjets);
    oldchain->SetBranchAddress("genJets", &genjets);

    if (fs::exists(output))
        cerr << red << "Overwriting existing output file\n" << normal;
    auto newfile = TFile::Open(output.c_str(), "RECREATE");
    auto newtree = oldchain->CloneTree(0);

    MetaInfo metainfo(newtree);
    metainfo.AddCorrection("PUstaubSauger");
    if (nNow == 0) metainfo.Print();

    // for MC PU profile
    auto genpt = new TH2D("genpt", "genpt", nPtBins, pt_edges.data(), nYbins, y_edges.data()),
         recpt = new TH2D("recpt", "recpt", nPtBins, pt_edges.data(), nYbins, y_edges.data());

    Looper looper(__func__, oldchain, nSplit, nNow);
    while (looper.Next()) {

        if (sauge(pileup, event, recjets, genjets)) 
            continue;

        event->genWgts.front() *= rescale(event);

        auto gW = event->genWgts.front(),
             rW = event->recWgts.front();

        newtree->Fill();

        // control plots
        for (const RecJet& jet: *recjets)
            recpt->Fill(jet.RawPt(), jet.AbsRap(), gW * rW);
        for (const GenJet& jet: *genjets)
            genpt->Fill(jet.p4.Pt(), jet.AbsRap(), gW);
    }

    // closing
    newtree->AutoSave();
    for (auto h: {genpt, recpt}) {
        h->SetDirectory(newfile);
        h->Write();
    }
    sauge.Write(newfile);
    newfile->Close();
    delete oldchain;
    cout << "Done " << output << endl;
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    if (argc < 4) {
        cout << argv[0] << "input output correction [nSplit [nNow]]\n"
             << "\twhere\tinput = n-tuple\n"
             << "\t     \toutput = new n-tuple\n" 
             << "\t     \tcorrection = txt file with parameters of function to cut off bad events" << endl;
        return EXIT_SUCCESS;
    }
    fs::path input = argv[1],
             output = argv[2],
             correction = argv[3];
    int nNow = 0, nSplit = 1;
    if (argc > 4) nSplit = atoi(argv[4]);
    if (argc > 5) nNow   = atoi(argv[5]);

    applyPUstaubSauger(input, output, correction, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
