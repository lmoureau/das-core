#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <vector>
#include <filesystem>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/terminal.h"

#include "Core/PUstaubSauger/interface/sigma.h"

#include <TROOT.h>
#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>

#include "Math/VectorUtil.h"

using namespace std;
using namespace DAS;

namespace fs = std::filesystem;

////////////////////////////////////////////////////////////////////////////////
/// Get plots for PU staub (i.e. high-weight events due to bad PU sampling)
void getPUstaub 
            (const fs::path& input,  //!< name of input root file
             const fs::path& output, //!< name of output root file
             const fs::path& correction, //!< name of txt file with the parameters for cut-off function
             int nSplit = 1, //!< number of jobs/tasks/cores
             int nNow = 0)   //!< index of job/task/core
{
    //Get old file, old tree and set top branch address
    TChain * oldchain = new TChain("inclusive_jets"); // arg = fs::path inside the ntuple
    assert(fs::exists(input));
    oldchain->AddFile(input.c_str());
    cout << "File " << input << endl;

    Event * event = nullptr;
    PileUp * pileup = nullptr;
    oldchain->SetBranchAddress("event", &event);
    oldchain->SetBranchAddress("pileup", &pileup);

    vector<RecJet> * recjets = nullptr;
    vector<GenJet> * genjets = nullptr;
    oldchain->SetBranchAddress("recJets", &recjets);
    oldchain->SetBranchAddress("genJets", &genjets);

    if (fs::exists(output))
        cerr << red << "Overwriting existing output file\n" << normal;
    auto file = TFile::Open(output.c_str(), "RECREATE");

    assert(fs::exists(correction));
    Sauge sauge(correction.c_str());

    Looper looper(__func__, oldchain, nSplit, nNow);
    while (looper.Next()) 
        sauge(pileup, event, recjets, genjets);
    
    sauge.Write(file); // control plots

    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    if (argc < 4) {
        cout << argv[0] << " input output correction [nSplit [nNow]]\n"
             << "\twhere\tinput = n-tuple\n"
             << "\t     \toutput = root file with histograms\n"
             << "\t     \tcorrection = txt file with parameters of function to cut off bad events" << endl;
        return EXIT_SUCCESS;
    }

    fs::path input = argv[1],
         output = argv[2],
         correction = argv[3];
    int nNow = 0, nSplit = 1;
    if (argc > 4) nSplit = atoi(argv[4]);
    if (argc > 5) nNow   = atoi(argv[5]);

    getPUstaub(input, output, correction, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
