#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <vector>
#include <filesystem>

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/terminal.h"

#include <TString.h>
#include <TFile.h>
#include <TH2D.h>

#include <TF1.h>

using namespace std;
using namespace DAS;

namespace fs = std::filesystem;

////////////////////////////////////////////////////////////////////////////////
/// Exclude PU contribution at high pt by performing a double Gaussian fit.
/// 
/// Output is similar to that of `getPUstaubLimitsSlices` with a max weight per rec pt bin.
void getPUstaubLimitsFlat (const fs::path& input,
                           const fs::path& output1,
                           const fs::path& output2)
{
    assert(fs::exists(input));
    auto f = TFile::Open(input.c_str(), "READ");
    TH2 * h2 = dynamic_cast<TH2 *>(f->Get("clean/recptLogWgt"));
    h2->SetDirectory(0);
    f->Close();

    if (fs::exists(output1))
        cerr << red << "Overwriting existing output file\n" << normal;
    f = TFile::Open(output1.c_str(), "RECREATE");

    cout << "ptmin\tmaxlogw" << endl;
    if (fs::exists(output2))
        cerr << red << "Overwriting existing output file\n" << normal;
    ofstream o(output2.c_str());
    o << "ptmin\tmaxlogw\n";

    int N = h2->GetNbinsX();
    for (int i=1; i<=N; ++i) {

        int ptmin = h2->GetXaxis()->GetBinLowEdge(i);

        TH1 * p = h2->ProjectionY(Form("p%d",ptmin),i,i);
        double a = p->Integral();
        if (a < numeric_limits<float>::epsilon()) continue;
        p->Scale(1./a);
        p->Write();

        // fit
        // 1) simple gaus
        p->Fit("gaus", "0Q");
        TF1 * f = p->GetFunction("gaus");
        double par[6];
        f->GetParameters(par);
        // 2) double gaus
        par[3] = par[0]/4;
        par[4] = par[1]+par[2];
        par[5] = 2*par[2];
        TF1 * doublegaus = new TF1(Form("f%d", ptmin), "gaus(0)+gaus(3)", -30, 20);
        doublegaus->SetParameters(par);
        doublegaus->SetLineWidth(1);
        p->Fit(doublegaus, "0Q");
        doublegaus->Write();
        doublegaus->GetParameters(par);
        if (par[5] > par[1]) {
            cout << "Skipping current entry (ptmin = " << ptmin << ") since fit of second Gaussian was not successful\n";
            continue;
        }
        double cut = par[4]+4*par[5]; // we exclude anything at more than four sigmas from the second gaussian

        // write
        o << ptmin << '\t' << cut << '\n';
        cout << ptmin << ' ' << cut << endl;
    }
    f->Close();
    o.close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2(true);
    if (argc < 4) {
        cout << argv[0] << " input output1 output2\n"
             << "\twhere\tinputs = flat root file with histograms (from `getPUstaub` or `applyMClumi`)\n"
             << "\t     \toutput1 = root file with histograms and fits\n"
             << "\t     \toutput2 = txt file with cuts (to be placed in `$DAS_WORKAREA/system/tables/PUstaub/akX`)\n"
             << "NB: this executable expect a flat sample!!" << endl;
        return EXIT_SUCCESS;
    }

    fs::path input = argv[1],
         output1 = argv[2],
         output2 = argv[3];

    getPUstaubLimitsFlat(input, output1, output2);
    return EXIT_SUCCESS;
}
#endif
