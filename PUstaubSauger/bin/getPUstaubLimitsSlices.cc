#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <limits>
#include <filesystem>

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/terminal.h"

#include <TROOT.h>
#include <TString.h>
#include <TFile.h>
#include <TH2D.h>
#include <TRegexp.h>

#include <TF1.h>

using namespace std;
using namespace DAS;

namespace fs = std::filesystem;

static const auto deps = numeric_limits<double>::epsilon();

////////////////////////////////////////////////////////////////////////////////
/// Dummy class/wrapper for `getPUstaubLimitsSlices`,
/// just used to split the code into smaller subroutines.
struct SliceSauger {

    vector<TH2 *> h2s, //!< `recptLogWgt` per slice
                  n2s, //!< `recptLogWgt` per slice, normalised to respective weights
                  s2s; //!< relative uncertainty from `recptLogWgt` per slice, normalised to respective weights
    int nslices; //!< number of slices (determined from `inputs`)
    map<int, map<int, double>> contents,      //!< number of entries per pt bin and per slice
                               norm_contents, //!< weighted / effective number of entries per pt bin and per slice
                               errors; //!< relative uncertainty in % per pt bin and per slice
    map<int, map<int, bool>> Bads, //!< flag for bad contributions per pt bin and per slice
                             Mins; //!< flag for minimum relative uncertainty
    map<int, double> removed_events; //!< contains the fraction of removed events per slice

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor
    ///
    /// Extracts the histograms from the outputs
    SliceSauger (vector<fs::path> slices) :
        nslices(slices.size())
    {
        assert(nslices > 0);
        for (auto slice: slices) {

            TString fname = slice.filename().c_str();
            TFile * f = TFile::Open(fname);

            TString title = fname;
            title.ReplaceAll(".root", "");
            TString prefix = title(TRegexp("^[A-Za-z]*1[6-8]_")); // e.g. Pythia16_
            title.ReplaceAll(prefix, "");
            TString name = title;
            TString suffix = name(TRegexp("to[0-9]*")); 
            name.ReplaceAll(suffix, "");
            name.ReplaceAll("Inf", "");

            TH2 * h2 = dynamic_cast<TH2 *>(f->Get("clean/recptLogWgt"));
            h2->SetNameTitle("h" + name, title);
            h2->SetDirectory(0);

            TH2 * n2 = dynamic_cast<TH2 *>(h2->Clone("n" + name));
            n2->SetTitle(title);
            n2->Reset();
            n2->SetDirectory(0);

            TH2 * s2 = dynamic_cast<TH2 *>(h2->Clone("s" + name));
            s2->SetTitle(title);
            s2->Reset();
            s2->SetDirectory(0);

            for (int i = 1; i <= h2->GetNbinsY(); ++i) {
                double logw = h2->GetYaxis()->GetBinCenter(i);
                double w = exp(logw);

                for (int j = 1; j <= h2->GetNbinsX(); ++j) {
                    double content = h2->GetBinContent(j,i);
                    content *= w;
                    n2->SetBinContent(j,i,content);
                    content *= w;
                    s2->SetBinContent(j,i,content);
                }
            }

            h2s.push_back(h2);
            n2s.push_back(n2);
            s2s.push_back(s2);
            f->Close();
        }

        auto mySort = [](const char * c) {
            return [c](TH2 * a, TH2 * b) {
                TString A = a->GetName(), B = b->GetName();
                A.ReplaceAll(c, "");
                B.ReplaceAll(c, "");
                return A.Atoi() < B.Atoi();
            };
        };
        sort(h2s.begin(), h2s.end(), mySort("h"));
        sort(n2s.begin(), n2s.end(), mySort("n"));
        sort(s2s.begin(), s2s.end(), mySort("s"));
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// create matrix and normalise by row
    void FillContentMap (TFile * f = nullptr)
    {
        for (int ipt=1; ipt<=nPtBins; ++ipt) {
            int lowedge = h2s.front()->GetXaxis()->GetBinLowEdge(ipt);
            gFile->cd();
            TDirectory * d = gFile->mkdir(Form("pt%d", lowedge));

            vector<TH1 *> h1s, n1s, s1s;

            // nominal value
            TDirectory * dd = d->mkdir("contents");
            dd->cd();
            for (TH2 * h2: h2s) {
                TString name = Form("slice%s", h2->GetName());
                TH1 * h1 = h2->ProjectionY(name,ipt,ipt);
                h1s.push_back(h1);
            }
            for (int isl = 0; isl < nslices; ++isl) {
                TH1 * h1 = h1s.at(isl);
                if (f != nullptr) h1->Write();
                contents[ipt][isl] = h1->Integral();
            }
            d->cd();

            // normalised nominal value
            dd = d->mkdir("norm_contents");
            dd->cd();
            for (TH2 * n2: n2s) {
                TString name = Form("slice%s", n2->GetName());
                TH1 * n1 = n2->ProjectionY(name,ipt,ipt);
                n1s.push_back(n1);
            }
            for (int isl = 0; isl < nslices; ++isl) {
                TH1 * n1 = n1s.at(isl);
                if (f != nullptr) n1->Write();
                norm_contents[ipt][isl] = n1->Integral();
            }
            d->cd();

            // uncertainty
            dd = d->mkdir("cumul_stat_unc");
            dd->cd();
            for (TH2 * s2: s2s) {
                TString name = Form("slice%s", s2->GetName());
                TH1 * s1 = s2->ProjectionY(name,ipt,ipt);
                s1s.push_back(s1);
            }
            for (int isl = 0; isl < nslices; ++isl) {
                TH1 * s1 = s1s.at(isl);
                if (f != nullptr) s1->Write();
                errors[ipt][isl] = s1->Integral();
            }
        }

        // one more step to determine the cumulative relative uncertainty
        auto cumul_cont = norm_contents;
        for (int ipt = 1; ipt<=nPtBins; ++ipt) {

            // make the cumulative sums
            for (int isl = nslices-1; isl >=0; --isl) { // start from above slices
                cumul_cont[ipt][isl] = cumul_cont[ipt][isl] + cumul_cont[ipt][isl+1];
                errors[ipt][isl] = errors[ipt][isl] + errors[ipt][isl+1];
            }

            // normalise errors
            for (int isl = 0; isl < nslices; ++isl) {
                if (cumul_cont[ipt][isl] > 0) {
                    double error = errors[ipt][isl] / pow(cumul_cont[ipt][isl],2);
                    errors[ipt][isl] = 100 * sqrt(error);
                }
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Find "bad" bins, i.e. bins that should be excluded.
    ///
    /// 2 conditions:
    /// - at 30 entries for a low pthat slice,
    /// - the absolute contribution of a low pthat slice should never be larger
    ///   than half of the absolute contribution of any higher pthat slice.
    void FillFlagBadMap ()
    {
        for (int ipt=1; ipt<=nPtBins; ++ipt) {
            Bads[ipt][nslices-1] = false;
            bool AlwaysGood = false;
            for (int isl = 0; isl < nslices-1; ++isl) { 

                if (AlwaysGood) {
                    Bads[ipt][isl] = false;
                    continue;
                }

                double content = contents.at(ipt).at(isl);
                
                double norm_content1 = norm_contents.at(ipt).at(isl  ), // content in the current pt bin
                       norm_content2 = norm_contents.at(ipt).at(isl+1); // content in the next lower pt bin
                bool Bad = content < 30 || norm_content1 > norm_content2/2; // i.e. the contribution from the slice should decrease
                Bads[ipt][isl] = Bad;
                AlwaysGood = !Bad;
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Find bins with minimal cumulative relative uncertainty
    void FillFlagMinMap ()
    {
        for (int ipt=1; ipt<=nPtBins; ++ipt) {
            auto& error = errors.at(ipt);
            auto MinEl = min_element(error.begin(), error.end(),
                     [](const auto& l, const auto& r)
                     { return l.second > deps && l.second <= r.second; });
            for (int isl = 0; isl < nslices; ++isl) 
                Mins[ipt][isl] = (isl == MinEl->first);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Avoid to remove certain bins:
    /// - one random bin in the middle of the slice
    /// - or one random bin in the tail
    void RegulariseBadMap ()
    {
        // regularising pt bins for each slice separetely
        for (int isl = 0; isl < nslices-1; ++isl)  {

            bool AlwaysGood = false;
            for (int ipt=nPtBins-1; ipt>=2; --ipt) {
                if (AlwaysGood) {
                    Bads[ipt][isl] = false;
                    continue;
                }
                if (contents.at(ipt).at(isl) < 30) {
                    Bads[ipt][isl] = true;
                    continue;
                }
                bool& bad1 = Bads[ipt-1][isl],
                    & bad2 = Bads[ipt  ][isl],
                    & bad3 = Bads[ipt+1][isl];

                AlwaysGood = !((!bad1) && (!bad2) && (!bad3));

                if ((!bad1) && (!bad3)) bad2 = false;
                if (  bad1  &&   bad3 ) bad2 = true;
            }
        }
        // the last slice is always entirely taken
        for (int ipt=1; ipt<=nPtBins; ++ipt) Bads[ipt][nslices-1] = false;
    }


    ////////////////////////////////////////////////////////////////////////////////
    /// estimates the fraction of events removed per pthat slice
    void GetFractionOfRemovedEvents ()
    {
        for (int isl = 0; isl < nslices; ++isl)  {
            double numerator = 0, denominator = 0;
            for (int ipt=1; ipt<=nPtBins; ++ipt) {
                double content = contents.at(ipt).at(isl);
                denominator += content;
                if (Bads.at(ipt).at(isl)) numerator += content;
            }
            if (denominator > 0)
                removed_events[isl] = 100. * numerator / denominator;
            else
                removed_events[isl] = 0.;

        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// print with beautiful colours on terminal
    ///
    /// The first column gives the low edge of the current pt bin.
    /// The header gives the pthat slice.
    ///
    /// For each slice & pt bin:
    /// - absolute number of jets in that bin,
    /// - relative cumulative statistical uncertainty
    ///   (cumulative from the right hand side!).
    ///
    /// Colour code:
    ///  - excluded contributions are shown in red;
    ///  - statistically most precise contribution is shown in green;
    ///  - additional contributions shown in normal colour.
    void Print ()
    {
        cout << "ptmin   " << left;
        for (auto h2: h2s) cout << setw(18) << h2->GetTitle();
        cout << endl;

        cout << right;
        
        for (int ipt=1; ipt<=nPtBins; ++ipt) {
            int ptmin = h2s.front()->GetXaxis()->GetBinLowEdge(ipt-1);

            // content
            cout << "\x1B[30m\e[0m" << setw(4) << ptmin;
            for (int isl = 0; isl < nslices; ++isl)  {

                //cout << isl << ' ' << ipt << endl;
                int content = contents.at(ipt).at(isl);
                double error = errors.at(ipt).at(isl);
                bool Bad = Bads.at(ipt).at(isl);
                bool Min = Mins.at(ipt).at(isl);

                if (Bad)      cout << red;
                else if (Min) cout << green;
                else          cout << normal;

                cout << setw(11) << content 
                     << setw( 6) << setprecision(2) << fixed << error << '%';
            }
            cout << '\n';
        }
        if (removed_events.size() > 0) {
            cout << normal << "    " << setprecision(6);
            for (int isl = 0; isl < nslices; ++isl)
                cout << setw(17) << removed_events.at(isl) << '%';
        }
        cout << endl;
    }

    //////////////////////////////////////////////////////////////////////////////// 
    /// Write cuts on weight per pt bin
    void Write (auto& Stream)
    {
        Stream << "ptmin\tmaxlogw\n";

        for (int ipt=1; ipt<=nPtBins; ++ipt) {
            int ptmin = n2s.front()->GetXaxis()->GetBinLowEdge(ipt-1);
            for (int isl = 0; isl < nslices; ++isl) {
                if (Bads.at(ipt).at(isl)) continue;

                TH2 * n2 = n2s.at(isl);
                for (int iwgt = n2->GetNbinsY(); iwgt > 0; --iwgt) {
                    double content = n2->GetBinContent(ipt, iwgt);
                    if (content < deps) continue;
                    double maxlogw = n2->GetYaxis()->GetBinLowEdge(iwgt+1);
                    Stream << ptmin << '\t' << maxlogw << '\n';
                    break;
                }
                break;
            }
        }
    }
};

////////////////////////////////////////////////////////////////////////////////
/// Exclude contributions from PU at high rec pt coming from low pt hat slices
/// by comparing directly the weighted contribution from each slice in each rec pt bin.
///
/// In addition, a check is performed by looking at the cumulative statistical uncertainty
/// from the highest pt hat slice to the current pt hat slice (i.e. we check how adding a 
/// lower pt hat slice improves or degrades the statistics of a given rec pt bin).
///
/// Output is similar to that of `getPUstaubLimitsFlat` with a max weight per rec pt bin.
void getPUstaubLimitsSlices (const vector<fs::path>& inputs,
                             const fs::path& output1,
                             const fs::path& output2)
{
    for (const auto& input: inputs)
        assert(fs::exists(input));
    SliceSauger ss(inputs);

    if (fs::exists(output1))
        cerr << red << "Overwriting existing output file\n" << normal;
    auto f = TFile::Open(output1.c_str(), "RECREATE");
    ss.FillContentMap(f);
    f->Close();
    ss.FillFlagBadMap();
    ss.FillFlagMinMap();
    ss.RegulariseBadMap();
    ss.GetFractionOfRemovedEvents();
    ss.Print();
    //ss.Write(cout);
    //cout << flush;

    if (fs::exists(output2))
        cerr << red << "Overwriting existing output file\n" << normal;
    ofstream o(output2.c_str());
    ss.Write(o);
    o.close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2(true);
    if (argc < 4) {
        cout << argv[0] << " input1 [input2 [input3 [...]]] output1 output2\n"
             << "\twhere\tinputs = root files with histograms (from `getPUstaub` or `applyMClumi`)\n"
             << "\t     \toutput1 = root file with histograms\n"
             << "\t     \toutput2 = txt file with cuts (to be placed in `$DAS_WORKAREA/system/tables/PUstaub/akX`)\n"
             << "NB: this executable expect a sample in slices!!" << endl;
        return EXIT_SUCCESS;
    }

    vector<fs::path> inputs;
    for (int iarg = 1; iarg < argc -2; ++iarg)
        inputs.push_back(argv[iarg]);

    fs::path output1 = argv[argc-2],
         output2 = argv[argc-1];

    getPUstaubLimitsSlices(inputs, output1, output2);
    return EXIT_SUCCESS;
}
#endif
