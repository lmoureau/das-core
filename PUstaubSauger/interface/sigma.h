#include <vector>
#include <map>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include <TFile.h>
#include <TH2.h>
#include <TGraph.h>

namespace DAS {

////////////////////////////////////////////////////////////////////////////////
/// structure to host relevant distributions for PU staub removal
struct PUstaubPlots {

    static TH1 * MakeHist (TString name, TString title);
    static TH2 * MakeHist2D (TString name, TString title);

    const TString name;

    TH1 * genpt, * recpt, * pthat, * pthatMax, * pthatMaxPU;
    TH2 * genpt_y, * recpt_y, * recptLogWgt;

    PUstaubPlots (TString Name);

    void Fill (PileUp * pileup,
               Event * event,
               std::vector<RecJet> * recjets,
               std::vector<GenJet> * genjets);

    void Write (TFile * f) const;
};

////////////////////////////////////////////////////////////////////////////////
/// functor to apply PU cleaning
struct Sauge {

    std::map<int, double> cuts;

    PUstaubPlots * dirty, * clean, * blink;

    Sauge (); //!< constructor if no file for correction is available
    Sauge (const char * file);

    bool operator() 
            (PileUp * pileup,       //!< pile-up, contains hard scale of PU interactions
             Event * event,         //!< event, contains hard scale of ME & event weight
             std::vector<RecJet> * recjets,  //!< collection of reconstructed jets
             std::vector<GenJet> * genjets); //!< collection of reconstructed jets

    void Write (TFile * f) const;
};

}
