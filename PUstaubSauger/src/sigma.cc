#include <cmath>
#include <filesystem>

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/PUstaubSauger/interface/sigma.h"

using namespace std;
namespace fs = std::filesystem;

namespace DAS {


Sauge::Sauge () :
    dirty(new PUstaubPlots("dirty")),
    clean(new PUstaubPlots("clean")),
    blink(nullptr)
{ }

Sauge::Sauge (const char * correction) :
    dirty(new PUstaubPlots("dirty")),
    clean(new PUstaubPlots("clean")),
    blink(new PUstaubPlots("blink"))
{
    fs::path p(correction);
    cout << p << endl;
    assert(fs::exists(p));

    ifstream file(correction);

    {
        TString ptmin, maxlogw;
        file >> ptmin >> maxlogw;
        assert(ptmin == "ptmin" && maxlogw == "maxlogw");
    }
    
    while (file.good()) {

        // extracting
        double ptmin, maxlogw;
        file >> ptmin >> maxlogw;
        cout << ptmin << ' ' << maxlogw << '\n';
        if (cuts.find(ptmin) == cuts.end())
            cuts.insert({ptmin, maxlogw});
    }
    cout << flush;
    file.close();
}

void Sauge::Write (TFile * f) const
{
    for (auto h: {dirty, clean, blink})
        if (h!=nullptr)
            h->Write(f);
}

////////////////////////////////////////////////////////////////////////////////
/// Cutting function for PU staub sauging
///
/// Returns true in case of bad event
bool Sauge::operator()
        (PileUp * pileup,          //!< pile-up, contains hard scale of PU interactions
         Event * evnt,             //!< event, contains hard scale of ME & event weight
         vector<RecJet> * recjets, //!< collection of reconstructed jets
         vector<GenJet> * genjets) //!< collection of generated jets
{
    dirty->Fill(pileup, evnt, recjets, genjets); 

    // first cut, based on hard scale
    double PU_hard_scale = pileup->pthatMax,
           ME_hard_scale = evnt->hard_scale;
    if (PU_hard_scale > ME_hard_scale) return true; // bad event
    clean->Fill(pileup, evnt, recjets, genjets);

    // last cut, based on weight and leading pt
    bool Bad = false;
    if (recjets->size() > 0) {
        int pt = recjets->front().p4.Pt();
        double w = evnt->genWgts.front();
        for (auto it = cuts.rbegin(); it != cuts.rend(); ++it) {
            if (pt < it->first) continue;
            Bad = log(w) > it->second;
            break;
        }
    }
    if (!Bad) blink->Fill(pileup, evnt, recjets, genjets);
    return Bad;
}

TH1 * PUstaubPlots::MakeHist (TString name, TString title)
{
    return new TH1D(name + title, title, nPtBins, pt_edges.data());
}

TH2 * PUstaubPlots::MakeHist2D (TString name, TString title)
{
    return new TH2D(name + title, title, nPtBins, pt_edges.data(), nYbins, y_edges.data());
}

PUstaubPlots::PUstaubPlots (TString Name) :
    name(Name),
    genpt     (MakeHist(name, "genpt"     )),
    recpt     (MakeHist(name, "recpt"     )),
    pthat     (MakeHist(name, "pthat"     )),
    pthatMax  (MakeHist(name, "pthatMax"  )),
    pthatMaxPU(MakeHist(name, "pthatMaxPU")),
    genpt_y   (MakeHist2D(name, "genpt_y" )),
    recpt_y   (MakeHist2D(name, "recpt_y" )),
    recptLogWgt  (new TH2D(name + "recptLogWgt", "recptLogWgt", nPtBins, pt_edges.data(), 400, -30, 20))
{ }

void PUstaubPlots::Fill (PileUp * pileup, Event * event, vector<RecJet> * recjets, vector<GenJet> * genjets)
{
    auto PU_hard_scale = pileup->pthatMax,
         ME_hard_scale = event->hard_scale,
         gW = event->genWgts.front(),
         rW = event->recWgts.front();

    for (auto& genjet: *genjets) {
        genpt->Fill(genjet.p4.Pt(), gW);
        genpt_y->Fill(genjet.p4.Pt(), genjet.AbsRap(), gW);
    }
    for (auto& recjet: *recjets) {
        recpt->Fill(recjet.RawPt(), gW * rW);
        recpt_y->Fill(recjet.RawPt(), recjet.AbsRap(), gW * rW);
    }
    if (recjets->size() > 0)
        recptLogWgt->Fill(recjets->front().RawPt(), log(gW * rW)); // we *don't* apply the weight to the entry
    pthat     ->Fill(    ME_hard_scale               , gW);
    pthatMax  ->Fill(max(ME_hard_scale,PU_hard_scale), gW);
    pthatMaxPU->Fill(                  PU_hard_scale , gW);
}

void PUstaubPlots::Write (TFile * f) const
{
    TDirectory * d = f->mkdir(name);
    d->cd();
    for (TH1 * h: {genpt, recpt, pthat, pthatMax, pthatMaxPU, 
            dynamic_cast<TH1*>(genpt_y), dynamic_cast<TH1*>(recpt_y),
            dynamic_cast<TH1*>(recptLogWgt)})
        h->Write(h->GetTitle());
    f->cd();
}

}
