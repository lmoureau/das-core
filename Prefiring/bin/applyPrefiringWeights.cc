#include <cassert>
#include <iostream>
#include <limits>

#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"
#include "Core/CommonTools/interface/terminal.h"

#include <TROOT.h>
#include <TString.h>
#include <TChain.h>
#include <TFile.h>

#include "Math/VectorUtil.h"

#include "Core/Prefiring/interface/applyPrefiringWeights.h"
#include "Core/CommonTools/interface/ControlPlots.h"

using namespace std;
using namespace DAS;

namespace fs = std::filesystem;

////////////////////////////////////////////////////////////////////////////////
/// Application of prefiring weights in data or simulation.
///
/// Calls a functor
void applyPrefiringWeights 
             (const fs::path& input,  //!< name of input root file 
              const fs::path& output, //!< name of output root file
              PrefOpt prefOpt, //!< option to choose the right map(s)
              int nSplit = 1, //!< number of jobs/tasks/cores
              int nNow = 0)   //!< index of job/task/core
{
    // Get old file, old tree and set top branch address
    assert(fs::exists(input));
    TChain * oldchain = new TChain("inclusive_jets"); // arg = fs::path inside the ntuple
    oldchain->Add(input.c_str());

    Event * evnt = nullptr;
    oldchain->SetBranchAddress("event", &evnt);
    vector<RecJet> * recJets = nullptr;
    oldchain->SetBranchAddress("recJets", &recJets);

    if (fs::exists(output))
        cerr << red << output << " will be overwritten\n" << normal;
    TFile * newfile = TFile::Open(output.c_str(), "RECREATE");
    TTree * newtree = oldchain->CloneTree(0);

    MetaInfo metainfo(newtree);
    if (nNow == 0) metainfo.Print();
    TString correction = "PrefiringWeights";
    metainfo.AddCorrection(correction);
    metainfo.AddRecEvWgt("Prefup");
    metainfo.AddRecEvWgt("Prefdown");
    bool isMC = metainfo.isMC();

    vector<GenJet> * genJets = nullptr;
    if (isMC) oldchain->SetBranchAddress("genJets", &genJets);

    int year = metainfo.year();
    applyPrefiringWeightsFunctor apply(year, prefOpt, isMC);

    ControlPlots::isMC = isMC;
    ControlPlots raw("raw");
    vector<ControlPlots> calib { ControlPlots("nominal"),
                                 ControlPlots("upper"  ),
                                 ControlPlots("lower"  ) };
    auto totRecWgt = [&](size_t i) {
        return (isMC ? evnt->genWgts.front() : 1) * evnt->recWgts.at(i);
    };

    Looper looper(__func__, oldchain, nSplit, nNow);
    while (looper.Next()) {
        if (isMC) raw(*genJets, evnt->genWgts.front());
        raw(*recJets, totRecWgt(0));
        apply(*evnt, *recJets);
        for (size_t i = 0; i < calib.size(); ++i) {
            if (isMC) calib.at(i)(*genJets, evnt->genWgts.front());
            if(recJets->size()==0) continue;
            calib.at(i)(*recJets, totRecWgt(i));
        }
        newtree->Fill();
    }
    newtree->AutoSave();


    raw.Write(newfile);
    for (size_t i = 0; i < calib.size(); ++i)
        calib.at(i).Write(newfile);

    TDirectory * controlplots = newfile->mkdir("Prefiring");
    apply.Write(controlplots);

    newfile->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 4) {
        cout << argv[0] << " input output option [nSplit [nNow]]\n"
             << "\twhere\tinput = n-tuple\n"
             << "\t     \toutput = modified n-tuple\n"
             << "\t     \toption = `AverageMap` (default; only possible option for MC), `MapsPerEra`, or `SmoothMapsPerEra`\n"
             << flush;
        return EXIT_SUCCESS;
    }

    fs::path input = argv[1],
         output = argv[2];

    TString option = argv[3];
    PrefOpt prefOpt = option =="MapsPerEra"       ?   PrefOpt::kMapsPerEra       :
                      option =="SmoothMapsPerEra" ?   PrefOpt::kSmoothMapsPerEra :
                    /*option =="AverageMap"       ?*/ PrefOpt::kAverageMap       ;

    int nNow = 0, nSplit = 1;
    if (argc > 4) nSplit = atoi(argv[4]);
    if (argc > 5) nNow = atoi(argv[5]);

    applyPrefiringWeights(input, output, prefOpt, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
