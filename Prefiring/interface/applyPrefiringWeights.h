#include <cstdlib>
#include <cassert>

#include <iostream>
#include <vector>
#include <tuple>
#include <filesystem>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"
//#include "Core/CommonTools/interface/terminal.h"

#include <TH2.h>
#include <TF1.h>

using namespace std;
using namespace DAS;

namespace fs = filesystem;

static const auto feps = numeric_limits<float>::epsilon();

static const std::map<int, std::map<int, TString>> eras
{
    { 2016,
        {
            { 272007, "BCD"},
            { 276831, "EF" }, // F probably corresponds to Fearly
            { 278802, "FGH" } // checked with Laurent Thomas (probably corresponds to Flate)
        }
    },

    { 2017,
        {
            //{ 294034, "A" },
            { 297000, "B" },
            { 299337, "C" },
            { 302030, "D" },
            { 303435, "E" },
            { 304911, "F" }
        }
    },
};

enum PrefOpt {
    kAverageMap      ,
    kMapsPerEra      ,
    kSmoothMapsPerEra
};

////////////////////////////////////////////////////////////////////////////////
/// Functor to apply weights from pre-firing maps
///
/// Source: following link + private chats with Laurent Thomas:
/// https://twiki.cern.ch/twiki/bin/viewauth/CMS/L1ECALPrefiringWeightRecipe
///
/// *Note*: we ignore photons
///
/// In contrast to the R-scan analysis (AN-18-044), we take the value from the map
/// itself (i.e. we do not fit or smooth anything).
struct applyPrefiringWeightsFunctor {

    const bool isMC; //!< flag

    map<int, tuple<TH2*, TH2*, TH2*>> prefRawWeights; //!< weights extracted from the maps, indiced with run number (three variations)
    map<int, tuple<TF1 *, TF1*>> prefSmoothWeights; //!< weights extracted from smooth functions, indiced with run number (two rapidity bins)

    using Operation = tuple<float,float,float> (applyPrefiringWeightsFunctor::*)(int,const RecJet&) const;
    const Operation GetWeights;

    vector<TH2 *> multiplicities; //!< jets in the forward region in the same binning as the maps (before correction)

    ////////////////////////////////////////////////////////////////////////////////
    /// Prepare weights including variations.
    ///
    /// The original maps contain the probability for *one* jet to cause the prefiring
    /// - we convert it to a weight = 1 - proba 
    /// - we  derive the uncertainty in addition, following the official recommendation:
    /// "the uncertainty is taken as the maximum between 20 percents of the object prefiring probability
    /// and the statistical uncertainty associated to the considered bin in the prefiring map."
    static tuple<TH2*, TH2*, TH2*> PrepareRawWeights (TString hname, TH2* hIn)
    {
        auto MapNo = dynamic_cast<TH2*>(hIn->Clone("nominal_map_" + hname)),
             MapUp = dynamic_cast<TH2*>(hIn->Clone("upper_map_"   + hname)),
             MapDn = dynamic_cast<TH2*>(hIn->Clone("lower_map_"   + hname));
        MapNo->SetDirectory(0);
        MapUp->SetDirectory(0);
        MapDn->SetDirectory(0);
        for (int etabin = 0; etabin <= hIn->GetNbinsX()+1; ++etabin)
        for (int  ptbin = 0;  ptbin <= hIn->GetNbinsY()+1; ++ ptbin) {
            auto proba = hIn->GetBinContent(etabin, ptbin),
                 error = hIn->GetBinError  (etabin, ptbin);
            
            // uncertainty only in the forward region! (the following boolean is a trick :D)
            bool Forward = (proba > feps); // means proba > 0
            auto unc = Forward ? max(proba * 0.2f, error) : 0.f;

            // the map contains the probability, but we want the weight
            auto weight = 1. - proba;

            MapNo->SetBinContent(etabin, ptbin, weight);
            MapUp->SetBinContent(etabin, ptbin, weight+unc);
            MapDn->SetBinContent(etabin, ptbin, weight-unc);

            //if (Forward) cout << green;
            //cout << setw(10) << etabin << setw(10) << ptbin << setw(20) << proba << setw(15) << error << setw(20) << weight << setw(15) << unc << /*normal <<*/ endl;
        }

        return { MapNo, MapUp, MapDn };
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Prepare weights including variations.
    ///
    /// The smooth function contain the probability for *one* jet to cause the prefiring
    /// - as it is a function, the conversion to 1-weight will be done while running
    /// - for the uncertainty, we will just vary the weight while applying it with 20%
    static tuple<TF1*, TF1*> PrepareSmoothWeights (TString hname, TFile * f)
    {
        cout << hname << endl;
        auto f5 = dynamic_cast<TF1*>(f->Get(hname + "_ybin5")),
             f6 = dynamic_cast<TF1*>(f->Get(hname + "_ybin6"));
        assert(f5 != nullptr);
        assert(f6 != nullptr);
        return {f5, f6};
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Multiplicity plots: not used anywhere, just to compare how the different
    /// samples are populating the forward region...
    static vector<TH2 *> GetMultiplicityPlots (TH2 * templ, int n = 7)
    {
        vector<TH2*> v;
        for (int i = 1; i <= n; ++i) {
            auto h = dynamic_cast<TH2*>(templ->Clone(Form("multiplicity_nbin%d", i)));
            h->Reset();
            v.push_back(h);
        }
        return v;
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Functor to apply prefiring correction.
    applyPrefiringWeightsFunctor
        (int year,                //!< 201? (can be retrieved from MetaInfo)
         PrefOpt prefOpt, //!< prefiring option (see enum)
         bool IsMC = false        //!< flag (can be retrieved from MetaInfo)
         ) :
        isMC(IsMC),
        GetWeights(prefOpt == PrefOpt::kSmoothMapsPerEra ? &applyPrefiringWeightsFunctor::GetSmoothWeight
                                                                 : &applyPrefiringWeightsFunctor::GetRawWeights)
    {
        if (year != 2016 && year != 2017) {
            if (year == 2018)
                cerr << "No need to apply prefiring correction for 2018\n";
            cerr << "Expecting 2016 or 2017 only\n";
            exit(EXIT_FAILURE);
        }

        if (isMC) {
            switch (prefOpt) {
                case PrefOpt::kMapsPerEra: // this is a fundamental constrain
                    cerr << "Only average maps can be applied to MC\n";
                    exit(EXIT_FAILURE);
                case PrefOpt::kSmoothMapsPerEra: // but smoothing is in principle
                    cerr << "Smoothing can only be applied to data\n"; // also possible for the average maps -- TODO?
                    exit(EXIT_FAILURE);
                case PrefOpt::kAverageMap:
                    cout << "Proceeding with option AverageMap" << endl;
            }
        };

        // retrieve fs::path to file
        fs::path p = getenv("DAS_WORKAREA");
        p /= "tables/prefiring";
        switch (prefOpt) {
            case PrefOpt::kMapsPerEra      : p /= "JetPrefiringMapsperIOV_EOY.root";          break;
            case PrefOpt::kSmoothMapsPerEra: p /= "JetPrefiringMapsperIOV_EOY_smoothed.root"; break;
            case PrefOpt::kAverageMap      : p /= Form("L1prefiring_jetpt_%dBto%c_coarse.root", year, year == 2016 ? 'H' : 'F');
        }
        if (!exists(p)) {
            cerr << p << " could not be found.\n";
            exit(EXIT_FAILURE);
        }

        auto f = TFile::Open(p.c_str(), "READ");
        auto getHist = [f](const char * hname) {
            cout << "Getting " << hname << endl;
            auto hIn = dynamic_cast<TH2*>(f->Get(hname)); // this map contains the probabilities, not the weights (p + w = 1)
            assert(hIn != nullptr);
            return hIn;
        };
        
        // retrieve map in histograms
        if (prefOpt == PrefOpt::kAverageMap) {
            auto firstRun = (eras.at(year).begin())->first;
            TString e = year;

            TString hname = Form("L1prefiring_jetpt_%dBto%c", year, year == 2016 ? 'H' : 'F');
            auto hIn = getHist(hname);
            prefRawWeights.insert( {firstRun, PrepareRawWeights(e, hIn) });
        }
        else
        for (auto& era: eras.at(year)) {
            int firstRun = era.first;
            TString e = era.second;

            TString hname = Form("L1prefiring_jetpt_%d", year) + e;
            if (prefOpt == PrefOpt::kSmoothMapsPerEra) {
                prefSmoothWeights.insert( {firstRun, PrepareSmoothWeights(hname, f) });
            }
            else {
                auto hIn = getHist(hname);
                prefRawWeights.insert( {firstRun, PrepareRawWeights(e, hIn) });
            }
        }

        f->Close();

        if (prefOpt != PrefOpt::kSmoothMapsPerEra) {
            TH2 * templ = get<0>(prefRawWeights.begin()->second);
            multiplicities = GetMultiplicityPlots(templ);
        }

        cout << "Prefiring weights are ready" << endl;
    }

private:
    ////////////////////////////////////////////////////////////////////////////////
    /// Subroutine to get weights for a give event directly from the maps
    tuple<float,float,float> GetRawWeights (int run, const RecJet& recjet) const
    {
        auto it = prefRawWeights.rbegin();
        if (!isMC) while (run < it->first) ++it;
        TH2 * MapNo = get<0>(it->second),
            * MapUp = get<1>(it->second),
            * MapDn = get<2>(it->second);
        
        FourVector p4 = recjet.p4;
        p4.Scale(recjet.JECs.front());

        auto eta = p4.Eta();

        if (std::abs(eta) < 2.0 || std::abs(eta) >= 3.0) return { 1, 1, 1};

        auto pt = p4.Pt();

        int etabin =     MapNo->GetXaxis()->FindBin(eta),
            ptbin  = min(MapNo->GetYaxis()->FindBin(pt ), MapNo->GetNbinsY()); // i.e. don't take the overflow

        auto corr  = MapNo->GetBinContent(etabin, ptbin),
             varUp = MapUp->GetBinContent(etabin, ptbin),
             varDn = MapDn->GetBinContent(etabin, ptbin);

        //cout << run << setw(15) << eta << setw(15) << pt << setw(10) << etabin << setw(10) << ptbin << setw(20) << corr << setw(10) << varUp << setw(10) << varDn << '\n';

        return {corr, varUp, varDn};
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Subroutine to get weights for a give event from the smooth functions
    tuple<float, float, float> GetSmoothWeight (int run, const RecJet& recjet) const
    {
        auto it = prefSmoothWeights.begin();
        if (!isMC) while (run < it->first) ++it;

        FourVector p4 = recjet.p4;
        p4.Scale(recjet.JECs.front());

        auto eta = p4.Eta();
        int etabin = 1 + abs(eta)*2;

        //cout << eta << ' ' << etabin << endl;
        if (etabin < 5 || etabin > 6) return { 1, 1, 1};

        auto f = etabin == 5 ? get<0>(it->second) : get<1>(it->second);
        auto pt = p4.Pt();
        auto eval = f->Eval(pt);

        float corr  = 1-    eval,
              varUp = 1-1.5*eval,
              varDn = 1-0.75*eval;

        //cout << run << setw(15) << eta << setw(15) << pt << setw(10) << etabin << setw(20) << corr << setw(10) << varUp << setw(10) << varDn << '\n';

        return {corr, varUp, varDn};
    }

public:
    ////////////////////////////////////////////////////////////////////////////////
    /// Operator overloading for event-by-event call
    void operator() (Event & evnt, vector<RecJet>& recjets)
    {
        //cout << "=====" << endl;

        // first, get the weights as a function of all jets present at |eta| > 2.0
        tuple<float,float,float> wAllJets { 1, 1, 1 };
        for (const RecJet& recjet: recjets) {

            auto wJet = (this->*GetWeights)(evnt.runNo, recjet);

            get<0>(wAllJets) *= get<0>(wJet);
            get<1>(wAllJets) *= get<1>(wJet);
            get<2>(wAllJets) *= get<2>(wJet);

        }

        // then fill control plots
        size_t i = 0;
        bool fillMult = recjets.size() > 0 && recjets.front().CorrPt() >= 74 && abs(recjets.front().Rapidity()) < y_edges.back();
        for (const RecJet& recjet: recjets) {

            auto pt = recjet.CorrPt(0),
                 wEvt0 = evnt.recWgts.front(),
                 wJet0 = recjet.weights.front();
            if (isMC) wEvt0 *= evnt.genWgts.front();

            // multiplicites
            static const size_t n = multiplicities.size();
            if (i < n && fillMult) {
                multiplicities.at(i)->Fill(recjet.CorrEta(), pt, wEvt0 * wJet0);
                ++i;
            }
        }

        //cout << evnt.runNo << setw(20) << get<0>(wAllJets) << setw(20) << get<1>(wAllJets) << setw(20) << get<2>(wAllJets) << '\n';
        //cout << "-----" << endl;

        // finally, modify the branches
        auto rw0 = evnt.recWgts.front();
        if (isMC) {
            for (auto& rw: evnt.recWgts)
                rw *= get<0>(wAllJets);
            evnt.recWgts.push_back(rw0 * get<1>(wAllJets));
            evnt.recWgts.push_back(rw0 * get<2>(wAllJets));
        }
        else {
            assert(get<0>(wAllJets) > 0); // this could only arrive for jets < 30 GeV
            for (auto& rw: evnt.recWgts)
                rw /= get<0>(wAllJets);
            evnt.recWgts.push_back(rw0 / get<1>(wAllJets));
            evnt.recWgts.push_back(rw0 / get<2>(wAllJets));
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Write maps and control plots to output root file
    void Write (TDirectory * dir)
    {
        dir->cd();
        for (auto& prefRawWeight: prefRawWeights) {
            dir->cd();
            auto subdir = dir->mkdir(Form("fromRun%d", prefRawWeight.first));
            subdir->cd();
            TH2 * MapNo = get<0>(prefRawWeight.second),
                * MapUp = get<1>(prefRawWeight.second),
                * MapDn = get<2>(prefRawWeight.second);
            for (TH2 * h: {MapNo, MapUp, MapDn}) {
                h->SetDirectory(subdir);
                TString name = h->GetName();
                name = name(0, name.First('_')-1);
                h->Write(name);
            }
        }
        for (auto& prefSmoothWeight: prefSmoothWeights) {
            dir->cd();
            auto subdir = dir->mkdir(Form("fromRun%d", prefSmoothWeight.first));
            subdir->cd();
            TF1 * f5 = get<0>(prefSmoothWeight.second),
                * f6 = get<1>(prefSmoothWeight.second);
            f5->Write("ybin5");
            f6->Write("ybin6");
        }
        dir->cd();
        for (auto m: multiplicities) {
            m->SetDirectory(dir);
            m->Write();
        }
    }
};
