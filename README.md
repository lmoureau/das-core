# Core

The software is developed on top of CMSSW.
It allows to conduct the jet analyses from official CMS data sets to the results ready for publication.

In addition to the production of *n*-tuples, the basic principle of the software is to modify the Monte Carlo *n*-tuples step by step to make them resemble as much as possible to the data, and then to unfold.
It encompasses the following steps:
 1. *n*-tuplisation
 2. Basic treatement of data and MC (the list of correction is not exhaustive):
   1. for data, trigger, and ormalisation to the luminosity,
   2. for MC, normalisation to the cross section, corrections of pile-up simulation, corrections to the jet energy
 3. Unfolding with systematics

## Software Installation

First of all, connect to DESY NAF (note that the software has not been developed to work at CERN but only at DESY).

Then, install the software on your AFS (fast but limited storage):
```
cd $HOME
mkdir DAS
cd DAS
module use -a /afs/desy.de/group/cms/modulefiles/
module load cmssw
cmsrel CMSSW_10_6_30
cd $CMSSW_BASE/src
git clone https://gitlab.cern.ch/DasAnalysisSystem/Core.git
cd Core
cmsenv
scram b -j`nproc`
```

Finally, install the workarea on your NFS (slow but large storage):
```
cd /nfs/dust/cms/user/$USER
mkdir DAS
cd DAS
git clone ssh://git@gitlab.cern.ch:7999/DasAnalysisSystem/workarea.git
```

## Description of the subpackages

In alphabetical order:
 - [CommonTools](CommonTools/README.md): some common tools for development and running (no physics in this directory)
 - [JEC](JEC/README.md): apply JES correction and JER smearing
 - [Normalisation](Normalisation/README.md): normalisation of data and simulation after merging
 - [Ntupliser](Ntupliser/README.md): code for *n*-tuplisation with CRAB jobs
 - [Objects](Objects/README.md): code for the objects in the *n*-tuples
 - [PUprofile](PUprofile/README.md): tools to perform the pile-up profile reweighting in simulation
 - [PUstaubSauger](PUstaubSauger/README.md): tool to clean up the simulation from the badly sampled simulation
 - [Trigger](Trigger/README.md): trigger to determine the trigger turn-on points for 99% of efficiency

## Generic source of information

 - [JetMET](https://twiki.cern.ch/twiki/bin/view/CMS/JetMET)
 - [BTV](https://twiki.cern.ch/twiki/bin/viewauth/CMS/BtagRecommendation)
