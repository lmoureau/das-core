#include <fstream>
#include <iostream>

using namespace std;

struct TriggerLumi {
    int pt, turnon;
    double weight;
    TriggerLumi (int Pt, int Turnon, double Weight) :
        pt(Pt), turnon(Turnon), weight(Weight)
    {}
    TriggerLumi (ifstream& infile)
    {
        infile >> pt;
        infile >> turnon;

        double lumi;
        infile >> lumi;
        weight = 1./lumi;
    }
};

ostream& operator<< (ostream& Stream, const TriggerLumi& tr_lu)
{
    Stream << tr_lu.pt << '\t' << tr_lu.turnon << '\t' << tr_lu.weight << '\t' << 1./tr_lu.weight;
    return Stream;
}
