#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <vector>
#include <map>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/Looper.h"

#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>

#include "Math/VectorUtil.h"

using namespace std;
using namespace DAS;

/*
// TODO: get this from file old one
static const vector<double> lumis = {
    0.256679,
    0.697905,
    2.65953,
    23.3008,
    99.5125,
    571.491,
    1706.36,
    4998.15,
    35030.1
};
*/

////effective-based lumis
//static const vector<double> lumis = {
//    0.264135,
//    0.718248,
//    2.725,
//    23.933,
//    102.578,
//    587.08,
//    1752.59,
//    5135.58,
//    35893.7
//};

//prescale-based lumis
static const vector<double> lumis = {
      0.263583,
      0.716499,
      2.71181,
      23.8762,
      102.042,
      582.099,
      1747.75,
      5135.58,
      35893.7
};



static const size_t nlumis = lumis.size();



int getTrigIndex(double pt)
{
    // TODO: get this from file
    static const vector<double> turnOns = {
        50,
        85.8228,
        108.377,
        171.332,
        239.027,
        312.536,
        378.93,
        451.078,
        493.462};

    assert(lumis.size() == turnOns.size());

    for(int i = turnOns.size()-1; i >= 0; --i) {
        double pTlow  =  (i >= 0) ? turnOns[i] : 0;
        double pThigh =  (i < int(turnOns.size())-1) ? turnOns[i+1] : 1e8;
        if(pTlow < pt && pt < pThigh) {
            //cout << pt <<" "<< turnOns[i] << endl;
            return i;
        }
    }
    return -1;
}


////////////////////////////////////////////////////////////////////////////////
/// Get trigger curves from the data *n*-tuples
[[ deprecated ]]
void getTriggerCheck 
            (TString input,      //!< name of input root file with *n*-tuple
             TString output,     //!< name of output root file with trigger curves
             int nSplit = 1,     //!< number of jobs/tasks/cores
             int nNow = 0)       //!< index of job/task/core
{
    TH1::SetDefaultSumw2();

    TChain * chain = new TChain("inclusive_jets"); 
    chain->Add(input);

    Event * event = nullptr;
    Trigger * trigger = nullptr;
    chain->SetBranchAddress("event", &event);
    chain->SetBranchAddress("trigger", &trigger);

    vector<RecJet> * recjets = nullptr;
    vector<FourVector> * hltjets = nullptr;
    chain->SetBranchAddress("recJets", &recjets);
    chain->SetBranchAddress("hltJets", &hltjets);

    TFile *file = TFile::Open(output, "RECREATE");

    //Histogram based on the leading-pT triggering
    TH1D *hPtLead = new TH1D("hPtLead", "hPtLead",  pt_edges.size()-1, pt_edges.data());
    //Histogram based on the inclusive-pT triggering
    TH1D *hPtInc = new TH1D("hPtInc", "hPtInc",  pt_edges.size()-1, pt_edges.data());

    //Histogram based on the leading-pT triggering
    TH1D *hPtLeadPres = new TH1D("hPtLeadPres", "hPtLeadPres",  pt_edges.size()-1, pt_edges.data());

    // prescale run by run (map) for each trigger (vector)
    vector<map<pair<int,int>,int>> prescales(nlumis);

    Looper looper(__func__, chain, nSplit, nNow);
    while (looper.Next()) {

        // avoid empty events
        if(recjets->size() == 0) continue;

        //Filling pT using leading pT triggering method
        {
            int iTrig = getTrigIndex(recjets->front().p4.Pt());
            if(iTrig != -1 && trigger->Bit[iTrig]) {
                double wgt = 1 / lumis[iTrig];
                for(const auto & r : *recjets)
                    hPtLead->Fill(r.p4.Pt(), wgt);
            }
        }

        //Filling pT using inclusive pt triggering method
        {
            for(const auto & r : *recjets) {
                int iTrig = getTrigIndex(r.p4.Pt());
                if(iTrig != -1 && trigger->Bit[iTrig]) {
                    double wgt = 1 / lumis[iTrig];
                    hPtInc->Fill(r.p4.Pt(), wgt);
                }
            }
        }

        //Filling pT using leading pT triggering method with prescale
        {
            int iTrig = getTrigIndex(recjets->front().p4.Pt());
            if(iTrig != -1 && trigger->Bit[iTrig]) {
               double preHLT   = trigger->PreHLT[iTrig];
               double preL1min = trigger->PreL1min[iTrig];
               double preL1max = trigger->PreL1max[iTrig];
               if(preL1min != preL1max)
                  cout << preL1min <<" "<<  preL1max << endl;
               //if(iTrig == 2) cout << preHLT * preL1min << endl;

               double prescale = preHLT * preL1min;

               double wgt = prescale /  lumis.back();
               for(const auto & r : *recjets)
                  hPtLeadPres->Fill(r.p4.Pt(), wgt);

               pair<int,int> runlum = {event->runNo, event->lumi};
               if (prescales[iTrig].count(runlum)) {
                  //cout << iTrig << ' ' << runNo << ' ' << prescales[iTrig][runNo] << ' ' << prescale << endl;
                  assert(prescales[iTrig].at(runlum) == prescale);
               }
               else {
                  //cout << iTrig << ' ' << runlum.first << ' ' << runlum.second << ' ' << prescale << endl;
                  prescales[iTrig][runlum] = prescale;
               }
            }
        }
    }
    file->Write();
    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    if (argc < 3) {
        cout << argv[0] << " input output [nSplit [nNow]]\n"
             << "\twhere\tinput = n-tuple\n"
             << "\t     \toutput = root file with trigger check" << endl;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2];
    int nNow = 0, nSplit = 1;
    if (argc > 3) nSplit = atoi(argv[3]);
    if (argc > 4) nNow = atoi(argv[4]);

    getTriggerCheck(input, output, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
