#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <map>
#include <filesystem>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/stream.h"

#include "Core/CommonTools/interface/terminal.h"
#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/Looper.h"
#include "Core/CommonTools/interface/MetaInfo.h"

#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TTreeReader.h>
#include <TH1.h>
#include <TH2.h>
#include <TRandom3.h>

#include "Math/VectorUtil.h"
#include "Core/HotKiller/interface/applyConservativeHotKiller.h"

#include "common.h"
#include "Core/Trigger/interface/match.h"

using namespace std;
using namespace DAS;

namespace fs = filesystem;

////////////////////////////////////////////////////////////////////////////////
/// EfficiencyCurve does not exactly contain *the* trigger efficiency curve,
/// but rather the histograms to fill in order to get the efficiency curve,
/// namely the reference trigger and the test trigger histograms.
struct EfficiencyCurve {
    static const vector<double> y_edges;
    static const int nYbins;

    static vector<double> thresholds;
    static int nThresholds;
    const int t;
    const TString suffix; //!< name
    TH2 * ref, //!< count of jets by reference trigger 
        * test, //!< count of jets by test & reference triggers
        * hAll, //<! CP for all matched jets
        * hFired, //!< CP for all matched jets IFF the trigger has fired
        * hFiredThreshold, //!< CP for all matched jets IFF the trigger has fired AND the matched HLT jet is above the HLT threshold
        * hHLTmap; //!< Eta vs phi distribution of the HLT jets

    EfficiencyCurve (const char * method, int threshold):
        t(threshold),
        suffix(Form("_%s_%d", method, threshold)),
        ref            (new TH2F("ref"           + suffix, ";p_{T}^{PF corr}   (GeV);N_{ref}"     , nPtBins, pt_edges.data(), nYbins, y_edges.data())),
        test           (new TH2F("test"          + suffix, ";p_{T}^{PF corr}   (GeV);N_{test}"    , nPtBins, pt_edges.data(), nYbins, y_edges.data())),
        hAll           (new TH2F("all"           + suffix, ";p_{T}^{PF corr}   (GeV);p_{T}^{HLT}   (GeV)", nRecBins, recBins.data(), nThresholds, thresholds.data())),
        hFired         (new TH2F("fired"         + suffix, ";p_{T}^{PF corr}   (GeV);p_{T}^{HLT}   (GeV)", nRecBins, recBins.data(), nThresholds, thresholds.data())),
        hFiredThreshold(new TH2F("firedMatching" + suffix, ";p_{T}^{PF corr}   (GeV);p_{T}^{HLT}   (GeV)", nRecBins, recBins.data(), nThresholds, thresholds.data())),
        hHLTmap        (new TH2F("HLTmap"        + suffix, "; #phi^{hlt} ; #eta^{hlt}"            , 200, -M_PI, M_PI, 200, -5, 5))
    {
        assert(find(thresholds.begin(), thresholds.end(), t) != thresholds.end());
    }

    void Fill (const FourVector& p4, bool fired, double w = 1)
    {
        double y = abs(Rapidity(p4));
        ref->Fill(p4.Pt(), y, w);
        if (fired)
            test->Fill(p4.Pt(), y, w);
    }

    void Write (TDirectory * d)
    {
        d->cd();
        auto dd = d->mkdir(Form("HLT%d", t));
        dd->cd();
        for (auto h: {ref, test, hAll, hFired, hFiredThreshold, hHLTmap}) {
            h->SetDirectory(dd);
            TString name = h->GetName();
            name.ReplaceAll(suffix,"");
            h->Write(name);
        }
    }
};

const vector<double> EfficiencyCurve::y_edges = MN_helper::y_edges;
const int EfficiencyCurve::nYbins = EfficiencyCurve::y_edges.size()-1;

vector<double> EfficiencyCurve::thresholds;
int EfficiencyCurve::nThresholds;

double getPrescale (Trigger * trigger, size_t indx)
{
    double preHLT   = trigger->PreHLT[indx];
    double preL1min = trigger->PreL1min[indx];
    double preL1max = trigger->PreL1max[indx];
    assert(preL1min == preL1max);
    double prescale = preHLT * preL1min;
    return prescale;
}

using ROOT::Math::VectorUtil::DeltaR;
using ROOT::Math::VectorUtil::DeltaPhi;

////////////////////////////////////////////////////////////////////////////////
/// Get trigger curves from the data *n*-tuples
void getTriggerCurves 
            (const fs::path& input,      //!< name of input root file with *n*-tuple
             const fs::path& output,     //!< name of output root file with trigger curves
             const fs::path& lumi_file,  //!< name of input txt 2-colums file with thresholds and lumis
             bool usePrescale,   //!< flag whether or not prescales should be used when filling histograms
             int nSplit = 1,     //!< number of jobs/tasks/cores
             int nNow = 0)       //!< index of job/task/core
{
    assert(fs::exists(input));
    TFile * source = TFile::Open(input.c_str(), "READ");
    TTree * tree = dynamic_cast<TTree *>(source->Get("inclusive_jets"));

    MetaInfo metainfo(tree);
    if (nNow == 0) metainfo.Print();

    ifstream triggerLumi;
    assert(fs::exists(lumi_file));
    triggerLumi.open(lumi_file);
    while (triggerLumi.good()){
        float threshold;
        float lumi;
        triggerLumi >> threshold >> lumi;
        if (triggerLumi.eof()) break;
        EfficiencyCurve::thresholds.push_back(threshold);
    }
    EfficiencyCurve::nThresholds = EfficiencyCurve::thresholds.size()-1;

    assert(!metainfo.isMC());

    TChain * oldchain = new TChain("inclusive_jets"); // arg = fs::path inside the ntuple
    oldchain->Add(input.c_str());

    Event * event = nullptr;
    Trigger * trigger = nullptr;
    oldchain->SetBranchAddress("event", &event);
    oldchain->SetBranchAddress("trigger", &trigger);

    vector<RecJet> * recjets = nullptr;
    vector<FourVector> * hltjets = nullptr;
    oldchain->SetBranchAddress("recJets", &recjets);
    oldchain->SetBranchAddress("hltJets", &hltjets);

    if (fs::exists(output))
        cerr << red << output << " will be overwritten\n" << normal;
    TFile *file = TFile::Open(output.c_str(), "RECREATE");

    map<TString, size_t> methods { // methods[name] = shift (first trigger for which the method is used)
        {"emulation", 1}, // default method
        {"emulationShift", 2}, // emulation but with a lower-threshold reference trigger (e.g. trig 40 as reference for trig 80 as test)
        {"TnP", 0} // tag-and-probe method (used in any case of the lowest reference trigger(s)
    };

    TH2F *pt_correlation = new TH2F("ptCorr","; p_{T}^{PF,corr}; p_{T}^{hlt}",nPtBins,pt_edges.data(),nPtBins,pt_edges.data());
    TH1F *Deta = new TH1F("deta","; #Delta #eta; nevt", 200, -5, 5);
    TH1F *Dphi = new TH1F("dphi","; #Delta #phi; nevt", 200, -M_PI, M_PI);
    TH1F *dR = new TH1F("dR","; #Delta R; nevt", 200, 0, 10);

    map<TString, map<int, EfficiencyCurve *>> curves;
    for (auto method: methods) {
        for (size_t i = method.second; i < EfficiencyCurve::thresholds.size(); ++i) {
            int t = EfficiencyCurve::thresholds.at(i);
            curves[method.first][t] = new EfficiencyCurve(method.first, t);
            cout << method.first << '\t' << t << '\n';
        }
    }
    cout << flush;

    TRandom3 Rand(/* seed = */ nNow);

    Looper looper(__func__, oldchain, nSplit, nNow);
    while (looper.Next()) {

        // sanity check
        size_t nbits = trigger->Bit.size();    
        if (nbits == 0) continue;
        assert(nbits >= EfficiencyCurve::thresholds.size());

        // avoid empty events
        if (recjets->size() == 0) continue;

        for (RecJet pfjet: *recjets){
            FourVector hltjet = match(pfjet.p4, hltjets);

            if ( hltjet == FourVector()) continue;
            Deta->Fill(pfjet.CorrEta() - hltjet.Eta(), pfjet.weights.front());
            Dphi->Fill(DeltaPhi(pfjet.p4,hltjet), pfjet.weights.front());
            dR->Fill(DeltaR(pfjet.p4, hltjet), pfjet.weights.front());
            pt_correlation->Fill(pfjet.CorrPt(), hltjet.Pt(), pfjet.weights.front());
        }

        FourVector pfjet0 = recjets->front().p4;
        pfjet0.Scale(recjets->front().JECs.front());           // TODO: Be Carefull only the leading jet is considered.

        FourVector hltjet0 = match(pfjet0, hltjets);

        /***  using emulation method(s) ***/
        for (auto method: methods) {

            if (method.first.Contains("TnP")) continue;
            size_t shift = method.second;

            for (size_t i = shift; i < EfficiencyCurve::thresholds.size(); ++i) {

                int t = EfficiencyCurve::thresholds[i];
                auto& eff = curves.at(method.first).at(t);

                eff->hAll->Fill(pfjet0.Pt(), hltjet0.Pt());

                // test if previous trigger has fired
                if (!trigger->Bit[i-shift]) continue; 
                eff->hFired->Fill(pfjet0.Pt(), hltjet0.Pt());

                if (hltjet0.Pt() < EfficiencyCurve::thresholds.at(i-shift)) continue;
                eff->hFiredThreshold->Fill(pfjet0.Pt(), hltjet0.Pt());

                double wgt = usePrescale ? getPrescale(trigger, i-shift) : 1;
                bool fired = hltjet0.Pt() > t;
                eff->Fill(pfjet0, fired, wgt*recjets->front().weights.front());

                eff->hHLTmap->Fill( pfjet0.Phi(),pfjet0.Eta(),wgt*recjets->front().weights.front() );
            }
        } // end of loop on methods

        /*** using tag & probe method ***/
        for (size_t i = 0; i < EfficiencyCurve::thresholds.size(); ++i) {

            int t = EfficiencyCurve::thresholds[i];
            auto& eff = curves.at("TnP").at(t);

            eff->hAll->Fill(pfjet0.Pt(), hltjet0.Pt());

            // first, it needs to have fired
            if (!trigger->Bit[i])
                continue;
            eff->hFired->Fill(pfjet0.Pt(), hltjet0.Pt());

            // then we need dijet configurations at PF level:
            // -> 1) at least 2 jets 
            if (recjets->size() == 1) 
                continue;
            // -> 2) back-to-back jets
            if (DeltaPhi(recjets->at(0).p4, recjets->at(1).p4) < 2.4) continue;
            // -> 3) a possible 3rd jet should not be significant
            if (recjets->size() > 2) {
                const double pt0 = recjets->at(0).CorrPt(),
                             pt1 = recjets->at(1).CorrPt(),
                             pt2 = recjets->at(2).CorrPt();
                if (pt2 > 0.15*(pt0 + pt1))
                    continue;
            }

            // we take the probe and the tag randomly
            double r = Rand.Uniform();
            int itag = (r<0.5),
                iprobe = (r>=0.5);

            FourVector pftag   = recjets->at(itag  ).p4,
                       pfprobe = recjets->at(iprobe).p4;

            float pfprobe_wgt = recjets->at(iprobe).weights.front();
            float pftag_wgt = recjets->at(itag).weights.front();

            pftag  .Scale(recjets->at(itag  ).JECs.front());
            pfprobe.Scale(recjets->at(iprobe).JECs.front());
            
            // -> 4) selection on the rapidity of the system
            if (std::abs(recjets->at(itag).CorrEta()) > 1.3) continue;

            // match PF at HLT
            // 1) test if tag can be matched
            //    - if not, continue
            // 2) then test if the probe can be match

            const FourVector& hlttag = match(pftag, hltjets);
            if (hlttag.Pt() > t) {
                eff->hFiredThreshold->Fill(pfjet0.Pt(), hltjet0.Pt());

                const FourVector& hltprobe = match(pfprobe, hltjets);
                bool fired = hltprobe.Pt() > t;

                double wgt = usePrescale ? getPrescale(trigger, i) : 1;
                eff->Fill(pfprobe, fired, wgt * pftag_wgt * pfprobe_wgt);
                eff->hHLTmap->Fill( pfjet0.Phi(),pfjet0.Eta(),wgt*recjets->front().weights.front() );
            }
        }
    }

    cout << "Writing to file" << endl;
    for (auto& method: curves) {
        file->cd();
        auto d = file->mkdir(method.first);
        for (auto& curve: method.second)
            curve.second->Write(d);
    }
    file->cd();
    dR->Write();
    Deta->Write();
    Dphi->Write();
    pt_correlation->Write();
    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();

    if (argc < 5) {
        cout << argv[0] << " input output lumi_file prescales [nSplit [nNow]]\n"
             << "\twhere\tinput = data n-tuple\n"
             << "\toutput = root file with trigger curves (on top of which `getTriggerTurnons` can be used)\n"
             << "\tlumi_file = two column file with hlt threshlods and luminosities\n"
             << "\tprescales = boolean flag activating the use of prescales when filling histograms (yes/no, true/false, 1/0)\n"
             << flush;
        return EXIT_SUCCESS;
    }

    fs::path input = argv[1],
         output = argv[2],
         lumi_file = argv[3];

    TString prescales = argv[4];
    prescales.ToLower();
    bool usePrescale = (prescales.Contains("true") || prescales.Contains("yes") || prescales.Atoi() > 0);

    int nNow = 0, nSplit = 1;
    if (argc > 5) nSplit = atoi(argv[5]);
    if (argc > 6) nNow = atoi(argv[6]);

    getTriggerCurves(input, output, lumi_file, usePrescale, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
