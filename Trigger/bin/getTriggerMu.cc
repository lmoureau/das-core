#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <map>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Lepton.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/Looper.h"

#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>

#include "Math/VectorUtil.h"

using namespace std;
using namespace DAS;

const vector<double> muTrh = {1.4*8, 1.4*17, 1.4*20, 1.4*27, 1.4*50};


#ifndef DOXYGEN_SHOULD_SKIP_THIS
struct Curves {
    TString t;
    TH1 * ref, * test;
    Curves (int id, int threshold) :
        t(Form("%d", threshold)),
        ref (new TH1D(Form("ref%s_%d", t.Data(), id), Form("ref%s_%d", t.Data(), id), nPtBins, pt_edges.data())),
        test(new TH1D(Form("test%s_%d", t.Data(), id), Form("test%s_%d", t.Data(), id), nPtBins, pt_edges.data()))
    {}
    void Fill (const FourVector& p4, bool fired)
    {
        ref->Fill(p4.Pt());
        if (fired)
            test->Fill(p4.Pt());
    }
};
#endif

////////////////////////////////////////////////////////////////////////////////
/// Get trigger curves from the data *n*-tuples
[[ deprecated ]]
void getTriggerMu 
            (TString input,      //!< name of input root file with *n*-tuple
             TString output,     //!< name of output root file with trigger curves
             int nSplit = 1,     //!< number of jobs/tasks/cores
             int nNow = 0)       //!< index of job/task/core
{
    TH1::SetDefaultSumw2();

    TChain * chain = new TChain("inclusive_jets"); 
    chain->Add(input);

    Trigger * trigger = nullptr;
    chain->SetBranchAddress("trigger", &trigger);

    vector<RecJet> * recjets = nullptr;
    vector<RecLep> * recMu = nullptr;
    vector<FourVector> * hltjets = nullptr;
    chain->SetBranchAddress("recJets", &recjets);
    chain->SetBranchAddress("recMuons", &recMu);
    chain->SetBranchAddress("hltJets", &hltjets);

    TFile *file = TFile::Open(output, "RECREATE");

    vector<map<int, Curves *>> curves(5);

    vector<int> thresholds {40, 60, 80, 140, 200, 260, 320, 400, 450, 500};
    for(int i = 0; i < 5; ++i)
       for (int t: thresholds) 
          curves[i][t] = new Curves(i, t);

    Looper looper(__func__, chain, nSplit, nNow);
    while (looper.Next()) {

        // sanity check
        assert(trigger->Bit.size() >= thresholds.size());

        // avoid empty events
        if(recjets->size() == 0) continue;
        if(recMu->size() == 0) continue;

        const FourVector& leadingRec = recjets->front().p4;
        //double ptHLT = hltjets->size() > 0 ? hltjets->front().Pt() : 0;


        //bool hlt = false;
        //for(int k = 0; k < 10; ++k)
        //   hlt += trigger->Bit[k];


        //if(trigger->Bit[10+4]) 
        //   cout <<"Hope " <<  ptHLT <<" "<< leadingRec.Pt() <<" "<< hlt << endl;

        for(int k = 0; k < 5; ++k) {
           if(!trigger->Bit[10+k]) continue;
           if(recMu->front().p4.Pt() < muTrh[k]) continue;
           if(abs(recMu->front().p4.Eta())  > 1.5) continue;

           // using emulation method
           for (size_t i = 0; i < thresholds.size(); ++i) {
               int t = thresholds[i];
               //bool fired = ptHLT > t;
               bool fired = trigger->Bit[i];
               curves[k][t]->Fill(leadingRec, fired);
           }
        }
    }
    file->Write();
    file->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    if (argc < 3) {
        cout << argv[0] << " input output [nSplit [nNow]]\n"
             << "\twhere\tinput = n-tuple\n"
             << "\t     \toutput = root file with trigger curves" << endl;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2];
    int nNow = 0, nSplit = 1;
    if (argc > 3) nSplit = atoi(argv[3]);
    if (argc > 4) nNow = atoi(argv[4]);

    getTriggerMu(input, output, nSplit, nNow);
    return EXIT_SUCCESS;
}
#endif
