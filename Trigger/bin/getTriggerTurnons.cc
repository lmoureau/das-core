#include <cstdlib>
#include <cassert>
#include <cmath>
#include <iostream>
#include <fstream>
#include <filesystem>

#include <TROOT.h>
#include <TString.h>
#include <TFile.h>
#include <TH2.h>
#include <TKey.h>

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/terminal.h"

#include "Core/Trigger/interface/sigmoid.h"
#include "Core/Trigger/interface/match.h"

#include "common.h"

using namespace std;
using namespace DAS;

namespace fs = filesystem;

////////////////////////////////////////////////////////////////////////////////
/// Get the trigger turn-on points from the trigger curves
void getTriggerTurnons 
            (const fs::path& input,                 //!< name of input root file with trigger curves
             const fs::path& outputTxt,             //!< name of output txt file
             const fs::path& outputRoot,            //!< name of output root file
             const fs::path& lumi_file,             //!< two column file with the HLT thresholds and the lumis
             int year)
{
    cout << "Strategy:\n"
         << " - For the first trigger (presumably HLT40), only TnP is available\n"
         << " - For all other triggers, emulation is used by default\n"
         << " - The other methods are used as cross-checks\n"
         << flush;
    
    assert(fs::exists(input));
    auto fileIn  = TFile::Open(input     .c_str(), "READ"    );
    if (fs::exists(outputRoot))
        cerr << red << outputRoot << " will be overwritten\n" << normal;
    auto fileOut = TFile::Open(outputRoot.c_str(), "RECREATE");

    cout << setw(10) << "trigger";
    static const vector<TString> yBins = MakeTitle(MN_helper::y_edges, "|y|", false, true,
                                                [](double v) { return Form("%.1f", v);});
    for (auto yBin: yBins) cout << setw(18) << yBin;
    cout << endl;

    ifstream triggerLumi;
    assert(fs::exists(lumi_file));
    triggerLumi.open(lumi_file);
    vector<int> triggerThresholds;
    while (triggerLumi.good()){
        float threshold;
        float lumi;
        triggerLumi >> threshold >> lumi;
        if (triggerLumi.eof()) break;
        triggerThresholds.push_back(threshold);
    }

    map<int, int> turnonsFinal;
    TIter Next(fileIn->GetListOfKeys()) ;
    TKey* key = nullptr;
    TString defMethod = "TnP"; // only for the first trigger(s)
    while ( (key = dynamic_cast<TKey*>(Next())) ) { // looping over methods
        if (TString(key->ReadObj()->ClassName()) != TString("TDirectoryFile"))
            continue;

        fileIn->cd();
        auto dIn = dynamic_cast<TDirectory*>(key->ReadObj());

        int lastturnon = triggerThresholds.front();
        cout << dIn << endl;

        TString method = dIn->GetName();
        cout << method << endl;

        fileOut->cd();
        auto dOut = fileOut->mkdir(method);

        TIter Next2(dIn->GetListOfKeys()) ;
        while ( (key = dynamic_cast<TKey*>(Next2())) ) { // looping of triggers
            if (TString(key->ReadObj()->ClassName()) != TString("TDirectoryFile"))
                continue;

            dIn->cd();
            auto ddIn = dynamic_cast<TDirectory*>(key->ReadObj());

            TString trigName = ddIn->GetName();
            assert(trigName.Contains("HLT"));

            dOut->cd();
            auto ddOut = dOut->mkdir(trigName);

            trigName.ReplaceAll("HLT","");
            int trigger = trigName.Atoi();
            if (method == "TnP"            && trigger < triggerThresholds.at(0)) continue;
            if (method == "emulation"      && trigger < triggerThresholds.at(1)) continue;
            if (method == "emulationShift" && trigger < triggerThresholds.at(2)) continue;
            cout << setw(10) << trigger;

            auto h2  = dynamic_cast<TH2*>(ddIn->Get("test"));
            {
                auto den = dynamic_cast<TH2*>(ddIn->Get( "ref"));
                h2->Divide(h2, den, 1, 1, "B");
            }

            ddOut->cd();

            h2->SetDirectory(ddOut);
            h2->SetName("efficiency");

            vector<int> turnonsNow;
            for (int y = 1; y <= h2->GetNbinsY(); ++y) {

                // efficiency curve
                TH1 * h1 = h2->ProjectionX(Form("efficiency_ybin%d", y), y, y);
                //cout << y << '\t';
                static ostream bitBucket(0); // printing to nowhere
                double threshold = 0.995;
                switch (year) {
                    case 2017:
                        if (y > 5) threshold = 0.99;
                        break;
                    case 2018:
                        if (y > 3) threshold = 0.99;
                        break;
                    case 2016:
                    default:
                        if (y > 5) threshold = 0.99; //2016 Pre
                        //if (y > 5) threshold = 0.99; 2016 Post
                }
                float from = h1->GetBinLowEdge(h1->FindBin(lastturnon)+1);
                int turnon = GetTriggerEfficiency(trigger, h1, bitBucket, from, threshold);
                h1->SetTitle(Form("%d (%.3f)", turnon, threshold));
                //h1->Print("all");
                h1->SetDirectory(dOut);
                h1->Write();
                turnonsNow.push_back(turnon);
            }

            // TODO: implement a more general logic to use only rapidity <3 turnons to
            // determine the overall turnon
            auto maxturnon = max_element(turnonsNow.begin(), turnonsNow.begin()+6);
            h2->SetTitle(Form("%d", *maxturnon));
            h2->Write();
            for (auto turnon: turnonsNow) {
                if (turnon == *maxturnon) cout << "\x1B[32m\e[1m";
                if (turnon < 0) cout << "\x1B[31m\e[1m";
                cout << setw(18) << turnon;
                cout << "\x1B[30m\e[0m";
            }
            cout << endl;

            // final choice
            if (method == defMethod)
                turnonsFinal[trigger] = *maxturnon;

            defMethod = "emulation";
            lastturnon = *maxturnon;

            if (lastturnon == -1) { 
                    cerr << "Didn't find any turnon for trigger " << trigger << '\n';
                    lastturnon = trigger;
            }
        } // end of loop over triggers
    } // end of loop over method

    if (fs::exists(outputTxt))
        cerr << outputTxt << " will be overwritten\n";
    ofstream turnon_file;
    turnon_file.open(outputTxt);
    for (auto turnon: turnonsFinal) turnon_file << setw(3) << turnon.first << setw(10) << turnon.second << '\n';
    turnon_file.close();
    fileIn->Close();
    fileOut->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH2::SetDefaultSumw2(true);
    gROOT->SetBatch();

    if (argc < 6) {
        cout << argv[0] << " input outputTxt outputRoot\n"
             << "where\tinput = root file with trigger curves\n"
             << "     \toutputTxt = txt file with turn-on points\n"
             << "     \toutputRoot = root file with turn-on curves\n"
             << "     \tlumi_file = 2 column text file with the HLT thresholds and Lumis\n"
             << "     \tyear = 201?\n"
             << flush;
        return EXIT_SUCCESS;
    }
    fs::path input = argv[1],
             outputTxt = argv[2],
             outputRoot = argv[3],
             lumi_file = argv[4];
    int year = atoi(argv[5]);
    getTriggerTurnons(input, outputTxt, outputRoot, lumi_file, year);
    return EXIT_SUCCESS;
}
#endif
