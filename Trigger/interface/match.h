#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/stream.h"
#include "Core/CommonTools/interface/variables.h"
#include "Math/VectorUtil.h"

DAS::FourVector match (const DAS::FourVector & jet, const std::vector<DAS::FourVector>* hltJets) {
    for (const auto& hltjet: *hltJets){
        using ROOT::Math::VectorUtil::DeltaR;
        if (DeltaR(hltjet, jet) < 0.3)
            return hltjet;
    }
    return DAS::FourVector();
}

namespace MN_helper {
static const std::vector<double> y_edges {0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0};
} // end of namespace
